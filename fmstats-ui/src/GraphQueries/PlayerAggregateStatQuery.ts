import { BasicDataQuery } from './BasicDataQuery';
import gql from 'graphql-tag';
import { PlayerAggregateStats } from '../objects/Player/PlayerStats';
import { DocumentNode } from 'graphql';

export class PlayerAggregateStatQuery extends BasicDataQuery<PlayerAggregateStats> {
    listCollection = 'allPlayerStats';    
    singleResultProperty = 'PlayerStat';

    multipleResult = (
        params?: {
                queryParams?: {ids?: Array<string>, category: string}, 
                pagination?: {page: number, rowsPerPage: number }, 
                keys?: Array<keyof PlayerAggregateStats>
            }
        ) : {query: DocumentNode, resultTransformer: (data: object) => Array<PlayerAggregateStats>} => {
            
        const {ids, category} = params?.queryParams;        
        if(!!ids && ids.length > 0) {
            let request = `{`;
            ids.forEach(id => {
                request += this.formQueryItem({
                    alias: `${this.listCollection}${id}`,
                    filterParams: {id: id, category: category},
                    pagination: params?.pagination,
                    keys: params?.keys
                });
                request += '\n';                
            });
            request += '}';
            const gRequest = gql`${request}`;
            const resultTransformer = (data: object) : Array<PlayerAggregateStats> => {
                return ids.flatMap(id => data[`${this.listCollection}${id}`]);
            }

            return {
                query: gRequest,
                resultTransformer: resultTransformer
            };
        } else {
            return super.multipleResult(params);
        }                               
    };    

    static build = () => {
        const playerAggStats : PlayerAggregateStats = {
            id: '',
            category: null,
            season: null,
            statistics: []
        };

        return new PlayerAggregateStatQuery(playerAggStats);
    }
}