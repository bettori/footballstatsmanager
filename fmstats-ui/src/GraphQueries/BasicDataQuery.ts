import { DocumentNode } from 'graphql';
import { GraphQLClient } from '../serviceClients/GraphQlClient';
import { toJS } from 'mobx';
import gql from 'graphql-tag';
import { SortDirection } from '@material-ui/core/TableCell';

export class BasicDataQuery<T> {
    private graphQLClient;

    referenceObject: T
    defaultKeys: Array<keyof T>
    
    constructor(public refObject: T) {
        this.referenceObject = refObject;
        this.defaultKeys = Object.keys(this.referenceObject) as Array<keyof T>;
        this.graphQLClient = new GraphQLClient();
    }
    
    listCollection: string = '';
    singleResultProperty: string = '';

    isNumber(x: any): x is number {
        return typeof x === 'number';
    }
    
    isString(x: any): x is string {
        return typeof x === 'string';
    }

    protected shapeQueryWithKeys = (query: string, keys: Array<keyof T>) : string => {
        query += ` {\n`;
        keys.forEach(key => {
            query += `${key}\n`;
        });
        query += `}`;
        return query;
    }

    singleResult = (id: string, keys?: Array<keyof T>) : DocumentNode => { 
        let request = `${this.singleResultProperty}(id: ${id})`;
        request = this.shapeQueryWithKeys(request, keys ?? this.defaultKeys);
        return gql`${request}`;
    }

    singleResultTransformer = (data: object) : T => data[this.singleResultProperty];

    retrieveSingle = async<K extends keyof T>(id: string, keys?: Array<K>) : Promise<T> => {
        return toJS(this.singleResultTransformer(await this.graphQLClient.fetch(this.singleResult(id, keys))));
    }

    formQueryItem = (
                        itemParams: {
                            alias?: string, 
                            filterParams?: object, 
                            sorting?: {sortField: string, sortOrder: SortDirection}
                            pagination?: {page: number, rowsPerPage: number}, 
                            keys?: Array<keyof T> 
                        }
                    ) : string => 
    {
        let request = '';
        if(!!itemParams.alias) {
            request += `${itemParams.alias}: `;
        }

        request += `${this.listCollection}`;

        if(!!itemParams.filterParams || !!itemParams.pagination) {
            request += '(';

            if(!!itemParams.filterParams) {
                request += 'filter: {';
                Object.keys(itemParams.filterParams).forEach(key => {
                    const value = itemParams.filterParams[key];
                    if(this.isNumber(value)) {
                        request += `${key}: ${itemParams.filterParams[key]}, `;                        
                    } else if(this.isString(value)) {
                        request += `${key}: "${itemParams.filterParams[key]}", `
                    } else {
                        throw new Error('Invalid filter query parameter type');
                    }
                });
                request = request.slice(0, -2); // remove last , 
                request += '}, ';
            }

            if(!!itemParams.pagination) {
                request += `page: ${itemParams.pagination?.page}, perPage: ${itemParams.pagination?.rowsPerPage}, `
            }

            if(!!itemParams.sorting) {
                request += `sortField: "${itemParams.sorting?.sortField}", sortOrder: "${itemParams.sorting?.sortOrder}", `
            }

            request = request.slice(0, -2); // remove last , 

            request += ')';
        }        
        
        request = this.shapeQueryWithKeys(request, itemParams.keys ?? this.defaultKeys);                                  
        return request;
    }

    multipleResult = (
        params?: {
            queryParams?: object, 
            pagination?: { page: number, rowsPerPage: number }, 
            sorting?: { sortField: string, sortOrder: SortDirection },
            keys?: Array<keyof T>
        }) : {query: DocumentNode, resultTransformer: (data: object) => Array<T>} => {

        const itemParams = {
            filterParams: params?.queryParams,
            pagination: params?.pagination,
            sorting: params?.sorting,
            keys: params?.keys
        };
        let request = `{ 
            ${this.formQueryItem(itemParams)}
        }`;        
        const resultTransformer = (data: object) : Array<T> => data[this.listCollection];

        return {
            query: gql`${request}`,
            resultTransformer: resultTransformer
        };
    }

    retrieveMultiple = async (
        params?: {
            queryParams?: object,
            pagination?: {page: number, rowsPerPage: number}, 
            sorting?: { sortField: string, sortOrder: SortDirection },
            keys?: Array<keyof T>
        }
    ) : Promise<Array<T>> => {
        const {query, resultTransformer} = this.multipleResult(params);        
        return toJS(resultTransformer(await this.graphQLClient.fetch(query)));
    }

    static build : () => object
}