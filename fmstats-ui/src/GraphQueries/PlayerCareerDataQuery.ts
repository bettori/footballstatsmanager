import gql from 'graphql-tag';
import { BasicDataQuery } from './BasicDataQuery';
import { PlayerCareerData } from '../objects/Player/PlayerData';
import { DocumentNode } from 'graphql';

export class PlayerCareerDataQuery extends BasicDataQuery<PlayerCareerData> {
    singleResultProperty: string = 'PlayerCareerDatum';
    listCollection: string = 'allPlayerCareerData';

    multipleResult = (
        params?: {
            queryParams?: {ids?: Array<string>, category: string}, 
            pagination?: {page: number, rowsPerPage: number},
            keys?: Array<keyof PlayerCareerData>
        }) : {query: DocumentNode, resultTransformer: (data: object) => Array<PlayerCareerData>} => {
            const {ids, category} = params?.queryParams;
            if(!!ids && ids.length > 0) {            
                let request = `{`;
                ids.forEach(id => {
                    request += this.formQueryItem(
                        {
                            alias: `${this.listCollection}${id}${category}`,
                            filterParams: {id: id, category: category},
                            pagination: params?.pagination,
                            keys: params?.keys
                        });
                    request += '\n';
                })

                request += '}';
                console.log(request);
                const gRequest = gql`${request}`;
                const resultTransformer = (data: object) : Array<PlayerCareerData> => {
                    return ids.flatMap(id => data[`${this.listCollection}${id}${category}`]);
                }

                return {
                    query: gRequest,
                    resultTransformer: resultTransformer
                };
            } else {
                return super.multipleResult(params);
            }        
    };
    
    static build = () : PlayerCareerDataQuery => {
        const careerData : PlayerCareerData = {
            id: '',
            category: '',
            statistics: []
        }

        return new PlayerCareerDataQuery(careerData);
    }
}