import gql from 'graphql-tag';
import { BasicDataQuery } from './BasicDataQuery';
import { DocumentNode } from 'graphql';
import { Player } from '../objects/Player/Player'

export class PlayerQuery extends BasicDataQuery<Player> {
    constructor(refObject: Player) {
        super(refObject);
    }

    singleResultProperty: string = 'Player';
    listCollection: string = 'allPlayers';

    static build = () : PlayerQuery => {
        const player : Player = {
            id: '',
            firstName: '',
            lastName: '',
            age: 0,
            height: '',
            weight: '',
            nationality: '',
            position: '',
            currentTeam: ''
        }
    
        return new PlayerQuery(player);
    }
}