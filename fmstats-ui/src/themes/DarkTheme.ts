import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

const DarkTheme : ThemeOptions = {
    palette: {
        type: 'dark',
        primary: {
            contrastText: '#c0b283',
            main: '#373737'
        },
        secondary: {
            main: '#dcd0c0'
        },
        background: {
            default: '#373737'
        },
        text: {
            primary: '#c0b283',
            secondary: 'rgb(192, 178, 131, 0.6)'
        }            
    },
    typography: {
        body2: {
            fontSize: '0.75rem'
        }
    }
};

export default DarkTheme;
