import { BasicDataQuery } from '../GraphQueries/BasicDataQuery';
import gql from 'graphql-tag';

export interface StatDomain {
    domain: string
    category: Array<StatCategory>
}


interface StatCategory {
    name: string
    description: string
    min: number
    max: number
    categoryCode: string
}

export {StatCategory}

export class StatDomainQuery extends BasicDataQuery<StatDomain> {
    singleResultProperty = 'StatDomain';
    listCollection = 'allStatDomains'
    multipleResult = () =>  {
        const query = gql`query StatDomains {
            ${this.listCollection} {
                domain
                category
            }
        }`;
        const resultTransformer = (data: object) => data[this.listCollection];
        return {
            query: query,
            resultTransformer: resultTransformer            
        }
    }
    
    static build = () : StatDomainQuery => {
        const statDomain = {
            domain: '',
            category: []
        };

        return new StatDomainQuery(statDomain);
    }
}