import {BasicDataQuery} from '../../GraphQueries/BasicDataQuery'
import gql from 'graphql-tag';

export interface Season {
    id: number
    leagueId: number
    label: string
}

export class SeasonQuery extends BasicDataQuery<Season> {
    static listCollection = 'allSeasons'
    static multipleResult = (params: {leagueId: number}) => { 
        return gql`${SeasonQuery.listCollection}(filter: {leagueId: ${params.leagueId}}) {
            id
            leagueId
        }`;
    }
}