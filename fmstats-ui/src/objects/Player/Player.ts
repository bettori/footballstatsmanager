export interface Player {
    id: string
    firstName: string
    lastName: string
    age: number
    height: string
    weight: string
    nationality: string
    position: string
    currentTeam: string
}
