import { Season } from '../Season/Season';

export enum ComparisonType {
    Attack = 'attack',
    Defense = 'defense',
    Midfield = 'midfield',
    Goalkeeping = 'goalkeeping',
    Summary = 'summary'
}

export interface PlayerAggregateStats {
    id: string
    category: ComparisonType
    season: Season
    statistics: Array<PlayerAggregateStatistic>
}

export interface PlayerAggregateStatistic {
    value: number,   
    categoryCode: string
}

export const CareerStatFilter = {
    summary: {
        code: 'summary',
        label: 'Summmary'
    },
    attack: {
        code: 'attack',
        label: 'Attack'  
    },
    defense: {
        code: 'defense',
        label: 'Defense'
    },
    passing: {
        code: 'passing',
        label: 'Passing'
    }    
}