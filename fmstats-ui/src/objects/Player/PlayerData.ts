import { PlayerAggregateStatistic } from './PlayerStats';
import { Player } from './Player';

export class PlayerData {
    id: String
    statistics: Array<PlayerAggregateStatistic>
    firstName: String
    lastName: String
}

export interface PlayerCareerData {
    id: string
    category: string
    statistics: Array<PlayerSeasonData & object>
}

// Base properties that each career data domain should have
export interface PlayerBaseData {
    club: string
    league: string
    appearances: number
    minutes: number    
}

export type PlayerSeasonData = PlayerBaseData & {season: string}

export interface PlayerSummaryData {
    goals: number
    assists: number
    yellowCards: number
    redCards: number
    pointsPerGame: number
}

export interface PlayerCareerOverviewData {
    player: Player
    careerStatistics: {appearances: number, minutes: number} & PlayerSummaryData
}