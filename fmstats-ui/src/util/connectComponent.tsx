import { observer, inject } from 'mobx-react';
import { ProvidedStores } from '../stores/ProvidedStores';

export function connectComponent<F, C, S extends Omit<F, keyof C>>(storeConnector: (stores: ProvidedStores) => C, component: React.ComponentType<F & C>) : React.ComponentType<S> {
    const newComponent = (inject(storeConnector)(observer(component)) as any);
    return newComponent;
}