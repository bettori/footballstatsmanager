import { RootStore } from './RootStore';

export class BaseStore {
    protected rootStore: RootStore;
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }
}