import { RootStore } from './RootStore';

export interface ProvidedStores {
    rootStore: RootStore
}