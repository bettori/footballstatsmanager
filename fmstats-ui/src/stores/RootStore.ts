import { PlayerStore } from './domain/PlayerStore';
import { ConcreteServiceClient } from '../serviceClients/ServiceClient';
import { StatisticsStore } from './domain/StatisticsStore';
import { PlayerCareerSummaryStore } from './UI/PlayerCareerSummaryStore';
import { GraphQLClient } from '../serviceClients/GraphQlClient';
import { RadarComparatorStore } from './UI/RadarComparatorStore';
import { ItemTableStore } from './UI/ItemTableStore';
import { PlayerListPageStore } from './UI/PlayerListPageStore';

export class RootStore {
    static serviceClient = new ConcreteServiceClient();
    static graphQLClient = new GraphQLClient();

    // Domain stores
    playerStore: PlayerStore = new PlayerStore(this);
    statisticsStore: StatisticsStore = new StatisticsStore(this);

    // UI stores
    playerCareerSummaryStore: PlayerCareerSummaryStore = new PlayerCareerSummaryStore(this);
    radarComparatorStore: RadarComparatorStore = new RadarComparatorStore(this);
    itemTableStore: ItemTableStore = new ItemTableStore(this);
    playerListPageStore: PlayerListPageStore = new PlayerListPageStore(this);ß
}