import { observable, action, computed, toJS, observe } from 'mobx';
import { PlayerAggregateStats, ComparisonType } from '../../objects/Player/PlayerStats';
import { PlayerAggregateStatQuery } from '../../GraphQueries/PlayerAggregateStatQuery';
import { RootStore } from '../RootStore';
import { StatDomain, StatDomainQuery } from '../../objects/StatDomain';
import { Season } from '../../objects/Season/Season';

type PlayerSeasonKey = string
type PlayerId = string
type SeasonId = number
type PlayerSeasonTuple = [SeasonId, PlayerId]

export class StatisticsStore {
    private knownObjects : Map<ComparisonType, Map<PlayerSeasonKey, PlayerAggregateStats>> = new Map();
    private rootStore: RootStore;
    
    constructor(rootStore: RootStore)  {
        this.rootStore = rootStore;
    }

    // Radar comparison stats
    @observable statisticDomains: Array<StatDomain> = [];
    @observable isSyncing: Boolean = false;   
    @observable selectedPlayerSeason: Season;
    @observable comparisonPlayerSeason: Season; 

    private getKnownStatsByIdsAndComparison = (keys: Array<PlayerSeasonKey>, comparisonType: ComparisonType) : Array<PlayerAggregateStats> => {        
        let statsMap : Map<PlayerSeasonKey, PlayerAggregateStats> = this.knownObjects.get(comparisonType);
        const allKeysInMap = (hasKeys: boolean, key: PlayerSeasonKey) => {
            return statsMap.has(key) && hasKeys
        };
        
        if(!statsMap || !keys.reduce(allKeysInMap, true)) {
            return [];
        }

        const playerStatistics = keys.filter(key => statsMap.has(key)).map(key => statsMap.get(key));
        return playerStatistics;
    }

    private updatePlayerStatistics = (stats: Array<PlayerAggregateStats>, comparisonType: ComparisonType) => {
        const currentMap = this.knownObjects.get(comparisonType);

        if(!!currentMap) {
            stats.forEach(stat => {                
                currentMap.set(`${stat.season.id}, ${stat.id}`, stat);                
            });
            this.knownObjects.set(comparisonType, currentMap);
        } else {
            const requestedStatsMap : Map<PlayerSeasonKey, PlayerAggregateStats> = new Map(stats.map(s => [`${s.season.id}, ${s.id}`, s]));        
            this.knownObjects.set(comparisonType, requestedStatsMap);
        }
    }

    @computed
    get currentStatisticsDomain() {
        return this.statisticDomains;
    }

    @computed 
    get syncInProgress() {
        return this.isSyncing;
    }    
    
    @action.bound
    setStatsDomains(domains: Array<StatDomain>) {
        this.statisticDomains = domains;
    }
   
    toPlayerSeasonKey = (seasonId: number, playerId: string) : PlayerSeasonKey => {
        return `${seasonId}, ${playerId}`;
    }

    toKeysFromTuples = (playerSeasonTuples: [PlayerSeasonTuple, PlayerSeasonTuple]) : [PlayerSeasonKey, PlayerSeasonKey] => {
        return [
            this.toPlayerSeasonKey(playerSeasonTuples[0][0], playerSeasonTuples[0][1]),
            this.toPlayerSeasonKey(playerSeasonTuples[1][0], playerSeasonTuples[1][1])
        ];
    }

    @action.bound
    async getPlayerStats(tuples: [PlayerSeasonTuple, PlayerSeasonTuple], comparisonType: ComparisonType) : Promise<Array<PlayerAggregateStats>> {
        const keys : [PlayerSeasonKey, PlayerSeasonKey] = this.toKeysFromTuples(tuples);                     
        let cachedStats = this.getKnownStatsByIdsAndComparison(keys, comparisonType);

        if(!cachedStats || cachedStats.length === 0) {
            // update player stats list
            await this.retrievePlayerStats(tuples.map(tuple => tuple[1]), comparisonType);
            cachedStats = this.getKnownStatsByIdsAndComparison(keys, comparisonType);
        }

        return cachedStats;
    }

    @action.bound
    async retrievePlayerStatsByCategory(category: ComparisonType) : Promise<Array<PlayerAggregateStats>> {
        const cachedStats = Array.from(this.knownObjects.get(category).values());

        if(!cachedStats || cachedStats.length === 0) {
            const playerAggregateStatsQuery = PlayerAggregateStatQuery.build();
            const requestedStats = await playerAggregateStatsQuery.retrieveMultiple({queryParams: {category: category}});
            this.updatePlayerStatistics(requestedStats, category);
            return requestedStats;
        }

        return cachedStats;
    }

    @action.bound
    async retrieveStatsDomains() : Promise<Array<StatDomain>> {
        try {
            this.isSyncing = true;            
            const statQuery : StatDomainQuery = StatDomainQuery.build();
            const requestedDomains : Array<StatDomain> = await statQuery.retrieveMultiple();                 
             this.setStatsDomains(requestedDomains);
             return this.statisticDomains;
        }
        finally {
            this.isSyncing = false;
        }
    }

    async retrievePlayerStats(ids: Array<string>, comparisonType: ComparisonType) : Promise<Array<PlayerAggregateStats>> {        
        const playerAggregateStatsQuery = PlayerAggregateStatQuery.build();
        const params= { 
            queryParams: {
                ids: ids,
                category: comparisonType
            }
        };
        let requestedStats: Array<PlayerAggregateStats> = await playerAggregateStatsQuery.retrieveMultiple(params);
        this.updatePlayerStatistics(requestedStats, comparisonType);
        return requestedStats;
    }
}