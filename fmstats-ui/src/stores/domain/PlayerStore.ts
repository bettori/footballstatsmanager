import { observable, action, computed, toJS } from 'mobx';
import { Player } from '../../objects/Player/Player';
import { PlayerQuery } from '../../GraphQueries/PlayerQuery';
import { RootStore } from '../RootStore';
import { PlayerCareerDataQuery } from '../../GraphQueries/PlayerCareerDataQuery';
import { PlayerCareerData } from '../../objects/Player/PlayerData';

export class PlayerStore {   

    private rootStore: RootStore;

    constructor(rootStore: RootStore)  {
        this.rootStore = rootStore;
    }
    
    @observable playerBios: Array<Player> = [];    
    @observable currentComparison: string

    @computed
    get players() {
        return this.playerBios;
    }    

    @action.bound
    setPlayerBios(playerNames: Array<Player>) {
        this.playerBios = playerNames;
    }

    getPlayer(id: String) : Player {
        return this.playerBios.find(playerName => playerName.id === id);
    }
    
    // Retrieve data from server
    @action.bound
    async retrievePlayerCareerData(id: string, comparison: string): Promise<PlayerCareerData> {                
        const queryParams = {
            ids: [id],
            category: comparison
        };
        const playerDataQuery = PlayerCareerDataQuery.build();
        const playerStats = await playerDataQuery.retrieveMultiple({queryParams: queryParams, keys: ['id', 'statistics']});        
        return playerStats[0];            
    }

    @action.bound
    async retrievePlayerBios(pagination?: {page: number, rowsPerPage: number}) : Promise<void> {
        const playerQuery = PlayerQuery.build();        
        const playerNames = await playerQuery.retrieveMultiple({pagination: pagination});
        
        this.setPlayerBios(playerNames);
    }

    @action.bound
    async retrievePlayerBio(id: string) : Promise<Player> {
        const playerQuery = PlayerQuery.build();
        const player : Player = await playerQuery.retrieveSingle(id);
        return player;
    }

}