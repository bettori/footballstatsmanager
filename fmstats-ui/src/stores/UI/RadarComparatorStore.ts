import { observable, action, computed } from 'mobx';
import { RootStore } from '../RootStore';
import { toJS } from 'mobx';
import { Player } from '../../objects/Player/Player';
import { ComparisonType, PlayerAggregateStats } from '../../objects/Player/PlayerStats';
import { Season } from '../../objects/Season/Season';

type PlayerId = string
type SeasonId = number
type PlayerSeasonTuple = [SeasonId, PlayerId]

export class RadarComparatorStore {
    private rootStore: RootStore;
    radarComparisonType: ComparisonType = ComparisonType.Attack;

    @observable selectedPlayerSeason: Season;
    @observable comparisonPlayerSeason: Season;
    @observable comparisonStats: Array<PlayerAggregateStats>
    @observable allStatsForCategory: Array<PlayerAggregateStats> = [];
    @observable selectedPlayer: Player
    @observable comparisonPlayer: Player
    
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    get currentComparisonType() {
        return {
            label: this.radarComparisonType,
            value: this.radarComparisonType
        }
    }

    @computed
    get radarPlayerData() {
        return (this.comparisonStats ?? []).map(stat => {
            const player = this.rootStore.playerStore.playerBios.find(player => player.id === stat.id);
            
            return {
                id: player.id,
                firstName: player.firstName,
                lastName: player.lastName,
                statistics: toJS(stat.statistics)
            };
          });
    }

    @computed
    get selectedPlayerSeasonDisplayList() {
        return this.allStatsForCategory.filter(stat => stat.id === this.selectedPlayer.id).map(stat => {
            return {
                label: stat.season.label,
                value: stat.season.id
            }
        });        
    }

    @computed
    get comparisonPlayerSeasonDisplayList() {
        return this.allStatsForCategory.filter(s => s.id === this.comparisonPlayer.id).map(pStats => {
            return {
                    label: pStats.season.label, 
                    value: pStats.season.id
                };
        });
    }

    @computed
    get selectedPlayerSeasonDisplay() {
        return {
            label: this.selectedPlayerSeason?.label,
            value: this.selectedPlayerSeason?.id
        }
    }

    @computed
    get comparisonPlayerSeasonDisplay() {
        return {
            label: this.comparisonPlayerSeason?.label,
            value: this.comparisonPlayerSeason?.id
        }
    }

    @computed
    get playerSelected() {
        return {
            label: !!this.selectedPlayer ? `${this.selectedPlayer.lastName}, ${this.selectedPlayer.firstName}` : '',
            value: !!this.selectedPlayer ? this.selectedPlayer.id : ''
        }
    }

    @computed
    get playerCompared() {
        return {
            label: !!this.comparisonPlayer ? `${this.comparisonPlayer.lastName}, ${this.comparisonPlayer.firstName}` : '',
            value: !!this.comparisonPlayer ? this.comparisonPlayer.id : ''
        }
    }

    @action.bound
    async initializeStore(player: Player) : Promise<void> {
        this.selectedPlayer = player;
        await this.rootStore.playerStore.retrievePlayerBios();
        await this.rootStore.statisticsStore.retrieveStatsDomains();
        const selectedPlayer = this.playerSelected;
        const playerNames = this.rootStore.playerStore.players.filter(
            player => player.id !== selectedPlayer.value
        );
        const comparisonPlayer = playerNames[0];
        const [selectedPlayerId, comparisonPlayerId] = [selectedPlayer.value, comparisonPlayer.id];
        this.comparisonPlayer = comparisonPlayer;
        this.allStatsForCategory = await this.rootStore.statisticsStore.retrievePlayerStats([selectedPlayerId, comparisonPlayerId], this.radarComparisonType);
        const [selectedPlayerStats, comparisonPlayerStats] = [this.allStatsForCategory.find(s => s.id === selectedPlayerId), this.allStatsForCategory.find(s => s.id === comparisonPlayerId)];
        this.selectedPlayerSeason = selectedPlayerStats.season;
        this.comparisonPlayerSeason = comparisonPlayerStats.season;
        this.comparisonStats = [selectedPlayerStats, comparisonPlayerStats];
    }

    @action.bound
    async updatePlayerData(playerId: string) : Promise<void> {
        const keys : [PlayerSeasonTuple, PlayerSeasonTuple] =  [
            [this.selectedPlayerSeason.id, this.selectedPlayer.id], 
            [this.comparisonPlayerSeason.id, playerId]
        ];
        const updatedStats = await this.rootStore.statisticsStore.getPlayerStats(keys, this.radarComparisonType);        
        const playerCompared = this.rootStore.playerStore.getPlayer(playerId);
        [this.comparisonPlayer, this.comparisonStats] = [playerCompared, updatedStats];
        this.allStatsForCategory = await this.rootStore.statisticsStore.retrievePlayerStatsByCategory(this.radarComparisonType);
    }

    @action.bound
    async updateDataByComparisonType(comparisonType: ComparisonType) {
        const keys : [PlayerSeasonTuple, PlayerSeasonTuple] = [ 
            [this.selectedPlayerSeason.id, this.selectedPlayer.id], 
            [this.comparisonPlayerSeason.id, this.comparisonPlayer.id]
        ];
        const comparedStatistics = await this.rootStore.statisticsStore.getPlayerStats(keys, comparisonType);
        [this.radarComparisonType, this.comparisonStats] = [comparisonType, comparedStatistics];
        this.allStatsForCategory = await this.rootStore.statisticsStore.retrievePlayerStatsByCategory(comparisonType);
    }

    @action.bound
    async updateStatsBySeason(seasonId: number) {
        const keys : [PlayerSeasonTuple, PlayerSeasonTuple] = [
            [seasonId, this.selectedPlayer.id],
            [this.comparisonPlayerSeason.id, this.comparisonPlayer.id]
        ];
        const statistics = await this.rootStore.statisticsStore.getPlayerStats(keys, this.radarComparisonType);
        const season = statistics.find(s => s.season.id === seasonId).season;
        [this.selectedPlayerSeason, this.comparisonStats] = [season, statistics];
    }

    @action.bound
    async updateComparisonStatsBySeason(seasonId: number) {
        const keys : [PlayerSeasonTuple, PlayerSeasonTuple] = [
            [this.selectedPlayerSeason.id, this.selectedPlayer.id],
            [seasonId, this.comparisonPlayer.id]        
        ];
        const statistics = await this.rootStore.statisticsStore.getPlayerStats(keys, this.radarComparisonType);
        const season = statistics.find(s => s.season.id === seasonId).season;
        [this.comparisonPlayerSeason, this.comparisonStats] = [season, statistics];
    }
}