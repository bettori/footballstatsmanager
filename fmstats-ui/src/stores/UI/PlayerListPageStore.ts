import { observable, computed, action } from 'mobx';
import { RootStore } from '../RootStore';
import { BaseStore } from '../BaseStore';
import { PlayerSummaryData, PlayerCareerData, PlayerBaseData, PlayerSeasonData } from '../../objects/Player/PlayerData';
import { Player } from '../../objects/Player/Player';
import { PlayerCareerOverviewData } from '../../objects/Player/PlayerData';
import { ComparisonType } from '../../objects/Player/PlayerStats';
import { PaginatedArray } from '../../collections/PaginatedArray';
import { SortDirection } from './ItemTableStore';
import { PlayerQuery } from '../../GraphQueries/PlayerQuery';

export class PlayerListPageStore extends BaseStore {

    @observable playerList: Array<PlayerCareerOverviewData>

    constructor(rootStore: RootStore) {
        super(rootStore);        
    }

    private calculateCareerStats = (summary: PlayerBaseData & PlayerSummaryData, nextStat:  PlayerSeasonData & object) : PlayerBaseData & PlayerSummaryData => {
        Object.keys(summary).forEach(key => {
            switch(key) {
                case 'pointsPerGame': {
                    summary[key] = (summary[key] + nextStat[key]) / 2;
                    break;
                }
                default: {
                    summary[key] += nextStat[key];
                    break;
                }
            }
        });

        return summary;
    }

    retrievePlayerSummaryData = async (playerId: string) : Promise<PlayerCareerOverviewData> => {
        const player : Player = await this.rootStore.playerStore.retrievePlayerBio(playerId);
        const playerCareerData: PlayerCareerData = await this.rootStore.playerStore.retrievePlayerCareerData(playerId, ComparisonType.Summary);
        

        let careerStatSummary = {
            appearances: 0,
            minutes: 0,
            goals: 0,
            assists: 0,
            yellowCards: 0,
            redCards: 0,
            pointsPerGame: playerCareerData.statistics[0]['pointsPerGame']
        }

        const playerCareerStats = playerCareerData.statistics.reduce(this.calculateCareerStats, careerStatSummary); 

        return {
            player: player,
            careerStatistics: playerCareerStats
        };
    }

    retrievePlayerListPage = async (page: number, rowsPerPage: number, sortColumn?: string, sortOrder?: SortDirection) : Promise<PaginatedArray<PlayerCareerOverviewData>> => {
        const query = PlayerQuery.build();
        const params = {
            pagination: {
                page: page,
                rowsPerPage: rowsPerPage
            }
        }
        
        if(!!sortOrder && !!sortColumn) {
            params['sorting'] = {
                sortField: sortColumn,
                sortOrder: sortOrder
            }
        }
        
        const data = await query.retrieveMultiple(params);

        return {
            count: 3,
            currentPage: page,
            rowsPerPage: rowsPerPage,
            data: data.map(datum => {
                return {
                    player: datum,
                    careerStatistics: null
                }
            })
        }
    }


}