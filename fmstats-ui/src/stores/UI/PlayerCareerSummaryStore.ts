import { observable, action, computed } from 'mobx';
import { RootStore } from '../RootStore';
import { PlayerCareerData } from '../../objects/Player/PlayerData';
import { BaseStore } from '../BaseStore';

export class PlayerCareerSummaryStore extends BaseStore {
    @observable selectedStatView: string
    @observable playerData: Array<object>
    @observable selectedPlayerCareerData: PlayerCareerData

    constructor(rootStore: RootStore) {
        super(rootStore);
        this.selectedStatView = 'summary';
    }

    @computed
    get playerCareerData() {
        return this.selectedPlayerCareerData;
    }

    @action.bound
    async retrieveCareerSummaryData(id: string, comparison: string): Promise<PlayerCareerData> {
        if (!this.selectedPlayerCareerData || this.selectedPlayerCareerData.id !== id || this.selectedStatView !== comparison) {
            this.selectedStatView = comparison;
            const playerData = await this.rootStore.playerStore.retrievePlayerCareerData(id, comparison);
            this.selectedPlayerCareerData = playerData;
            return playerData;
        }

        return this.selectedPlayerCareerData;
    }
}