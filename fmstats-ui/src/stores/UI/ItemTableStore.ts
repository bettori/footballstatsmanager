import { observable, action } from 'mobx';
import { RootStore } from '../RootStore';

export enum SortDirection {
    Ascending = 'asc',
    Descending = 'desc'
}

export class ItemTableStore {
    private rootStore: RootStore
    @observable itemCount: number = 1;
    @observable currentPage: number = 0;
    @observable rowsPerPage: number;    
    @observable sortByColumn: string;
    @observable sortDirection: SortDirection;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    @action
    setItemCount = (count: number) => {
        this.itemCount = count;
    }

    @action
    setCurrentPage = (page: number) => {
        this.currentPage = page;
    }

    @action
    setRowsPerPage = (rowsPerPage: number) => {
        this.rowsPerPage = rowsPerPage;
    }

    @action
    setSortByColumn = (column: string) => {
        this.sortByColumn = column;
    }

    @action
    setSortDirection = (direction: SortDirection) => {
        this.sortDirection = direction;
    }
    
}