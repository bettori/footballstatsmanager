
import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { useLocalStore, inject, observer } from 'mobx-react';
import { CSSProperties } from '@material-ui/styles';
import { Typography } from '@material-ui/core';
import { ProvidedStores } from '../stores/ProvidedStores';

interface ImportViewProps {}
interface ImportViewLocalState {
    outcomeLog: string
    updateOutcomeLog: (log: string) => void
}

export function ImportView(props: ImportViewProps) {
    const style : CSSProperties = {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '20px',
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: '#bdbdbd',
        outline: 'none',
        transition: 'border .24s ease-in-out'
    };

    const localState = useLocalStore(() : ImportViewLocalState => (
        {
            outcomeLog: '',
            updateOutcomeLog: (log: string) => {
                localState.outcomeLog = log;
            }
        })
    )

    const onDrop = useCallback(acceptedFiles => {
        console.log(acceptedFiles);
        localState.updateOutcomeLog('Files dragged and dropped\nSuccessfully parsed file\nData loaded in database');
    }, []);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop: onDrop,
        accept: 'text/csv, application/vnd.oasis.opendocument.spreadsheet'
    });

    const messageDisplay = (isDragActive: boolean) => {
        if(isDragActive) {
            return (
                <p>Drop the files here...</p>        
            )
        } else {
            return (
                <p>Drag n drop files here, or click to select files</p>
            )
        }
    }

    return (
        <div>
            <Typography
                color='textPrimary'
                variant='h3'
                gutterBottom={true}
            >
                {'Import Player Statistics'}
            </Typography>
            <section>
                <div {...getRootProps({style})}>
                    <input {...getInputProps()}/>
                    {messageDisplay(isDragActive)}
                </div>
            </section>
            <Typography
                variant='overline'
            >
                {'Results: '}
            </Typography>
            <Typography
                paragraph={true}
                display='block'
            >
                {localState.outcomeLog}
            </Typography>
        </div>
    );
}

const storeConnector = (stores: ProvidedStores) : any => ({})

export default inject(storeConnector)(observer(ImportView));