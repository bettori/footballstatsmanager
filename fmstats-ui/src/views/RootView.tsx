import React from 'react';
import { RootStore } from '../stores/RootStore';
import { Provider } from 'mobx-react';
import PlayerStatsView from './PlayerStatsView';
import ImportView from './ImportView';
import { Route, Switch} from 'react-router-dom';
import { observer } from 'mobx-react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import DarkTheme from '../themes/DarkTheme';
import PlayerListPage from '../components/listPages/PlayerListPage';
import Layout from '../components/layout/Layout';


@observer
export class RootView extends React.PureComponent {
    private rootStore: RootStore = new RootStore();
    private appTheme : any = createMuiTheme(DarkTheme);    

    playerDetailView = ({match}) => {
        return (
            <PlayerStatsView playerId={match.params.id}/>
        )
    }

    playerListView = ({match}) => {
        return (
            <PlayerListPage/>
        );
    }

    importView = ({match}) => {
        return (
            <ImportView/>
        );
    }

    render() {          
        console.log(this.appTheme);      
        return (
            <main>                              
                <MuiThemeProvider theme={this.appTheme}>
                    <Provider rootStore={this.rootStore}>
                        <Layout> 
                            <Switch>
                                <Route exact={true} path='/' component={this.importView}/>
                                <Route path='/players/:id' component={this.playerDetailView}/>
                                <Route path='/players' component={this.playerListView}/>
                                <Route path='/import' component={this.importView}/>
                                <Route component={this.importView}/>                            
                            </Switch>
                        </Layout>
                    </Provider>
                </MuiThemeProvider>                                
            </main>
        )
    }
}