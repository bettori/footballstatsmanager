import React from 'react';
import PlayerBio from '../components/presentation/PlayerBio';
import RadarComparator from '../components/presentation/RadarComparator';
import {RootStore} from '../stores/RootStore';
import { Player } from '../objects/Player/Player';
import PlayerCareerSummary from '../components/presentation/PlayerCareerSummary';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { inject, observer } from 'mobx-react';
import { StyledComponentProps, withStyledComponent } from '../components/presentation/StyledComponent';

interface PlayerStatsViewProps {
    playerId: string
    rootStore?: RootStore
}

type StyledPlayerStatsViewProps = PlayerStatsViewProps & StyledComponentProps

@inject('rootStore')
@observer
export class PlayerStatsView extends React.Component<StyledPlayerStatsViewProps, {value: number, player: Player}> {

    static styles = theme => ({
        appBarRoot: {
            marginTop: '1%'
        }
    });

    constructor(props: StyledPlayerStatsViewProps) {
        super(props);
        this.state = {value: 0, player: null};
    }

    async componentDidMount() {
        const { rootStore, playerId } = this.props;
        await rootStore.playerStore.retrievePlayerBios();
        const player = rootStore.playerStore.getPlayer(playerId);
        this.setState({player: player});
    }

    handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        this.setState({value: newValue})
    }

    displayCareerStats = (display: boolean, player: Player) => {
        return display ? 
        (                                             
            <PlayerCareerSummary
                player={player}
            />           
        ) : null;
    }

    displayRadar = (display: boolean, player: Player) => {
        return display ? 
        (           
            <RadarComparator
                player={player}
            />           
        ) : null;
    }

    render() {
        const { styleClasses } = this.props;
        const { player } = this.state;

        if(!!player) {
            return (
                <div className={styleClasses.playerView}>
                    <PlayerBio
                        player={player}
                    />
                    <AppBar position='static' square={false} classes={{root: styleClasses.appBarRoot}}>
                        <Tabs value={this.state.value} onChange={this.handleChange}>
                            <Tab label='Career Stats'/>
                            <Tab label='Comparison'/>
                        </Tabs>
                    </AppBar>
                    {this.displayCareerStats(this.state.value === 0, player)}
                    {this.displayRadar(this.state.value === 1, player)}        
                </div>
            )
        } else {
            return (<div>Waiting...</div>);
        }
        
    }
}

export default withStyledComponent(PlayerStatsView, PlayerStatsView.styles);