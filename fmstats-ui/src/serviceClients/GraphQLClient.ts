import ApolloClient, { DocumentNode, OperationVariables } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export class GraphQLClient {
    private uri: string = `${process.env.GRAPHQL_SERVER}/graphql`;    

    private client = new ApolloClient({uri: this.uri});
    
    fetch = (request: DocumentNode, variables?: OperationVariables) : Promise<any> => {
        return this.client.query({
            query: request,
            variables: variables,
            fetchPolicy: 'no-cache'
        }).then(res => res.data);
    }
}