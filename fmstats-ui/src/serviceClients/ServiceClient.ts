export interface ServiceClient {
    fetch: (url: String, params: object) => Promise<any>
}

export class ConcreteServiceClient implements ServiceClient {
    fetch = (url: string, params?: object) : Promise<any> => {
        return fetch(url)
        .then(res => res.json())
    }
}