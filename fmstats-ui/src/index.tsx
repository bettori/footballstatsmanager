import React from 'react';
import ReactDOM from 'react-dom';
import { RootView } from './views/RootView';
import { BrowserRouter as Router} from 'react-router-dom';

ReactDOM.render(<Router><RootView/></Router>, document.getElementById('root'));