import React from 'react';
import { VictoryChart, VictoryPolarAxis, VictoryLabel, VictoryArea, VictoryGroup, VictoryTheme, VictoryLegend } from 'victory';
import { format } from 'd3-format';
import { StatCategory, StatDomain } from '../../objects/StatDomain';
import { PlayerData } from '../../objects/Player/PlayerData';
import { StyledComponentProps, withStyledComponent } from '../../components/presentation/StyledComponent';
import { Tooltip } from '@material-ui/core';

type StatsRadarProps = {
    data: Array<PlayerData>
    domain: StatDomain
}

type StyledRadarProps = StatsRadarProps & StyledComponentProps

export class StatsRadar extends React.Component<StyledRadarProps> {  

    static styles : object = {
        polarAxisOne: {
            axisLabel: { padding: 5 },
            axis: { stroke: 'none'},
            grid: { stroke: 'grey', strokeWidth: 0.25, opacity: 0.5 },        
        },
        polarAxisTwo: {
            axis: { stroke: 'grey' },
            grid: { stroke: 'grey', opacity: 0.5 }
        },
        legend: {
            border: { stroke: 'black' }, 
            title: {fontSize: 10 }, 
            labels: {fontSize: 8} 
        }, 
        chart: {
            padding: {top: 40, right: 50, bottom: 40}
        }        
    }

    getDataRangeForCategory = (data: Array<PlayerData>, category: StatCategory) : StatCategory => {
        const relevantStats = data.flatMap(datum => datum.statistics.filter(stat => stat.categoryCode === category.categoryCode))
        .map(stat => stat.value);
        category.min = Math.min(category.min, ...relevantStats);
        category.max = Math.max(category.max, ...relevantStats);
        return category;
    }

    adjustStatCategoryDisplayScale = (data: Array<PlayerData>, categories: Array<StatCategory>) : Array<StatCategory> => {
        return categories.map(category => {
            return this.getDataRangeForCategory(data, category);
        });
    }

    transformDataForDisplay = (data: Array<PlayerData>, domains: Array<StatCategory>) : Array<object> => {
        const displayScale = this.adjustStatCategoryDisplayScale(data, domains);
        const makeDataArray = (d: PlayerData) : Array<object> => {
            const result = d.statistics.map(stat => {                
                const dom = displayScale.find(dm => dm['categoryCode'] === stat.categoryCode);
                if(!!dom) {
                    return { x: dom['name'], y: (stat.value - dom['min']) / (dom['max'] - dom['min'])};
                } else {
                    // data categories do not match up with current domain      
                    return {x: 0, y: 0};
                }
            });
            return result;
        };

        return data.map((datum) => makeDataArray(datum));
    }

    extractLegendFromData = (data: Array<PlayerData>) : Array<object> => {
        return data.map(datum => {
            return {
                name: `${datum.lastName}, ${datum.firstName}`
            };
        });
    }

    formatTicks = (t: any) => {              
        const formattedTick = format('.2r')(t);        
        return formattedTick;        
    }

    generateAreaPlotFromData = (data: Array<object>) : Array<JSX.Element> => {
        return data.map((datum, i) => {            
            return <VictoryArea key={i} data={datum}/>;
        })        
    }

    generatePolarAxesFromDomains = (categories: Array<StatCategory>) : Array<JSX.Element> => {
        const { getStyle, data } = this.props;
        const tickFormatter = (category: StatCategory) => (tick: any) => {
            const {min, max} = this.getDataRangeForCategory(data, category);
            return format('.2')(min + tick*(max - min));
        };

        return categories.map((domain, i) => {
            const axisAngle = 10 + i*(360/(categories.length-1));
            return (                
                <VictoryPolarAxis 
                    key={i}
                    axisLabelComponent={<Tooltip title={domain.description} key={domain.name}><VictoryLabel/></Tooltip>}
                    dependentAxis={true}
                    style={getStyle(['polarAxisOne'])}
                    tickLabelComponent={<VictoryLabel labelPlacement='vertical'/>}
                    labelPlacement='perpendicular'
                    axisValue={i+1} 
                    label={domain.name}                            
                    tickFormat={tickFormatter(domain)}
                    tickValues={[0.25, 0.5, 0.75, 1, 1.25, 2]}
                />                                    
            );
        })
    }

    generateParallelPolaxAxis = () : JSX.Element => {
        const { getStyle } = this.props;
        const tickFormatter = () => '';
        return (
            <VictoryPolarAxis
                labelPlacement='parallel'
                tickFormat={tickFormatter}
                style={getStyle(['polarAxisTwo'])}
            />
        );
    }

    render() {
        const {data, domain, getStyle} = this.props;        
        const statList = (!!domain && domain.category.length > 0) ? domain.category : [];   
        const processedData = this.transformDataForDisplay(data, statList);
        return (
            <svg width='600' height='500' viewBox='40 0 250 350'>
                <VictoryChart 
                    standalone={false}
                    polar={true}                    
                    theme={VictoryTheme.material}
                    domain={{y: [0, 1.2]}}
                    startAngle={10}
                    endAngle={370}
                    padding={getStyle(['chart', 'padding'])}                    
                >
                    <VictoryGroup style={{ data: { fillOpacity: 0.2, strokeWidth: 2 } }}>
                        {this.generateAreaPlotFromData(processedData)}                        
                    </VictoryGroup>
                    {this.generatePolarAxesFromDomains(statList)}
                    {this.generateParallelPolaxAxis()}
                    <VictoryLegend                        
                        title={'Players'}
                        x={235}                        
                        style={getStyle(['legend'])}
                        data={this.extractLegendFromData(data)}
                    />
                </VictoryChart>
            </svg>
                
        )
    }
}

export default withStyledComponent<StatsRadarProps>(StatsRadar, StatsRadar.styles);