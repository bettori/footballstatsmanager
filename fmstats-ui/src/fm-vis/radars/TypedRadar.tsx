import React from 'react';
import { observer, inject } from 'mobx-react';
import StatsRadar from './StatsRadar';
import { withStyledComponent, StyledComponentProps } from '../../components/presentation/StyledComponent';
import { StatisticsStore } from '../../stores/domain/StatisticsStore';
import { RootStore } from '../../stores/RootStore';
import { PlayerData } from '../../objects/Player/PlayerData';
import { ComparisonType } from '../../objects/Player/PlayerStats';
import { StatDomain } from '../../objects/StatDomain';

type TypedRadarProps = {
    data: Array<PlayerData>
    comparison: ComparisonType
    domain: StatDomain
    rootStore?: RootStore
}

type StyledTypedRadarProps =  TypedRadarProps & StyledComponentProps

@inject('rootStore')
@observer
export class TypedRadar extends React.Component<StyledTypedRadarProps> {  

    static styles = {
        typedRadar: {
            marginTop: '10px'
        },
        radarContents: {
            fontSize: '8px',
            width: '80%',
            height: '100%',
            marginLeft: '0%'
        }
    }

    private statsStore: StatisticsStore

    constructor(props: StyledTypedRadarProps) {
        super(props);
        this.statsStore = props.rootStore!.statisticsStore;
        const { comparison } = props;
        this.statsStore.statisticDomains.find(domain => domain.domain === comparison);
    }

    async componentDidMount() {
        
    }

    render() {     
        const {styleClasses, data, domain} = this.props;        
        return (
            <div className={styleClasses.typedRadar}>                   
                <div className={styleClasses.radarContents}>
                    <StatsRadar
                        data={data}      
                        domain={domain}              
                    /> 
                </div>                            
            </div>
        )
    }
}

export default withStyledComponent<TypedRadarProps>(TypedRadar, TypedRadar.styles);