export class PaginatedArray<T> {
    count: number
    currentPage: number
    rowsPerPage: number
    data: Array<T>    
}