import React from 'react';
import TableCell, { TableCellProps } from '@material-ui/core/TableCell';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TablePagination from '@material-ui/core/TablePagination';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { withStyledComponent, StyledComponentProps } from '../presentation/StyledComponent';
import { ProvidedStores } from '../../stores/ProvidedStores';
import { connectComponent } from '../../util/connectComponent';
import { SortDirection } from '../../stores/UI/ItemTableStore';

interface ItemTableStoreProps {
    rowsPerPage: number
    itemCount: number
    currentPage: number
    sortByColumn: string
    sortDirection: SortDirection
    setRowsPerPage: (rows: number) => void
    setItemCount: (count: number) => void
    setCurrentPage: (page: number) => void
    setSortByColumn: (column: string) => void
    setSortDirection: (direction: SortDirection) => void
}

interface ItemTableProps {
    tableData: Array<object>
    headers: Array<string>
    columnSorting?: boolean
    columnSorter?: (rowsPerPage: number, column: string, direction: SortDirection) => Promise<void>
    paginationOptions?: PaginationOptions
    displayHeader?: boolean,  
    columnRenderer?: Renderer
    headerRenderer?: Renderer
}

type StyledTableProps = ItemTableProps & StyledComponentProps & ItemTableStoreProps;

export interface PaginationOptions {    
    changePageHandler: (rowsPerPage: number, page: number, sortColumn: string, sortOrder: SortDirection) => Promise<void>
    rowsPerPageOptions: Array<any>
}

export interface Renderer {    
    [key: string]: (columnName: string, columnData?: object, index?: number) => React.ReactElement<TableCellProps>
}

export class ItemTable extends React.Component<StyledTableProps> {

    constructor(props: StyledTableProps) {
        super(props);
        if(props.paginationOptions) {
            props.setCurrentPage(0);
            props.setRowsPerPage(props.paginationOptions?.rowsPerPageOptions[0]);            
        }
    }

    defaultCellAlignment : 'inherit' | 'left' | 'center' | 'right' | 'justify' = 'inherit'

    static defaultProps = {
        displayHeader: true,
        columnSorting: false
    };

    static styles = {
        tableRoot: {
            width: '100%',
            display: ' table',
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            borderSpacing: 0,
            borderCollapse: 'collapse'
        },
        head: {
            backgroundColor: '#4286f4',
            color: 'white'
        }
    }

    sortRequestHandler = (currentColumn: string) => (event: React.MouseEvent<unknown>) => {
        // TODO - Add ability to handle server side sort request
        const {sortDirection, sortByColumn, setSortDirection, setSortByColumn, columnSorter, rowsPerPage} = this.props;
        const isAsc = sortByColumn === currentColumn && sortDirection === SortDirection.Ascending;
        const newSortDirection = isAsc ? SortDirection.Descending : SortDirection.Ascending;
        setSortDirection(newSortDirection);
        setSortByColumn(currentColumn);
        if (!!columnSorter) {
            columnSorter(rowsPerPage, currentColumn, newSortDirection);
        }
    }

    renderBodyCell = (columnName: string, columnData: object, colIndx : number) : React.ReactElement<TableCellProps> => {
        const {columnRenderer} = this.props;
        if (columnRenderer && columnName in columnRenderer!) {
            return columnRenderer![columnName](columnName, columnData, colIndx);
        } else {
            return (
                <TableCell 
                    align={this.defaultCellAlignment} 
                    key={colIndx}
                >
                    {columnData[columnName]}
                </TableCell>
            )
        }
    }

    renderHeaderCell = (headerName: string) => {
        const {headerRenderer, styleClasses, columnSorting, sortByColumn, sortDirection} = this.props;

        const headerCellContent = (sortEnabled: boolean) => {
            if(sortEnabled) {
                return (
                    <TableSortLabel
                        active={sortByColumn === headerName}
                        direction={sortByColumn === headerName ? sortDirection : 'asc'}
                        onClick={this.sortRequestHandler(headerName)}
                    >
                        {headerName}
                    </TableSortLabel>
                )
            } else {
                return headerName;
            }
        }

        if(headerRenderer && headerName in headerRenderer!) {
            return headerRenderer![headerName](headerName);
        } else {
            return (
                <TableCell 
                    classes={{head: styleClasses.head}} 
                    align={this.defaultCellAlignment} 
                    key={headerName}                    
                >
                    {headerCellContent(columnSorting)}
                </TableCell>
            )
        }
    }

    renderBodyRow = (rowData: object, columnNames: Array<string>, rowIndex: number) : JSX.Element => {
        const cells = columnNames.map((key, colIndx) => this.renderBodyCell(key, rowData, colIndx));

        return (
            <TableRow key={rowIndex}>
                {cells}
            </TableRow>
        );
    }

    renderHeaderRow = (headers: Array<string>) : JSX.Element => {
        const headerCells = headers.map(header => this.renderHeaderCell(header));
        return (
            <TableRow key='header'>
                {headerCells}
            </TableRow>
        );
    } 

    getHeaderElement = (headers: Array<string>) => {
        return (
            <TableHead>                
                {this.renderHeaderRow(headers)}               
            </TableHead>
        );
    }

    getTablePagination = () => {
        const { paginationOptions, rowsPerPage, itemCount, currentPage, setCurrentPage, sortDirection, sortByColumn, setRowsPerPage } = this.props;
        const pageChangeHandler = async (event: object, page: number) : Promise<void> => {
            await paginationOptions?.changePageHandler(rowsPerPage, page, sortByColumn, sortDirection);
            setCurrentPage(page);
        }

        const rowsPerPageChangeHandler = async (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) : Promise<void> => {
            const { paginationOptions, sortByColumn, sortDirection } = this.props;
            const newRowsPerPage: number = parseInt(event.target.value, 10);
            await paginationOptions?.changePageHandler(newRowsPerPage, 0, sortByColumn, sortDirection);
            setRowsPerPage(newRowsPerPage);
            setCurrentPage(0);
        }

        return (
            <TablePagination
                rowsPerPageOptions={paginationOptions?.rowsPerPageOptions}
                component='div'
                colSpan={3}
                count={itemCount ?? 0}
                rowsPerPage={rowsPerPage ?? paginationOptions?.rowsPerPageOptions[0]}
                page={currentPage ?? 0}
                onChangePage={pageChangeHandler}
                onChangeRowsPerPage={rowsPerPageChangeHandler}
            /> 
        )
    }    

    render() {
        const {headers, tableData, displayHeader, styleClasses, paginationOptions } = this.props;
        const rowComponents = tableData.map((row, rowIndex) => {
            return this.renderBodyRow(row, headers, rowIndex);
        });

        return(
            <div>
                <Table
                    classes={{root: styleClasses.tableRoot}}
                >
                    {displayHeader && this.getHeaderElement(headers)}
                    <TableBody>
                        {rowComponents}
                    </TableBody>                    
                </Table>
                {!!paginationOptions && this.getTablePagination()}
            </div>

        )
    }
}

const storeConnector = (stores: ProvidedStores) : ItemTableStoreProps => {
    const itemTableStore = stores.rootStore.itemTableStore;

    return {
        rowsPerPage: itemTableStore.rowsPerPage,
        itemCount: itemTableStore.itemCount,
        currentPage: itemTableStore.currentPage,
        sortByColumn: itemTableStore.sortByColumn,
        sortDirection: itemTableStore.sortDirection,
        setCurrentPage: itemTableStore.setCurrentPage,
        setItemCount: itemTableStore.setItemCount,
        setRowsPerPage: itemTableStore.setRowsPerPage,
        setSortByColumn: itemTableStore.setSortByColumn,
        setSortDirection: itemTableStore.setSortDirection
    }
}

const connectedComponent = connectComponent<StyledTableProps, ItemTableStoreProps, StyledComponentProps & ItemTableProps>(storeConnector, ItemTable);
export default withStyledComponent(connectedComponent, ItemTable.styles)