import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import MenuList from '@material-ui/core/MenuList';
import MenutItem from '@material-ui/core/MenuItem';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import { withStyledComponent, StyledComponentProps } from '../presentation/StyledComponent';
import { Link } from 'react-router-dom';

const styles = {
    contentDisplay: {
        marginLeft: '11%'
    }
}

export function Layout(props: {children: any} & StyledComponentProps) {

    const { children, styleClasses } = props;

    return (
        <div>
            <Grid container={true} direction='row'>
                <Grid item={true} key='menu' xs={6}>
                <Drawer anchor='left' open={true} variant='permanent'>
                    <MenuList>
                        <MenutItem component={Link} to='/import'>
                            Import Statistics                       
                        </MenutItem>
                        <MenuItem component={Link} to='/players'>
                            Search Players
                        </MenuItem>
                    </MenuList>
                </Drawer>
                </Grid>
                <Grid item={true} key='content' xs={12}>
                <div className={styleClasses.contentDisplay}>
                    {children}
                </div>
                </Grid>
            </Grid>                                    
        </div>
    );
}

export default withStyledComponent(Layout, styles);