import React, { useEffect } from 'react';
import { useLocalStore, inject, observer } from 'mobx-react';
import ItemTable, { PaginationOptions, Renderer } from '../grid/ItemTable';
import { PlayerCareerOverviewData } from '../../objects/Player/PlayerData';
import { PaginatedArray } from '../../collections/PaginatedArray';
import { ProvidedStores } from '../../stores/ProvidedStores';
import { Link } from 'react-router-dom';
import { TableCell } from '@material-ui/core';
import { SortDirection } from '../../stores/UI/ItemTableStore';

export interface PlayerListPageStoreProps {
    playerData?: Array<any>
    retrieveData?: (page: number, rowsPerPage: number, sortColumn?: string, sortOrder?: SortDirection) => Promise<PaginatedArray<PlayerCareerOverviewData>>
    setItemCount?: (count: number) => void
}

interface PlayerListPageProps {}

type CombinedPlayerListPageProps = PlayerListPageStoreProps & PlayerListPageProps;

interface PlayerListPageState {
    dataToShow: Array<any>
    updateData: (data: Array<any>) => void
}

export function PlayerListPage(props: CombinedPlayerListPageProps) {

    const extractPlayerData = (data: Array<PlayerCareerOverviewData>) : Array<any> => {
        return data.map(datum => ({
            id: datum.player.id,
            name: `${datum.player.firstName} ${datum.player.lastName}`,
            age: datum.player.age,
            height: datum.player.height,
            weight: datum.player.weight,
            nationality: datum.player.nationality,
            position: datum.player.position,
            currentTeam: datum.player.currentTeam
        }));
    }

    const localState = useLocalStore(() : PlayerListPageState => (
        {
            dataToShow: [],
            updateData: (data: Array<any>) => {
                localState.dataToShow = data;
            }
        }
    ));

    const paginateOptions : PaginationOptions = {        
        rowsPerPageOptions: [1, 2],
        changePageHandler: async (rowsPerPage: number, page: number, sortColumn?: string, sortOrder?: SortDirection) : Promise<void> => {
            const paginatedData = await props.retrieveData(page, rowsPerPage, sortColumn, sortOrder);
            localState.updateData(extractPlayerData(paginatedData.data));
        }
    }

    const sortHandler = async (rowsPerPage: number, sortColumn: string, sortOrder: SortDirection) : Promise<void> => {
        const { retrieveData } = props;
        const sortedData = await retrieveData(0, rowsPerPage, sortColumn, sortOrder);
        localState.updateData(extractPlayerData(sortedData.data));
    }

    const headerRenderer: Renderer = {
        'id': (colName: string, columnData: object, colIndx: number) => {
            return (<td key={colName}/>);
        }
    }

    const columnRenderer : Renderer = {
        'id':  (colName: string, columnData: object, colIndx: number) => {
            return (<td key={colIndx}/>);
        },
        'name': (colName: string, columnData: object, colIndx: number) => {
            return (
                <TableCell key={colIndx}>
                    <Link to={`/players/${columnData['id']}`}>{columnData[colName]}</Link>
                </TableCell>
            )
        }
    }

    useEffect(() => {
        const dataRetriever = async () => {
            const {setItemCount, retrieveData} = props;
            const paginatedData = await retrieveData(0, paginateOptions.rowsPerPageOptions[0]);
            setItemCount(paginatedData.count);
            localState.updateData(extractPlayerData(paginatedData.data));            
        };

        dataRetriever();
    }, []); // empty array ensures the effect only executes once.

    const drawTable = () => {        
        if(localState.dataToShow.length > 0) {
            return (                
                <ItemTable
                    headers={Object.keys(localState.dataToShow[0])}
                    tableData={localState.dataToShow}
                    paginationOptions={paginateOptions}
                    columnRenderer={columnRenderer}
                    headerRenderer={headerRenderer}
                    columnSorting={true}
                    columnSorter={sortHandler}
                />            
            )
        } else {
            return null;
        }
    }

    return (
        <div>
           {drawTable()}           
        </div>
        
    )  
}

const storeConnector = (stores: ProvidedStores) : PlayerListPageStoreProps => ({
    playerData: [],
    retrieveData: stores.rootStore.playerListPageStore.retrievePlayerListPage,
    setItemCount: stores.rootStore.itemTableStore.setItemCount
});

export default inject(storeConnector)(observer(PlayerListPage));  