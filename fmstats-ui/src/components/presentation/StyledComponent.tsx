import React from 'react';
import { withStyles, createStyles, Theme } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/styles';

export interface StyledComponentProps {
    getStyle: (levels: Array<string>) => object
    makeStyles: (styles: object) => any
    getStyledComponent: (elementGenerator: (classes: any) => JSX.Element, styles: any) => any
    theme?: Theme
    styleClasses: any
    
}

export function withStyledComponent<T>(ComponentToStyle: React.ComponentType<T & StyledComponentProps>, styles: object, themeAugmenter? : ((styles: object) => (theme: Theme) => object)) {
    return function StyledComponent(props: T) {

        const theme : Theme = useTheme();

        const traverseStyles = (levels: Array<string>, styles: object) => {
            if(levels.length === 1) {
                const result = styles[levels[0]];                    
                return result;
            } else {
                const subStyle = styles[levels.shift()];
                return traverseStyles(levels, subStyle);
            }
        }              

        // TODO - replace this with makeStyles from material-ui/styles package, when it starts working with core
        const makeStyles = (styles: object) => {
            return createStyles(styles);
        }

         // isolate dependencies on mui-theme from generic style object
         const themedStyles = themeAugmenter != null ? makeStyles(themeAugmenter!(styles)) : makeStyles(styles); 

        const getStyle = (levels: Array<string>) : object => {
            const searchStyles = Object.assign({}, styles);
            
            if(levels.length === 0) {
                return searchStyles;
            }

            return traverseStyles(levels, searchStyles);            
        }

        const getElement = (): (classes: any) => JSX.Element => {
            return ({classes}) => {
                return (
                    <ComponentToStyle
                        {...props}
                        getStyle={getStyle}
                        makeStyles={makeStyles}
                        getStyledComponent={getStyledComponent}
                        styleClasses={classes}
                        theme={theme}
                    />
                )
            }           
        }

        const getStyledComponent = (elementGenerator: (classes: any) => JSX.Element, styles: any) => {
            return withStyles(styles, {withTheme: true})(elementGenerator);
        }
    
        const element = getElement();
        const StylishComponent = withStyles(themedStyles, {withTheme: true})(element);

        return (
            <StylishComponent/>
        )
        
    }
}