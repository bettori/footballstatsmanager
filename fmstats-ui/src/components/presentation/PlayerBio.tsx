import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import { StyledComponentProps, withStyledComponent } from './StyledComponent';
import TextField from '../fields/TextField';
import { Player } from '../../objects/Player/Player';
import { Typography } from '@material-ui/core';


interface PlayerBioProps {
    player: Player
}

type StyledPlayerBioProps = PlayerBioProps & StyledComponentProps;

const styles = {
    root: {
        // backgroundColor: '#f4f4f4'
    },
    colorTextPrimary: {
        // color: '#373737'
    }
};

export function PlayerBio(props: StyledPlayerBioProps){

    const fieldStyles = {
        overline: {                   
            fontSize: 'calc(15% + 0.4vw + 0.1vh)',
            marginRight: '1vw'                        
        },
        // colorTextPrimary: PlayerBio.styles.colorTextPrimary
    }

    const generateDisplayFields = (attributes: Array<any>) : Array<JSX.Element> => {
        return attributes.map(attribute => {
            return (
                <Grid item={true} key={attribute.label} xs={2}>     
                    <TextField
                        label={attribute.label}
                        value={attribute.value}
                        styles={fieldStyles}
                    />                                    
                </Grid>
            );
        })
    }

    
    const {player, styleClasses} = props;
    const name =`${player.firstName} ${player.lastName}`;
    const displayAttributes = [            
        {label: 'Age', value: player.age},
        {label: 'Height', value: player.height},
        {label: 'Weight', value: player.weight},
        {label: 'Nationality', value: player.nationality},
        {label: 'Position', value: player.position},
        {label: 'Current Team', value: player.currentTeam}
        ];
        const nameStyles : any = {
        colorTextPrimary: styleClasses.colorTextPrimary
        };

    return (
        <div>
            <Card classes={{root: styleClasses.root}}>
                <CardContent>
                    <Grid container={true} direction={'row'}>                            
                        <Grid item={true} key={'name'} xs={12}>                         
                            <Typography 
                                color='textPrimary' 
                                variant='overline'
                                classes={nameStyles}    
                            >
                                {name}
                            </Typography>  
                        </Grid>
                        <Grid item={true} key={'dataInfo'} xs={12}>
                            <Grid container={true} spacing={8} direction={'row'}>
                                {generateDisplayFields(displayAttributes)}                                    
                            </Grid>  
                        </Grid>                                                                                 
                    </Grid>
                </CardContent>
            </Card>                
        </div>
    );
}

export default withStyledComponent(PlayerBio, styles);