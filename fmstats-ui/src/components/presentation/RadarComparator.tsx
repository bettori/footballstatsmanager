import React from 'react';
import { RootStore } from '../../stores/RootStore';
import { PlayerStore } from '../../stores/domain/PlayerStore';
import { observer, inject } from 'mobx-react';
import TypedRadar from '../../fm-vis/radars/TypedRadar';
import { Player } from '../../objects/Player/Player';
import { ComparisonType } from '../../objects/Player/PlayerStats';
import { StatisticsStore } from '../../stores/domain/StatisticsStore';
import Grid from '@material-ui/core/Grid';
import { StyledComponentProps, withStyledComponent } from './StyledComponent';
import { Card, CardContent, Theme } from '@material-ui/core';
import { Selector } from '../fields/Selector';
import { RadarComparatorStore } from '../../stores/UI/RadarComparatorStore';

interface RadarComparatorProps {
  rootStore?: RootStore;
  player: Player;
}

type StyledRadarComparatorProps = RadarComparatorProps & StyledComponentProps;

@inject('rootStore')
@observer
export class RadarComparator extends React.Component<StyledRadarComparatorProps> {
  static styles = {
    topLevel: {
      marginTop: '1%'
    },
    selectors: {
      margin: '5%'
    },
    selector: {
      paddingBottom: '5%'
    },
    cardRoot: {
      overflow: 'auto',
      backgroundColor: '#f4f4f4'
    }
  };

  static getThemedStyles = (styles: object) => {
    return (theme: Theme) => {
      const simpleStyles = styles;
      const selectStyles = {
        select: {
          singleValue: (provided, state) => ({
            ...provided,
            fontSize: 'calc(30% + 0.4vw + 0.1vh)'
          }),
          container: (provided, state) => ({
            ...provided,
            color: theme.palette.primary.main
          }),
          menu: (provided, state) => ({
            ...provided,
            zIndex: 1000
          })
        }
      };
      const themedStyles = Object.assign(simpleStyles, selectStyles);
      return themedStyles;
    };
  };

  playerStore: PlayerStore = null;
  statsStore: StatisticsStore = null;
  uiStore: RadarComparatorStore = null;
  comparisonOptions: Array<object> = null;

  constructor(props: StyledRadarComparatorProps) {
    super(props);
    this.playerStore = props.rootStore!.playerStore;
    this.statsStore = props.rootStore!.statisticsStore;
    this.uiStore = props.rootStore!.radarComparatorStore;
  }

  async componentDidMount() {
    const { player } = this.props;
    await this.uiStore.initializeStore(player);
  }

  playerSelected = async (chosenValue: any, action: String) => {
    const playerId = chosenValue.value;
    // await this.statsStore.updateStatsByPlayer(playerId);
    await this.uiStore.updatePlayerData(playerId);
  };

  comparisonSelected = async (chosenComparison: any, action: String) => {
    // when comparison type is chosen/changed
    const chosenValue = chosenComparison.value;
    const selectedComparisonType: ComparisonType = (ComparisonType as any)[
      chosenValue
    ];
    await this.uiStore.updateDataByComparisonType(selectedComparisonType);
  };

  playerSeasonSelected = async (chosenSeason: any) => {
    const seasonId = chosenSeason.value;
    this.uiStore.updateStatsBySeason(seasonId);
  }

  comparisonSeasonSelected = async(chosenSeason: any) => {
    const seasonId = chosenSeason.value;
    this.uiStore.updateComparisonStatsBySeason(seasonId);
  }

  getPlayerOptions = (): Array<object> => {
    return (this.playerStore.playerBios || [])
      .filter(player => player.id !== this.props.player.id)
      .map(player => {
        return {
          label: `${player.lastName}, ${player.firstName}`,
          value: player.id
        };
      });
  };

  getComparisonOptions = (): Array<object> => {
    if (!this.comparisonOptions) {
      const comparisonKeys = Object.keys(ComparisonType);
      this.comparisonOptions = comparisonKeys.map(cKey => {
        return { label: ComparisonType[cKey], value: cKey };
      });
    }

    return this.comparisonOptions;
  };

  render() {
    const { styleClasses, getStyle } = this.props;
    const playerOptions = this.getPlayerOptions();
    const playerData = this.uiStore.radarPlayerData ?? [];
    const statDomain = this.statsStore.currentStatisticsDomain.find(sd => sd.domain === this.uiStore.radarComparisonType);

    return (
      <div className={styleClasses.topLevel}>
        <Card classes={{ root: styleClasses.cardRoot }}>
          <CardContent>
            <Grid container={true} direction={'row'} spacing={2}>
              <Grid item={true} xs={4}>
                <Grid container={true} direction={'column'} spacing={8}>
                  <Grid item={true} xs={6}>
                    <Selector
                      className={styleClasses.selector}
                      label={'Select Player'}
                      changeCallback={this.playerSelected}
                      options={playerOptions}
                      value={this.uiStore.playerCompared || {}}
                      isSearchable={false}
                      styles={getStyle(['select'])}
                    />
                  </Grid>
                  <Grid item={true} xs={6}>
                    <Selector
                        className={styleClasses.selector}
                        label={'Select Compar. Type'}
                        changeCallback={this.comparisonSelected}
                        options={this.getComparisonOptions()}
                        value={this.uiStore.currentComparisonType}
                        isSearchable={false}
                        styles={getStyle(['select'])}
                    />
                  </Grid>
                  <Grid item={true} xs={6}>
                    <Selector
                        className={styleClasses.selector}
                        label={`Select Season (${this.uiStore.playerSelected.label  })`}
                        changeCallback={this.playerSeasonSelected}
                        options={this.uiStore.selectedPlayerSeasonDisplayList}
                        value={this.uiStore.selectedPlayerSeasonDisplay}
                        isSearchable={false}
                        styles={getStyle(['select'])}
                    />
                  </Grid>
                  <Grid item={true} xs={6}>
                    <Selector
                        className={styleClasses.selector}
                        label={`Select Season (${this.uiStore.playerCompared.label})`}
                        changeCallback={this.comparisonSeasonSelected}
                        options={this.uiStore.comparisonPlayerSeasonDisplayList}
                        value={this.uiStore.comparisonPlayerSeasonDisplay}
                        isSearchable={false}
                        styles={getStyle(['select'])}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item={true} xs={6}>
                {!this.statsStore.isSyncing && <TypedRadar data={playerData} comparison={this.uiStore.radarComparisonType} domain={statDomain} />}
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withStyledComponent(
  RadarComparator,
  RadarComparator.styles,
  RadarComparator.getThemedStyles
);
