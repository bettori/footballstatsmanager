import React, { useEffect } from 'react';
import { observer, inject } from 'mobx-react';
import { Player } from '../../objects/Player/Player';
import ItemTable from '../grid/ItemTable';
import { Renderer } from '../grid/ItemTable';
import { TableCell, Paper, Tabs, Tab, Tooltip } from '@material-ui/core';
import { withStyledComponent, StyledComponentProps } from './StyledComponent';
import { CareerStatFilter } from '../../objects/Player/PlayerStats';
import { pick } from 'lodash';
import { PlayerCareerData } from '../../objects/Player/PlayerData';
import { ProvidedStores } from '../../stores/ProvidedStores';

export interface PlayerCareerSummaryProps {
    player: Player
}

export interface PlayerCareerSummaryConnectProps {
    playerData?: PlayerCareerData
    retrieveCareerSummaryData?: (id: string, comparison: string) => Promise<PlayerCareerData>
    selectedStatView?: string
}

type StyledPlayerCareerSummaryProps = PlayerCareerSummaryProps & PlayerCareerSummaryConnectProps & StyledComponentProps;

const styles = theme => {
    const fontSize = 'calc(20% + 0.5vw + 0.2vh)';
    return ({
        cellRoot: {
            padding: '1% 1% 1% 1%',            
            borderBottom: '1px solid rgba(224, 224, 224, 1)',
            verticalAlign: 'inherit'
        },
        body: {                        
            fontSize: fontSize,
            color: 'rgba(0, 0, 0, 0.87)'
        },
        head: {
            fontSize: fontSize,
            padding: '1% 1% 1% 1%',
            backgroundColor: '#373737',
            color: '#c0b283'
        },
        cardRoot: {
            fontSize: '50%',
            margin: '4vh 0 2% 0%',    
            backgroundColor: '#f4f4f4',   
            overflow: 'auto'
        },
        tabRoot: {
            minHeight: '30px'
        },
        tabWrapper: {
            color: theme.palette.primary.main,
            fontSize: '70%'
        },
        tabSelected: {
            backgroundColor: theme.palette.secondary.main
        }
    })
}

export function PlayerCareerSummary(props: StyledPlayerCareerSummaryProps) {    
    
    const headerStrings = {
        'season': 'Season', 
        'club': 'Club', 
        'league': 'League', 
        'goals': 'Goals', 
        'assists': {
            label: 'Asts',
            detail: 'Assists'
        }, 
        'yellowCards': 'Yel. Cards', 
        'redCards': 'Red Cards', 
        'pointsPerGame': 'Pts / Game',
        'appearances': {
            label: 'Apps',
            detail: 'Appearances'
        },
        'minutes': {
            label: 'Mins',
            detail: 'Total Minutes'
        },
        'tackles': {
            label: 'Tackles',
            detail: 'Tackles / Game'
        },
        'interceptions': {
            label: 'Inter',
            detail: 'Interceptions / Game'
        },
        'fouls': {
            label: 'Fouls',
            detail: 'Fouls / Game'            
        },
        'shotsPerGame': {
            label: 'Shots',
            detail: 'Shots / Game'
        },
        'keyPassesPerGame': {
            label: 'KeyP',
            detail: 'Key Passes / Game'
        },
        'dribbles': {
            label: 'Drb',
            detail: 'Dribbles / Game'
        },
        'passPercent': {
            label: 'Passing %',
            detail: 'Pass success %'
        },
        'passesCompletedPer90': {
            label: 'Pass C. / 90',
            detail: 'Passes Completed / 90 minutes'
        }
    }; 

    useEffect(() => {
        const { retrieveCareerSummaryData, player } = props;
        retrieveCareerSummaryData(player.id, 'summary');
    }, []);

    
    const cellStyler = (columnName: string, columnData?: any, index?: number) => {
        const { styleClasses } = props;
        return (            
            <TableCell
                key={index}
                classes={{root: styleClasses.cellRoot, body: styleClasses.body}}
                align={'center'}
            >
                {columnData![columnName]}
            </TableCell>            
        );
    }

    const handleChange = async (event: React.ChangeEvent<{}>, newValue: string) => {  
        const { retrieveCareerSummaryData, playerData }  = props;     
        await retrieveCareerSummaryData(playerData.id, newValue);
    }
        
    const getcolumnRenderers = (columnStrings) : Renderer => {
        let columnRenderers = {};
        Object.keys(columnStrings).map(col => columnRenderers[col] = cellStyler)
        return columnRenderers;
    }

    const getHeaderRenderers = (headerStrings) : Renderer => {
        let headerRenderers = {};
        const { styleClasses } = props;

        const headerStyler = (headerName: string, columnData?: any, index?: number) => {      
            const header = headerStrings[headerName];
            const haveTooltip = typeof header === 'object' && header !== null && header.hasOwnProperty('detail');
            
            if(haveTooltip) {
                return (
                    <Tooltip title={header.detail} placement='top' key={headerName}>
                        <TableCell
                            key={headerName}
                            classes={{head: styleClasses.head}}
                            align={'center'}
                        >
                            {header.label}
                        </TableCell>
                    </Tooltip>                   
                );
            } else {
                return (
                    <TableCell
                        key={headerName}
                        classes={{head: styleClasses.head}}
                        align={'center'}
                    >
                        {header}
                    </TableCell>
                );
            }            
        };

        Object.keys(headerStrings).map(col => headerRenderers[col] = headerStyler)
        return headerRenderers;
    }

    const getStatColumns = (data: PlayerCareerData) : Array<string> => {
        if(!!data) {
            const statShape = data.statistics[0];
            return Object.getOwnPropertyNames(statShape);
        } else {
            return [];
        }
    }

    const getTabs = (styles: any) : Array<JSX.Element> => {
        const classes = {
            root: styles.tabRoot,
            wrapper: styles.tabWrapper,
            selected: styles.tabSelected
        };
        return Object.keys(CareerStatFilter).map(key => {
            return (
                <Tab
                    key={key}
                    label={CareerStatFilter[key].label}
                    value={CareerStatFilter[key].code}
                    classes={classes}
                />
            )
        })
    }

    
    const { player, styleClasses, playerData, selectedStatView } = props;
    const headers = pick(headerStrings, getStatColumns(playerData));
    
    return (
        <Paper classes={{root: styleClasses.cardRoot}}>    
            <Tabs 
                    value={selectedStatView} 
                    onChange={handleChange}                           
                    classes={{root: styleClasses.tabRoot}}                       
            >
                {getTabs(styleClasses)}                       
            </Tabs>   
            <ItemTable
                tableData={!!playerData && playerData.id === player.id ? playerData.statistics : []}
                headers={Object.keys(headers)}
                columnRenderer={getcolumnRenderers(headers)}
                headerRenderer={getHeaderRenderers(headers)}
            />         
        </Paper>                
    );
    
}

const storeConnector = (stores: ProvidedStores) : PlayerCareerSummaryConnectProps => ({
    playerData: stores.rootStore.playerCareerSummaryStore.playerCareerData,
    retrieveCareerSummaryData: stores.rootStore.playerCareerSummaryStore.retrieveCareerSummaryData,
    selectedStatView: stores.rootStore.playerCareerSummaryStore.selectedStatView
});

export default withStyledComponent(inject(storeConnector)(observer(PlayerCareerSummary)), styles);