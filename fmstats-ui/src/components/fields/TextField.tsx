import React from 'react';
import { Typography } from '@material-ui/core';
import { StyledComponentProps, withStyledComponent } from '../presentation/StyledComponent';

interface TextFieldProps {
    label: string
    value: any,
    styles?: any
}

type StyledTextFieldProps=TextFieldProps & StyledComponentProps;
type FieldType = 'textPrimary' | 'textSecondary';

export class TextField extends React.Component<StyledTextFieldProps> {

    componentDidMount() {
        console.log('mounted text field');
    }

    componentWillUnmount() {
        console.log('unmounting text field');
    }

    static styles = {}

    private getPrimaryStyles = () => {
        const {styles} = this.props;
        const primaryStyles : any = {};

        if (!!styles) {
            if ('colorTextPrimary' in styles) {
                primaryStyles.colorTextPrimary = styles.colorTextPrimary;
            }
    
            if('overline' in styles) {
                primaryStyles.overline = styles.overline;
            }
        }        

        return primaryStyles;
    }

    private getSecondaryStyles = () => {
        const {styles} = this.props;
        const secondaryStyles : any = {};

        if (!!styles) {
            if ('colorTextSecondary' in styles) {
                secondaryStyles.colorTextSecondary = styles.colorTextSecondary;
            }
    
            if('overline' in styles) {
                secondaryStyles.overline = styles.overline;
            }
        }        

        return secondaryStyles;
    }

    private getStyledField = (text: string, fieldType: FieldType) : (classes: any) => JSX.Element => {
        return (classes: any) => {
            return (
                <Typography
                    color={fieldType}
                    variant='overline'
                    classes={classes.classes}
                >
                    {text}
                </Typography>
            )
        }
    }    

    render() {
        const {label, value, getStyledComponent} = this.props;
        const secondaryTypographyClasses = this.getSecondaryStyles();
        const primaryTypographyClasses = this.getPrimaryStyles();

        const PrimaryTextElement = getStyledComponent(this.getStyledField(value, 'textPrimary'), primaryTypographyClasses);
        const SecondaryTextElement = getStyledComponent(this.getStyledField(label, 'textSecondary'), secondaryTypographyClasses);

        return (
        <div>
            <SecondaryTextElement/>            
            <PrimaryTextElement/>
        </div>
        );
        
    }
}

export default withStyledComponent(TextField, TextField.styles);