import React from 'react';
import Select from 'react-select';
import Typography from '@material-ui/core/Typography';

interface SelectorProps {
    label: string
    changeCallback: any
    options: object
    value: any
    isSearchable: boolean
    styles: any
    className: any
}

export class Selector extends React.Component<SelectorProps> {
    render() {
        const { label, changeCallback, options, value, isSearchable, styles, className } = this.props;
        return (
            <div className={className}>
                 <Typography color={'primary'} variant='body2'>
                    {label}
                </Typography>
                <Select
                    onChange={changeCallback}
                    options={options}
                    value={value}
                    isSearchable={isSearchable}
                    styles={styles}
                />
            </div>
        )
    }
}