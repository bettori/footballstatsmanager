const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractLess = new ExtractTextPlugin({
  filename: '[name].css'
});
const dotenv = require('dotenv');
module.exports = () => {
  const env = dotenv.config().parsed;
  
  // reduce it to a nice object
  const envKeys = Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next]);
    return prev;
  }, {});

  return {
    entry: './src/index.tsx', 
    devtool: 'cheap-module-eval-source-map',  
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: { presets: ['@babel/env'] }
        },
        {
          test :/(\.ts|\.tsx)$/,
          enforce: 'pre',
          exclude: /node_modules/,
          loader: 'tslint-loader',
          options: {
            emitWarning: true
          }
        },
        {
          test: /\.tsx?$/,
          loader: 'awesome-typescript-loader',
          exclude: /node_modules/
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        {
          test: /\.less$/,
          use: [{
            loader: 'style-loader'
          },{
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: false
            }
          }, {
            loader: 'less-loader'
          }]
        }
      ]
    },
    resolve: { 
      extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
      modules: [path.resolve(__dirname, 'src'), 'node_modules']
    },
    output: {
      path: path.resolve(__dirname, 'dist/'),
      publicPath: '/dist/',
      filename: '[name].bundle.js'
    },
    devServer: {
      contentBase: path.join(__dirname, "."),
      port: 3000,
      publicPath: 'http://localhost:3000/dist/'
    },
    plugins: [
      new webpack.DefinePlugin(envKeys)
    ]
  }  
};