function* seasons(start, end, step) {
    while(start < end) {
        const startYear = start;
        const endYear = start + 1;
        yield {start: startYear, end: endYear, label: `${startYear} - ${endYear}`};
        start += step;
    }
}

function* numbers(start, end, step, scale, digits) {
    const multiplier = Math.pow(10, digits);
    while(start < end) {
        yield Math.floor(multiplier*scale*Math.random())/multiplier;
        start += step;
    }
}

function selectRandomValue(array) {
    const length = array.length;
    const index = Math.abs(Math.floor(length * Math.random()) - 1);
    return array[index];
}

function significantDigits(number) {
    const log10 = Math.log(10);
    number = Math.abs(String(number).replace(".", "")); //remove decimal and make positive
    if (number == 0) return 0;
    while (number != 0 && number % 10 == 0) number /= 10; //kill the 0s at the end of n

    return Math.floor(Math.log(number) / log10) + 1; //get number of digits
}


generateSeasonStats = (domainDefinitions, leagues, seasons) => {
    const playerIds = ['12', '23', '24']    
    const domains = ['attack', 'defense', 'midfield'];
    // add category codes for each domain

    // source data:
    const clubs = ['Barcelona', 'Ajax Amsterdam', 'Fc Nantes', 'Olympique Marseille', 'Manchester United'];
    const appearances = Array.from(numbers(1, 100, 1, 38));
    const minutes = Array.from(numbers(0, 100, 1, 38 * 90));
    const categoryNumbers = new Map();    
    domainDefinitions.forEach(domain => {        
        domain.category.forEach(cat => {            
            const dig = significantDigits(cat.max);            
            categoryNumbers.set(cat.categoryCode, Array.from(numbers(0, 100, 1, cat.max, dig)));            
        });
    });

    return seasons.filter(s => s.leagueId === 1).flatMap(s => {
            return playerIds.flatMap(id => {
               return domains.map(domain => {
                    const domainDefition = domainDefinitions.find(d => d.domain === domain);
                    const statistics = domainDefition.category.map(cat => {
                        return {
                            categoryCode: cat.categoryCode,
                            value: selectRandomValue(categoryNumbers.get(cat.categoryCode))
                        }
                    });                    
            
                    return {
                        id: id,
                        category: domain,
                        statistics: statistics,
                        season: {
                            id: s.id,
                            leagueId: selectRandomValue(leagues).id,
                            label: s.label
                        }
                    };
                })
            });
    })
}

generateSeasonData = (leagues) => {
    const seasonData = Array.from(seasons(1992, 2019, 1));
    var index = 0;
    return leagues.flatMap(league => {        
        const seasonsLeague = seasonData.map(season => {            
            index += 1;
            return {
                id: index,
                leagueId: league.id,
                label: season.label,
                start: season.start,
                end: season.end
            };
        });
        seasonsLeague.push({
            id: index + 1, 
            leagueId: league.id, 
            label: 'career'
        });

        return seasonsLeague;
    });
}

dataGenerator = () => {
    const data = {
        players: [
            {
                id: '12',
                firstName: 'Andy',
                lastName: 'Van der Meyden',
                age: 45,
                height: '6 ft 1',
                weight: '186 lbs',
                nationality: 'Dutch',
                position: 'AMR',
                currentTeam: 'Retired'
            },
            {
                id: '23',
                firstName: 'Antoine',
                lastName: 'Griezmann',
                age: 29,
                height: '5 ft 8',
                weight: '176 lbs',
                nationality: 'French',
                position: 'ST',
                currentTeam: 'Barcelona'
            },
            {
                id: '24',
                firstName: 'Marcus',
                lastName: 'Rashford',
                age: 23,
                height: '6 ft 1',
                weight: '186 lbs',
                nationality: 'English',
                position: 'ST',
                currentTeam: 'Manchester United'
            }
        ],
        playerCareerData: [
            {
                id: '12',
                category: 'summary',
                statistics: [
                    {
                        season: '1997 - 1998',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 4,
                        minutes: 200,
                        goals: 0,
                        assists: 1,
                        yellowCards: 0,
                        redCards: 0,
                        pointsPerGame: 2.3
                    },
                    {
                        season: '1998 - 1999',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 1,
                        minutes: 200,
                        goals: 0,
                        assists: 0,
                        yellowCards: 0,
                        redCards: 0,
                        pointsPerGame: 1
                    },
                    {
                        season: '1999 - 2000',
                        club: 'FC Twente (loan)',
                        league: 'Eredivisie',
                        appearances: 32,
                        minutes: 200,
                        goals: 2,
                        assists: 1,
                        yellowCards: 0,
                        redCards: 0,
                        pointsPerGame: 1.9
                    },
                    {
                        season: '2000 - 2001',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 27,
                        minutes: 200,
                        goals: 2,
                        assists: 5,
                        yellowCards: 2,
                        redCards: 0,
                        pointsPerGame: 2.1
                    },
                    {
                        season: '2001 - 2002',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 30,
                        minutes: 200,
                        goals: 5,
                        assists: 7,
                        yellowCards: 2,
                        redCards: 1,
                        pointsPerGame: 2.3
                    },
                    {
                        season: '2002 - 2003',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 29,
                        minutes: 200,
                        goals: 11,
                        assists: 7,
                        yellowCards: 0,
                        redCards: 0,
                        pointsPerGame: 2.7
                    }
                ]
            },
            {
                id: '12',
                category: 'passing',
                statistics: [
                    {
                        season: '1997 - 1998',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 4,
                        minutes: 200,                    
                        assists: 1,
                        keyPassesPerGame: 0,
                        passPercent: 30,
                        passesCompletedPer90: 0
                    },
                    {
                        season: '1998 - 1999',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 1,
                        minutes: 90,                    
                        assists: 0,
                        keyPassesPerGame: 0,
                        passPercent: 30,
                        passesCompletedPer90: 0
                    },
                    {
                        season: '1999 - 2000',
                        club: 'FC Twente (loan)',
                        league: 'Eredivisie',
                        appearances: 32,
                        minutes: 200,                    
                        assists: 1,
                        keyPassesPerGame: 0,
                        passPercent: 30,
                        passesCompletedPer90: 0
                    },
                    {
                        season: '2000 - 2001',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 27,
                        minutes: 200,                    
                        assists: 5,
                        keyPassesPerGame: 0,
                        passPercent: 30,
                        passesCompletedPer90: 0
                    },
                    {
                        season: '2001 - 2002',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 30,
                        minutes: 200,                    
                        assists: 7,
                        keyPassesPerGame: 0,
                        passPercent: 30,
                        passesCompletedPer90: 0
                    },
                    {
                        season: '2002 - 2003',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 29,
                        minutes: 200,                    
                        assists: 7,
                        keyPassesPerGame: 0,
                        passPercent: 30,
                        passesCompletedPer90: 0
                    }
                ]
            },
            {
                id: '12',
                category: 'attack',
                statistics: [
                    {
                        season: '1997 - 1998',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 4,
                        minutes: 200,
                        goals: 0,
                        assists: 1,
                        shotsPerGame: 0,
                        keyPassesPerGame: 0,
                        dribbles: 2.3
                    },
                    {
                        season: '1998 - 1999',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 1,
                        minutes: 200,
                        goals: 0,
                        assists: 0,
                        shotsPerGame: 0,
                        keyPassesPerGame: 0,
                        dribbles: 2.3
                    },
                    {
                        season: '1999 - 2000',
                        club: 'FC Twente (loan)',
                        league: 'Eredivisie',
                        appearances: 32,
                        minutes: 200,
                        goals: 2,
                        assists: 1,
                        shotsPerGame: 0,
                        keyPassesPerGame: 0,
                        dribbles: 2.3
                    },
                    {
                        season: '2000 - 2001',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 27,
                        minutes: 200,
                        goals: 2,
                        assists: 5,
                        shotsPerGame: 0,
                        keyPassesPerGame: 0,
                        dribbles: 2.3
                    },
                    {
                        season: '2001 - 2002',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 30,
                        minutes: 200,
                        goals: 5,
                        assists: 7,
                        shotsPerGame: 0,
                        keyPassesPerGame: 0,
                        dribbles: 2.3
                    },
                    {
                        season: '2002 - 2003',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 29,
                        minutes: 200,
                        goals: 11,
                        assists: 7,
                        shotsPerGame: 0,
                        keyPassesPerGame: 0,
                        dribbles: 2.3
                    }
                ]
            },
            {
                id: '12',
                category: 'defense',
                statistics: [
                    {
                        season: '1997 - 1998',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 4,
                        minutes: 200,
                        tackles: 0,
                        interceptions: 1,
                        fouls: 0,
                        yellowCards: 0,
                        redCards: 0
                    },
                    {
                        season: '1998 - 1999',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 1,
                        minutes: 200,
                        tackles: 0,
                        interceptions: 1,
                        fouls: 0,
                        yellowCards: 0,
                        redCards: 0                    
                    },
                    {
                        season: '1999 - 2000',
                        club: 'FC Twente (loan)',
                        league: 'Eredivisie',
                        appearances: 32,
                        minutes: 200,
                        tackles: 0,
                        interceptions: 1,
                        fouls: 0,
                        yellowCards: 0,
                        redCards: 0                    
                    },
                    {
                        season: '2000 - 2001',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 27,
                        minutes: 200,
                        tackles: 0,
                        interceptions: 1,
                        fouls: 0,
                        yellowCards: 2,
                        redCards: 0                    
                    },
                    {
                        season: '2001 - 2002',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 30,
                        minutes: 200,
                        tackles: 0,
                        interceptions: 1,
                        fouls: 0,
                        yellowCards: 2,
                        redCards: 1                    
                    },
                    {
                        season: '2002 - 2003',
                        club: 'Ajax Amsterdam',
                        league: 'Eredivisie',
                        appearances: 29,
                        minutes: 200,
                        tackles: 0,
                        interceptions: 1,
                        fouls: 0,
                        yellowCards: 0,
                        redCards: 0                    
                    }
                ]
            }

        ],    
        statDomains: [
            {
                domain: 'attack',
                category: [
                    {name: 'Non-Pen Goals', description: 'Non penalty goals per game', min:0.05, max: 0.69, categoryCode: 'npGoals'},
                    {name: 'Shots', description: 'Shots per game', min: 1.03, max: 4.4, categoryCode: 'shots'},
                    {name: 'Shooting %', description: 'Percentage of shots on target', min: 28, max: 60, categoryCode: 'shotPercent'},
                    {name: 'Passing %', description: 'Percentage of completed passes', min: 65, max: 85, categoryCode: 'passPercent'},
                    {name: 'Assists', description: 'Assists per game', min: 0.03, max: 0.41, categoryCode: 'assists'},
                    {name: 'Key Passes', description: 'Key passes per game', min: 0.27, max: 4.33, categoryCode: 'keyPasses'},
                    {name: 'Int + Tackles', description: 'Interceptions + tackles per game', min: 1.03, max: 6.33, categoryCode: 'intTackles'},
                    {name: 'Succ. Dribbles', description: 'Successfull dribbles per game', min: 1.09, max: 6.17, categoryCode: 'succDribbles'},
                    {name: 'Average Rating', description: 'Average match rating', min: 6,  max: 7.35, categoryCode: 'avgRating'},
                    {name: 'Goal Conv. %', description: 'Percentage of goal scoring opportunities conveted', min: 3, max: 30, categoryCode: 'goalConvPerc'}
                ]
            },
            {
                domain: 'midfield',
                category: [
                    {name: 'Tackling %', description: 'Percentage of tackles won', min: 20 , max: 100, categoryCode: 'tacklePercent'},
                    {name: 'Passing %', description: 'Percentage of completed passes', min: 65, max: 85, categoryCode: 'passPercent'},
                    {name: 'Key Passes', description: 'Key passes per game', min: 0.27, max: 4.33, categoryCode: 'keyPasses'},
                    {name: 'Mistakes', description: 'Mistakes per game', min: 0.1, max: 5, categoryCode: 'mistakesPerGame'},
                    {name: 'Succ. Dribbles', description: 'Successfull dribbles per game', min: 1.09, max: 6.17, categoryCode: 'succDribbles'},
                    {name: 'Points/Game', description: 'Points won per game', min: 0, max: 3, categoryCode:'pointsPerGame'},
                    {name: 'Aer. Chal. %', description: 'Percentage of aerial challenges won', min: 15, max: 90, categoryCode:'aerialChallengesWonPercent'},
                    {name: 'Aer. Chal. / 90', description: 'Aerial challenges won / 90 min',min: 0, max: 5, categoryCode: 'aerialChallengesWonPer90'},
                    {name: 'Passes Comp. / 90', description: 'Passes completed / 90 min', min: 10, max: 200, categoryCode: 'passesCompletedPer90'},
                    {name: 'Tackles', description: 'Tackles per game', min: 0, max: 20, categoryCode: 'tackles'},
                    {name: 'Interceptions', description: 'Interceptions per game', min: 0, max: 20, categoryCode: 'interceptions'}
                ]
            },
            {
                domain: 'defense',
                category: [
                    {name: 'Tackling %', description: 'Percentage of tackles won', min: 20 , max: 100, categoryCode: 'tacklePercent'},
                    {name: 'Passing %', description: 'Percentage of completed passes', min: 65, max: 85, categoryCode: 'passPercent'},
                    {name: 'Key Passes', description: 'Key passes per game', min: 0.27, max: 4.33, categoryCode: 'keyPasses'},
                    {name: 'Mistakes', description: 'Mistakes per game', min: 0.1, max: 5, categoryCode: 'mistakesPerGame'},
                    {name: 'Succ. Dribbles', description: 'Successfull dribbles per game', min: 1.09, max: 6.17, categoryCode: 'succDribbles'},
                    {name: 'Points/Game', description: 'Points won per game', min: 0, max: 3, categoryCode:'pointsPerGame'},
                    {name: 'Aer. Chal. %', description: 'Percentage of aerial challenges won', min: 15, max: 90, categoryCode:'aerialChallengesWonPercent'},
                    {name: 'Aer. Chal. / 90', description: 'Aerial challenges won / 90 min',min: 0, max: 5, categoryCode: 'aerialChallengesWonPer90'},
                    {name: 'Tackles', description: 'Tackles per game', min: 0, max: 20, categoryCode: 'tackles'},
                    {name: 'Interceptions', description: 'Interceptions per game', min: 0, max: 20, categoryCode: 'interceptions'},
                    {name: 'Average Rating', description: 'Average match rating', min: 6,  max: 7.35, categoryCode: 'avgRating'}
                ]
            }
        ],
        leagues: [
            {
                id: 1,
                name: 'English Premier League'
            },
            {
                id: 2,
                name: 'Liga BBVA'
            },
            {
                id: 3,
                name: 'Eredivisie'
            },
            {
                id: 4,
                name: 'Ligue 1'
            }
        ]
};

    data.seasons = generateSeasonData(data.leagues);    
    data.playerStats = generateSeasonStats(data.statDomains, data.leagues, data.seasons);    
    
    return data;
};

module.exports = dataGenerator();