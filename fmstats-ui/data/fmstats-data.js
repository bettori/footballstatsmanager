module.exports = () => {
    const data = {
        players: [
        {
            id: '12',
            firstName: 'Andy',
            lastName: 'Van der Meyden',
            age: 45,
            height: '6 ft 1',
            weight: '186 lbs',
            nationality: 'Dutch',
            position: 'AMR',
            currentTeam: 'Retired'
        },
        {
            id: '23',
            firstName: 'Antoine',
            lastName: 'Griezmann',
            age: 29,
            height: '5 ft 8',
            weight: '176 lbs',
            nationality: 'French',
            position: 'ST',
            currentTeam: 'Barcelona'
        },
        {
            id: '24',
            firstName: 'Marcus',
            lastName: 'Rashford',
            age: 23,
            height: '6 ft 1',
            weight: '186 lbs',
            nationality: 'English',
            position: 'ST',
            currentTeam: 'Manchester United'
        }
    ],
    playerCareerData: [
        {
            id: '12',
            category: 'summary',
            statistics: [
                {
                    season: '1997 - 1998',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 4,
                    minutes: 200,
                    goals: 0,
                    assists: 1,
                    yellowCards: 0,
                    redCards: 0,
                    pointsPerGame: 2.3
                },
                {
                    season: '1998 - 1999',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 1,
                    minutes: 200,
                    goals: 0,
                    assists: 0,
                    yellowCards: 0,
                    redCards: 0,
                    pointsPerGame: 1
                },
                {
                    season: '1999 - 2000',
                    club: 'FC Twente (loan)',
                    league: 'Eredivisie',
                    appearances: 32,
                    minutes: 200,
                    goals: 2,
                    assists: 1,
                    yellowCards: 0,
                    redCards: 0,
                    pointsPerGame: 1.9
                },
                {
                    season: '2000 - 2001',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 27,
                    minutes: 200,
                    goals: 2,
                    assists: 5,
                    yellowCards: 2,
                    redCards: 0,
                    pointsPerGame: 2.1
                },
                {
                    season: '2001 - 2002',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 30,
                    minutes: 200,
                    goals: 5,
                    assists: 7,
                    yellowCards: 2,
                    redCards: 1,
                    pointsPerGame: 2.3
                },
                {
                    season: '2002 - 2003',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 29,
                    minutes: 200,
                    goals: 11,
                    assists: 7,
                    yellowCards: 0,
                    redCards: 0,
                    pointsPerGame: 2.7
                }
            ]
        },
        {
            id: '12',
            category: 'passing',
            statistics: [
                {
                    season: '1997 - 1998',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 4,
                    minutes: 200,                    
                    assists: 1,
                    keyPassesPerGame: 0,
                    passPercent: 30,
                    passesCompletedPer90: 0
                },
                {
                    season: '1998 - 1999',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 1,
                    minutes: 90,                    
                    assists: 0,
                    keyPassesPerGame: 0,
                    passPercent: 30,
                    passesCompletedPer90: 0
                },
                {
                    season: '1999 - 2000',
                    club: 'FC Twente (loan)',
                    league: 'Eredivisie',
                    appearances: 32,
                    minutes: 200,                    
                    assists: 1,
                    keyPassesPerGame: 0,
                    passPercent: 30,
                    passesCompletedPer90: 0
                },
                {
                    season: '2000 - 2001',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 27,
                    minutes: 200,                    
                    assists: 5,
                    keyPassesPerGame: 0,
                    passPercent: 30,
                    passesCompletedPer90: 0
                },
                {
                    season: '2001 - 2002',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 30,
                    minutes: 200,                    
                    assists: 7,
                    keyPassesPerGame: 0,
                    passPercent: 30,
                    passesCompletedPer90: 0
                },
                {
                    season: '2002 - 2003',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 29,
                    minutes: 200,                    
                    assists: 7,
                    keyPassesPerGame: 0,
                    passPercent: 30,
                    passesCompletedPer90: 0
                }
            ]
        },
        {
            id: '12',
            category: 'attack',
            statistics: [
                {
                    season: '1997 - 1998',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 4,
                    minutes: 200,
                    goals: 0,
                    assists: 1,
                    shotsPerGame: 0,
                    keyPassesPerGame: 0,
                    dribbles: 2.3
                },
                {
                    season: '1998 - 1999',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 1,
                    minutes: 200,
                    goals: 0,
                    assists: 0,
                    shotsPerGame: 0,
                    keyPassesPerGame: 0,
                    dribbles: 2.3
                },
                {
                    season: '1999 - 2000',
                    club: 'FC Twente (loan)',
                    league: 'Eredivisie',
                    appearances: 32,
                    minutes: 200,
                    goals: 2,
                    assists: 1,
                    shotsPerGame: 0,
                    keyPassesPerGame: 0,
                    dribbles: 2.3
                },
                {
                    season: '2000 - 2001',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 27,
                    minutes: 200,
                    goals: 2,
                    assists: 5,
                    shotsPerGame: 0,
                    keyPassesPerGame: 0,
                    dribbles: 2.3
                },
                {
                    season: '2001 - 2002',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 30,
                    minutes: 200,
                    goals: 5,
                    assists: 7,
                    shotsPerGame: 0,
                    keyPassesPerGame: 0,
                    dribbles: 2.3
                },
                {
                    season: '2002 - 2003',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 29,
                    minutes: 200,
                    goals: 11,
                    assists: 7,
                    shotsPerGame: 0,
                    keyPassesPerGame: 0,
                    dribbles: 2.3
                }
            ]
        },
        {
            id: '12',
            category: 'defense',
            statistics: [
                {
                    season: '1997 - 1998',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 4,
                    minutes: 200,
                    tackles: 0,
                    interceptions: 1,
                    fouls: 0,
                    yellowCards: 0,
                    redCards: 0
                },
                {
                    season: '1998 - 1999',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 1,
                    minutes: 200,
                    tackles: 0,
                    interceptions: 1,
                    fouls: 0,
                    yellowCards: 0,
                    redCards: 0                    
                },
                {
                    season: '1999 - 2000',
                    club: 'FC Twente (loan)',
                    league: 'Eredivisie',
                    appearances: 32,
                    minutes: 200,
                    tackles: 0,
                    interceptions: 1,
                    fouls: 0,
                    yellowCards: 0,
                    redCards: 0                    
                },
                {
                    season: '2000 - 2001',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 27,
                    minutes: 200,
                    tackles: 0,
                    interceptions: 1,
                    fouls: 0,
                    yellowCards: 2,
                    redCards: 0                    
                },
                {
                    season: '2001 - 2002',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 30,
                    minutes: 200,
                    tackles: 0,
                    interceptions: 1,
                    fouls: 0,
                    yellowCards: 2,
                    redCards: 1                    
                },
                {
                    season: '2002 - 2003',
                    club: 'Ajax Amsterdam',
                    league: 'Eredivisie',
                    appearances: 29,
                    minutes: 200,
                    tackles: 0,
                    interceptions: 1,
                    fouls: 0,
                    yellowCards: 0,
                    redCards: 0                    
                }
            ]
        }

    ],
    playerStats: [
        {
            category: 'attack',
            id: '12',
            statistics: [
                {
                    categoryCode: 'npGoals',
                    name: 'Non-Pen Goals',
                    value: 0.56,
                    min: 0.05,
                    max: 0.69
                },
                {
                    name: 'Shots', 
                    min: 1.03, 
                    max: 4.4, 
                    categoryCode: 'shots',
                    value: 3.5                    
                },
                {
                    name: 'Shooting %', 
                    min: 28, 
                    max: 60, 
                    categoryCode: 'shotPercent',
                    value: 55
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 84
                },
                {
                    name: 'Assists', 
                    min: 0.03, 
                    max: 0.41, 
                    categoryCode: 'assists',
                    value: 0.35
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Int + Tackles', 
                    min: 1.03, 
                    max: 6.33, 
                    categoryCode: 'intTackles',
                    value: 5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.4
                },
                {
                    name: 'Average Rating', 
                    min: 6,  
                    max: 7.35, 
                    categoryCode: 'avgRating',
                    value: 6.5
                },
                {
                    name: 'Goal Conv. %', 
                    min: 3, 
                    max: 30, 
                    categoryCode: 'goalConvPerc',
                    value: 23
                }                  
            ]
        },
        {
            category: 'attack',
            id: '23',
            statistics:            
            [
                {
                    categoryCode: 'npGoals',
                    name: 'Non-Pen Goals',
                    value: 0.66,
                    min: 0.05,
                    max: 0.69
                },
                {
                    name: 'Shots', 
                    min: 1.03, 
                    max: 4.4, 
                    categoryCode: 'shots',
                    value: 3.8                    
                },
                {
                    name: 'Shooting %', 
                    min: 28, 
                    max: 60, 
                    categoryCode: 'shotPercent',
                    value: 65
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 82
                },
                {
                    name: 'Assists', 
                    min: 0.03, 
                    max: 0.41, 
                    categoryCode: 'assists',
                    value: 0.36
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1.6
                },
                {
                    name: 'Int + Tackles', 
                    min: 1.03, 
                    max: 6.33, 
                    categoryCode: 'intTackles',
                    value: 5.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.1
                },
                {
                    name: 'Average Rating', 
                    min: 6,  
                    max: 7.35, 
                    categoryCode: 'avgRating',
                    value: 6.9
                },
                {
                    name: 'Goal Conv. %', 
                    min: 3, 
                    max: 30, 
                    categoryCode: 'goalConvPerc',
                    value: 29
                }                  
            ] 
        },
        {
            category: 'attack',
            id: '24',
            statistics:
            [
                {
                    categoryCode: 'npGoals',
                    name: 'Non-Pen Goals',
                    value: 0.69,
                    min: 0.05,
                    max: 0.69
                },
                {
                    name: 'Shots', 
                    min: 1.03, 
                    max: 4.4, 
                    categoryCode: 'shots',
                    value: 3.84                    
                },
                {
                    name: 'Shooting %', 
                    min: 28, 
                    max: 60, 
                    categoryCode: 'shotPercent',
                    value: 68
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 85
                },
                {
                    name: 'Assists', 
                    min: 0.03, 
                    max: 0.41, 
                    categoryCode: 'assists',
                    value: 0.54
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1.9
                },
                {
                    name: 'Int + Tackles', 
                    min: 1.03, 
                    max: 6.33, 
                    categoryCode: 'intTackles',
                    value: 5.7
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.9
                },
                {
                    name: 'Average Rating', 
                    min: 6,  
                    max: 7.35, 
                    categoryCode: 'avgRating',
                    value: 7.2
                },
                {
                    name: 'Goal Conv. %', 
                    min: 3, 
                    max: 30, 
                    categoryCode: 'goalConvPerc',
                    value: 35
                }                  
            ]   
        },
        {
            category: 'midfield',
            id: '12',
            statistics: 
            [
                {
                    name: 'Tackling %', 
                    min: 20 , 
                    max: 100, 
                    categoryCode: 'tacklePercent',
                    value: 55
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 84
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Mistakes/Game', 
                    min: 0.1, 
                    max: 5, 
                    categoryCode: 'mistakesPerGame',
                    value: 3.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.4
                },
                {
                    name: 'Points/Game', 
                    min: 0, 
                    max: 3, 
                    categoryCode:'pointsPerGame',
                    value: 1.2
                },
                {
                    name: 'Aerial Challenges Won %', 
                    min: 15, 
                    max: 90, 
                    categoryCode:'aerialChallengesWonPercent',
                    value: 23
                },
                {
                    name: 'Aerial Challenges Won Per 90 min', 
                    min: 0, 
                    max: 5, 
                    categoryCode: 'aerialChallengesWonPer90',
                    value: 4
                },
                {
                    name: 'Passes Completed Per 90 min', 
                    min: 10, 
                    max: 200, 
                    categoryCode: 'passesCompletedPer90',
                    value: 67
                },
                {
                    name: 'Tackles', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'tackles',
                    value: 11
                },
                {
                    name: 'Interceptions', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'interceptions',
                    value: 10
                }
            ]            
        },
        {
            category: 'midfield',
            id: '23',
            statistics:             
            [
                {
                    name: 'Tackling %', 
                    min: 20 , 
                    max: 100, 
                    categoryCode: 'tacklePercent',
                    value: 65
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 89
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Mistakes/Game', 
                    min: 0.1, 
                    max: 5, 
                    categoryCode: 'mistakesPerGame',
                    value: 3.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.8
                },
                {
                    name: 'Points/Game', 
                    min: 0, 
                    max: 3, 
                    categoryCode:'pointsPerGame',
                    value: 2.1
                },
                {
                    name: 'Aerial Challenges Won %', 
                    min: 15, 
                    max: 90, 
                    categoryCode:'aerialChallengesWonPercent',
                    value: 53
                },
                {
                    name: 'Aerial Challenges Won Per 90 min', 
                    min: 0, 
                    max: 5, 
                    categoryCode: 'aerialChallengesWonPer90',
                    value: 6.2
                },
                {
                    name: 'Passes Completed Per 90 min', 
                    min: 10, 
                    max: 200, 
                    categoryCode: 'passesCompletedPer90',
                    value: 67
                },
                {
                    name: 'Tackles', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'tackles',
                    value: 16
                },
                {
                    name: 'Interceptions', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'interceptions',
                    value: 10
                }
            ]           
        },
        {
            category: 'midfield',
            id: '24',
            statistics:                                                                                                                                              
            [
                {
                    name: 'Tackling %', 
                    min: 20 , 
                    max: 100, 
                    categoryCode: 'tacklePercent',
                    value: 55
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 84
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Mistakes/Game', 
                    min: 0.1, 
                    max: 5, 
                    categoryCode: 'mistakesPerGame',
                    value: 3.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.4
                },
                {
                    name: 'Points/Game', 
                    min: 0, 
                    max: 3, 
                    categoryCode:'pointsPerGame',
                    value: 2.1
                },
                {
                    name: 'Aerial Challenges Won %', 
                    min: 15, 
                    max: 90, 
                    categoryCode:'aerialChallengesWonPercent',
                    value: 33
                },
                {
                    name: 'Aerial Challenges Won Per 90 min', 
                    min: 0, 
                    max: 5, 
                    categoryCode: 'aerialChallengesWonPer90',
                    value: 7.2
                },
                {
                    name: 'Passes Completed Per 90 min', 
                    min: 10, 
                    max: 200, 
                    categoryCode: 'passesCompletedPer90',
                    value: 97
                },
                {
                    name: 'Tackles', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'tackles',
                    value: 16
                },
                {
                    name: 'Interceptions', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'interceptions',
                    value: 10
                }
            ]                  
        },
        {
            category: 'defense',
            id: '12',
            statistics: 
            [
                {
                    name: 'Tackling %', 
                    min: 20 , 
                    max: 100, 
                    categoryCode: 'tacklePercent',
                    value: 55
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 84
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Mistakes/Game', 
                    min: 0.1, 
                    max: 5, 
                    categoryCode: 'mistakesPerGame',
                    value: 3.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.4
                },
                {
                    name: 'Points/Game', 
                    min: 0, 
                    max: 3, 
                    categoryCode:'pointsPerGame',
                    value: 1.7
                },
                {
                    name: 'Aerial Challenges Won %', 
                    min: 15, 
                    max: 90, 
                    categoryCode:'aerialChallengesWonPercent',
                    value: 23
                },
                {
                    name: 'Aerial Challenges Won Per 90 min', 
                    min: 0, 
                    max: 5, 
                    categoryCode: 'aerialChallengesWonPer90',
                    value: 4
                },
                {
                    name: 'Tackles', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'tackles',
                    value: 11
                }, 
                {
                    name: 'Interceptions', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'interceptions',
                    value: 10
                },
                {
                    name: 'Average Rating', 
                    min: 6,  
                    max: 10, 
                    categoryCode: 'avgRating',
                    value: 6.5
                }
            ]           
        },
        {
            category: 'defense',
            id: '23',
            statistics:            
            [
                {
                    name: 'Tackling %', 
                    min: 20 , 
                    max: 100, 
                    categoryCode: 'tacklePercent',
                    value: 55
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 84
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Mistakes/Game', 
                    min: 0.1, 
                    max: 5, 
                    categoryCode: 'mistakesPerGame',
                    value: 3.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.4
                },
                {
                    name: 'Points/Game', 
                    min: 0, 
                    max: 3, 
                    categoryCode:'pointsPerGame',
                    value: 1.7
                },
                {
                    name: 'Aerial Challenges Won %', 
                    min: 15, 
                    max: 90, 
                    categoryCode:'aerialChallengesWonPercent',
                    value: 23
                },
                {
                    name: 'Aerial Challenges Won Per 90 min', 
                    min: 0, 
                    max: 5, 
                    categoryCode: 'aerialChallengesWonPer90',
                    value: 4
                },
                {
                    name: 'Tackles', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'tackles',
                    value: 11
                }, 
                {
                    name: 'Interceptions', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'interceptions',
                    value: 10
                },
                {
                    name: 'Average Rating', 
                    min: 6,  
                    max: 10, 
                    categoryCode: 'avgRating',
                    value: 6.5
                }
            ]           
        },
        {
            category: 'defense',
            id: '24',
            statistics: 
            [
                {
                    name: 'Tackling %', 
                    min: 20 , 
                    max: 100, 
                    categoryCode: 'tacklePercent',
                    value: 55
                },
                {
                    name: 'Passing %', 
                    min: 65, 
                    max: 85, 
                    categoryCode: 'passPercent',
                    value: 84
                },
                {
                    name: 'Key Passes', 
                    min: 0.27, 
                    max: 4.33, 
                    categoryCode: 'keyPasses',
                    value: 1
                },
                {
                    name: 'Mistakes/Game', 
                    min: 0.1, 
                    max: 5, 
                    categoryCode: 'mistakesPerGame',
                    value: 3.5
                },
                {
                    name: 'Succ. Dribbles', 
                    min: 1.09, 
                    max: 6.17, 
                    categoryCode: 'succDribbles',
                    value: 5.4
                },
                {
                    name: 'Points/Game', 
                    min: 0, 
                    max: 3, 
                    categoryCode:'pointsPerGame',
                    value: 1.7
                },
                {
                    name: 'Aerial Challenges Won %', 
                    min: 15, 
                    max: 90, 
                    categoryCode:'aerialChallengesWonPercent',
                    value: 23
                },
                {
                    name: 'Aerial Challenges Won Per 90 min', 
                    min: 0, 
                    max: 5, 
                    categoryCode: 'aerialChallengesWonPer90',
                    value: 4
                },
                {
                    name: 'Tackles', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'tackles',
                    value: 11
                }, 
                {
                    name: 'Interceptions', 
                    min: 0, 
                    max: 20, 
                    categoryCode: 'interceptions',
                    value: 10
                },
                {
                    name: 'Average Rating', 
                    min: 6,  
                    max: 10, 
                    categoryCode: 'avgRating',
                    value: 6.5
                }
            ]
        }
    ],
    statDomains: [
        {
            domain: 'attack',
            categories: [
                {name: 'Non-Pen Goals', min:0.05, max: 0.69, categoryCode: 'npGoals'},
                {name: 'Shots', min: 1.03, max: 4.4, categoryCode: 'shots'},
                {name: 'Shooting %', min: 28, max: 60, categoryCode: 'shotPercent'},
                {name: 'Passing %', min: 65, max: 85, categoryCode: 'passPercent'},
                {name: 'Assists', min: 0.03, max: 0.41, categoryCode: 'assists'},
                {name: 'Key Passes', min: 0.27, max: 4.33, categoryCode: 'keyPasses'},
                {name: 'Int + Tackles', min: 1.03, max: 6.33, categoryCode: 'intTackles'},
                {name: 'Succ. Dribbles', min: 1.09, max: 6.17, categoryCode: 'succDribbles'},
                {name: 'Average Rating', min: 6,  max: 7.35, categoryCode: 'avgRating'},
                {name: 'Goal Conv. %', min: 3, max: 30, categoryCode: 'goalConvPerc'}
            ]
        },
        {
            domain: 'midfield',
            category: [
                {name: 'Tackling %', min: 20 , max: 100, categoryCode: 'tacklePercent'},
                {name: 'Passing %', min: 65, max: 85, categoryCode: 'passPercent'},
                {name: 'Key Passes', min: 0.27, max: 4.33, categoryCode: 'keyPasses'},
                {name: 'Mistakes/Game', min: 0.1, max: 5, categoryCode: 'mistakesPerGame'},
                {name: 'Succ. Dribbles', min: 1.09, max: 6.17, categoryCode: 'succDribbles'},
                {name: 'Points/Game', min: 0, max: 3, categoryCode:'pointsPerGame'},
                {name: 'Aerial Challenges Won %', min: 15, max: 90, categoryCode:'aerialChallengesWonPercent'},
                {name: 'Aerial Challenges Won Per 90 min', min: 0, max: 5, categoryCode: 'aerialChallengesWonPer90'},
                {name: 'Passes Completed Per 90 min', min: 10, max: 200, categoryCode: 'passesCompletedPer90'},
                {name: 'Tackles', min: 0, max: 20, categoryCode: 'tackles'},
                {name: 'Interceptions', min: 0, max: 20, categoryCode: 'interceptions'}
            ]
        },
        {
            domain: 'defense',
            category: [
                {name: 'Tackling %', min: 20 , max: 100, categoryCode: 'tacklePercent'},
                {name: 'Passing %', min: 65, max: 85, categoryCode: 'passPercent'},
                {name: 'Key Passes', min: 0.27, max: 4.33, categoryCode: 'keyPasses'},
                {name: 'Mistakes/Game', min: 0.1, max: 5, categoryCode: 'mistakesPerGame'},
                {name: 'Succ. Dribbles', min: 1.09, max: 6.17, categoryCode: 'succDribbles'},
                {name: 'Points/Game', min: 0, max: 3, categoryCode:'pointsPerGame'},
                {name: 'Aerial Challenges Won %', min: 15, max: 90, categoryCode:'aerialChallengesWonPercent'},
                {name: 'Aerial Challenges Won Per 90 min', min: 0, max: 5, categoryCode: 'aerialChallengesWonPer90'},        
                {name: 'Tackles', min: 0, max: 20, categoryCode: 'tackles'},
                {name: 'Interceptions', min: 0, max: 20, categoryCode: 'interceptions'},
                {name: 'Average Rating', min: 6,  max: 10, categoryCode: 'avgRating'},
            ]
        }
    ]
};

    return data;
}