# Front end
For the time being, the frontend is setup using docker compose to run two different images:
- A graphQL server to serve up mock data
- The front end that connects to the graphQL server

In order to run the front end type in the following command
```
docker-compose -f <path-to-project>/fmstats-ui/frontend-compose.yml up -d && docker-compose -f <path-to-project>/fmstats-ui/frontend-compose.yml logs -f
```
This will bring up both images as well as display the logs.

To stop the frontend service, use the following command
```
docker-compose -f <path-to-project>/fmstats-ui/frontend-compose.yml down
```
You can also add aliases to your bash profile for simplicity.
Note that the data file used for the GraphQL server is /data/fmstats-graphql.js.

# Back end
The backend relies on a docker image of oriendDb. This can installed using
```
docker pull orientdb
```

In order to create the container run the command
```
docker run -d --name orientdb -p 2424:2424 -p 2480:2480
   -e ORIENTDB_ROOT_PASSWORD=admin orientdb:latest
   ```
Once the container is up, you can run the database integration tests found in OrientGraphDbClientTest.scala
