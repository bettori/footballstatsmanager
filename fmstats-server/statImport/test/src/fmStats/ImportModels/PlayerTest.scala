package fmStats.ImportModels

import org.scalatest.FunSuite
import fmStats.Utilities.ImportUtility.Column
import fmStats.Utilities.ImportUtility.Row
import zio.ZIO
import fmStats.Helpers.EffectTester
import fmStats.MessageStrings.ImportMessages

class PlayerTest extends FunSuite with EffectTester {

    test("Importing from row missing name should return error") {
        val columns = List(Column("test", "3"))
        val row = Row(1, columns)
        val result = Player.fromRow(row)
        testEffect(result, null, (p : Player) => assert(false), (t: Throwable) => assert(t.getMessage() == ImportMessages.playerNameNotSupplied))
    }

    test("If age string is not number format, return error") {
        val columns = List(Column("Name", "John Doe"), Column("Age", "htse"))
        val result = Player.fromRow(Row(1, columns))
        testEffect(result, null, (p: Player) => assert(false), (t: Throwable) => assert(true))
    }

    test("Can create player with name attribute only") {
        val playerName = "John Doe"
        val columns = List(Column("Name", playerName))
        val result = Player.fromRow(Row(1, columns))
        testEffect(result, null, (p: Player) => assert(p.name == playerName), (t: Throwable) => assert(false))
    }

    test("Player creation parses age correctly") {
        val playerName = "John Doe"
        val playerAge = "25"
        val columns = List(Column("Name", playerName), Column("Age", playerAge))
        val result = Player.fromRow(Row(1, columns))
        testEffect(result, null, (p: Player) => assert(p.age.exists(_ == playerAge.toInt)), (t: Throwable) => assert(false))
    }

    test("Player creation takes additional parameters if they are specified") {
        val playerName = "John Doe"
        val playerAge = "25"
        val playerWeight = "186 lbs"
        val columns = List(Column("Name", playerName), Column("Age", playerAge), Column("Weight", playerWeight))
        val result = Player.fromRow(Row(1, columns))
        testEffect(result, null, (p: Player) => assert(p.weight.exists(_ == playerWeight)), (t: Throwable) => assert(false))
    }
}