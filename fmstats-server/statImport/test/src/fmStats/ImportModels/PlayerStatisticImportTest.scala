package fmStats.ImportModels

import org.scalatest.FunSuite
import fmStats.Utilities.ImportUtility.Column
import fmStats.Utilities.ImportUtility.Row
import fmStats.Utilities.ColumnHeaders._
import scala.util.Failure
import scala.util.Success
import fmStats.Utilities.ColumnHeaders
import zio.ZIO
import fmStats.Helpers.EffectTester

class PlayerStatisticImportTest extends FunSuite with EffectTester {

    test("Importing from row missing player name should cause error") {
        val columns = List(Column("test", "3"), Column("Ps C", "30"))
        val row = Row(1, columns)
        val season = "1992-1993"
        val result = PlayerStatistics.fromRow(row)

        testEffect(result, season, 
        (stat: PlayerStatistics) => assert(false),
        (s: Throwable) => assert(s.getMessage == "Player name not provided in row 1."))
    }

    test("Importing from row with one number value should yield player statistics") {
        val playerName = "Maxi Lopez"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName), Column(ColumnHeaders.PassesCompleted.label, "30"))
        val row = Row(1, columns)
        implicit val season = "1992-1993"
        val result = PlayerStatistics.fromRow(row)

        testEffect(result, season, 
        (stat: PlayerStatistics) => assert(
            stat.player == playerName &&
            stat.statistics.length == 1 && 
            stat.importErrors.length == 0
        ),
        (s: Any) => assert(false))
    }

    test("Importing one row with percentage value should be correctly parsed (ZIO)") {
        val playerName = "Maxi Lopez"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName), 
                           Column(ColumnHeaders.PassesCompleted.label, "30"),
                           Column(ColumnHeaders.ShootingPercentage.label, "25%"))
        val row = Row(1, columns)
        val season = "1992-1993"

        val result = PlayerStatistics.fromRow(row)

        testEffect(result, season, 
        (stat: PlayerStatistics) => assert(stat.statistics.length == 2), 
        (s: Any) => assert(false))
    }

    test("Distance statistic correctly parsed with kms unit (ZIO)") {
        val playerName = "Maxi Lopez"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName), 
                           Column(ColumnHeaders.PassesCompleted.label, "30"),
                           Column(ColumnHeaders.ShootingPercentage.label, "25%"),
                           Column(ColumnHeaders.DistanceCovered.label, "25 kms"))
        val row = Row(1, columns)
        val season = "1992-1993"

        val result = PlayerStatistics.fromRow(row)
        
        testEffect(result, season,
        (stat: PlayerStatistics) => {
            val distanceCoveredOption = stat.statistics.find(_.categoryCode == ColumnHeaders.DistanceCovered.label)
            assert(stat.importErrors.length == 0 && 
            distanceCoveredOption.isDefined &&
            distanceCoveredOption.get.value == 25.0 &&
            distanceCoveredOption.get.unit.get.label == Units.Kilometers.label)
        },
        (s: Any) => assert(false))
    }

    test("Distance statistic correctly parsed with miles unit (ZIO)") {
        val playerName = "Maxi Lopez"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName), 
                           Column(ColumnHeaders.PassesCompleted.label, "30"),
                           Column(ColumnHeaders.ShootingPercentage.label, "25%"),
                           Column(ColumnHeaders.DistanceCovered.label, "25 miles"))
        
        val row = Row(1, columns)
        val season = "1992-1993"

        val result = PlayerStatistics.fromRow(row)

        testEffect(result, season, 
        (stat: PlayerStatistics) => {
            val distanceCoveredOption = stat.statistics.find(_.categoryCode == ColumnHeaders.DistanceCovered.label)
            assert(stat.importErrors.length == 0 && 
            distanceCoveredOption.isDefined &&
            distanceCoveredOption.get.value == 25.0 &&
            distanceCoveredOption.get.unit.get.label == Units.Miles.label)
        },
        (s: Any) => assert(false))
    }

    test("Distance statistic with incorrect unit reports parsing error but does not fail import") {
        val playerName = "Maxi Lopez"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName), 
                           Column(ColumnHeaders.PassesCompleted.label, "30"),
                           Column(ColumnHeaders.ShootingPercentage.label, "25%"),
                           Column(ColumnHeaders.DistanceCovered.label, "25 parsecs"))
        
        val row = Row(1, columns)
        val season = "1992-1993"

        val result = PlayerStatistics.fromRow(row)

        testEffect(result, season, 
        (stat: PlayerStatistics) => {
            val distanceCoveredOption = stat.statistics.find(_.categoryCode == ColumnHeaders.DistanceCovered.label)
            assert(stat.importErrors.length == 1 &&
            stat.importErrors.head.exception.getLocalizedMessage.contains(s"Could not parse statistics for column ${ColumnHeaders.DistanceCovered.label}") 
            )
        },
        (s: Any) => assert(false))
    }
}