package fmStats.Utilities
import fmStats.Utilities.HtmlParser
import org.scalatest.FunSuite
import scala.io.Source
import scala.jdk.javaapi.StreamConverters

class HtmlParserTest extends FunSuite {
    test("Should return non empty list") {
        val fileStream = getClass().getResourceAsStream("/PlayerData.html")        
        val rows = HtmlParser.extractRowsFromHtml(fileStream)
        assert(rows.length != 0)
        fileStream.close()
    }
}