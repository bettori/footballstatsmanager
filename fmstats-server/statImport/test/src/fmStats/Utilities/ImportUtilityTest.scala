package fmStats.Utilities
import org.scalatest.FunSuite
import fmStats.Helpers.EffectTester
import fmStats.Utilities.ImportUtility.Column
import fmStats.Utilities.ImportUtility.Row
import fmStats.Results.RowImportResults
import fmStats.MessageStrings.ImportMessages

class ImportUtilityTest extends FunSuite with EffectTester {
    test("Row with player should return import results with player") {
        val playerName = "Maxi Lopez"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName), Column(ColumnHeaders.Age.label, "30"))
        val row = Row(1, columns)
        val result = ImportUtility.extractDataFromRow(row)

        testEffect(result, null, 
        (r: RowImportResults) => {
            assert(r.playerResult.isRight &&
            r.playerResult.fold(_ => false, p => p.name == playerName && p.age.isDefined && p.age.get == 30)
            )
        }, 
        (t: Throwable) => assert(false))
    }

    test("Row without player name should result in import error") {
        val columns = List(Column(ColumnHeaders.Age.label, "30"))
        val row = Row(1, columns)
        val result = ImportUtility.extractDataFromRow(row)
        
        testEffect(result, null,
            (r: RowImportResults) => assert(false),
            (t: Throwable) => assert(t.getMessage == ImportMessages.playerNameNotSupplied)
        )
    }

    test("Row with nationality value imports country and assigns nationality to player") {
        val playerName = "Maxi Lopez"
        val country = "Argentina"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName),
                            Column(ColumnHeaders.Nationality.label, country))
        val row = Row(1, columns)
        val results = ImportUtility.extractDataFromRow(row)

        testEffect(results, null,
        (r: RowImportResults) => {
            assert(r.playerResult.isRight &&
            r.playerResult.fold(_ => false, p => p.name == playerName && p.nationality.isDefined && p.nationality.get.name == country) &&
            r.countryResult.isRight &&
            r.countryResult.fold(_ => false, c => c.name == country))
        },
        (t: Throwable) => assert(false))
    }

    test("Row with league and country imports both and assigns nationality to player") {
        val playerName = "Maxi Lopez"
        val country = "Argentina"
        val league = "Spanish Primera"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName),
                            Column(ColumnHeaders.Nationality.label, country),
                            (Column(ColumnHeaders.League.label, league)))
        val row = Row(1, columns)
        val result = ImportUtility.extractDataFromRow(row)

        testEffect(result, null, 
        (r: RowImportResults) => {
            assert(r.playerResult.isRight &&
            r.playerResult.fold(_ => false, p => p.name == playerName && p.nationality.isDefined &&
            p.nationality.get.name == country) &&
            r.countryResult.isRight &&
            r.countryResult.fold(_ => false, c => c.name == country) &&
            r.leagueResult.isRight &&
            r.leagueResult.fold(_ => false, l => l.name == league))
        }, 
        (t: Throwable) => assert(false))
    }

    test("Row with league, country, club imports all and assigns them correclty to player, league and club") {
        val playerName = "Maxi Lopez"
        val country = "Argentina"
        val league = "Spanish Primera"
        val club = "Espanyol"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName),
                            Column(ColumnHeaders.Nationality.label, country),
                            Column(ColumnHeaders.League.label, league),
                            Column(ColumnHeaders.Club.label, club))
        val row = Row(1, columns)
        val result = ImportUtility.extractDataFromRow(row)

        testEffect(result, null, 
        (r: RowImportResults) => {
            assert(r.playerResult.isRight &&
            r.playerResult.fold(_ => false, p => p.name == playerName && p.nationality.isDefined 
            && p.nationality.get.name == country &&
            p.currentTeam.isDefined && p.currentTeam.get.name == club &&
            p.currentTeam.get.league.isDefined &&
            p.currentTeam.get.league.get.name == league))
        },
        (t: Throwable) => assert(false))
    }

    test("Row with player stats imports them and assigns them to player") {
        val playerName = "Maxi Lopez"
        val season = "1992-1993"
        val columns = List(Column(ColumnHeaders.PlayerName.label, playerName),
                            Column(ColumnHeaders.Dribbles.label, "4"))
        val row = Row(1, columns)
        val result = ImportUtility.extractDataFromRow(row)

        testEffect(result, season,
        (r: RowImportResults) => {
            assert(r.playerResult.isRight && 
            r.playerResult.fold(_ => false, p => p.name == playerName &&
            r.statsResult.isRight &&
            r.statsResult.fold(_ => false, s => s.player == playerName && s.season == season && s.importErrors.length == 0 &&
            s.statistics.length == 1 && s.statistics.head.categoryCode == ColumnHeaders.Dribbles.label &&
            s.statistics.head.value == 4.0)))
        },
        (t: Throwable) => assert(false))
    }

    test("Importing well formed stats file yields import results") {
        val fileStream = getClass().getResourceAsStream("/PlayerData.html")
        val season = "1992-1993"
        val result = ImportUtility.parseDataStream(fileStream)
        
        testEffect(result, season, 
            (r: List[RowImportResults]) => {
                assert(r.length > 0)
            },
            (t: Throwable) => assert(false)
        )

        fileStream.close()
    }

    test("Managed stream import returns result for correctly formed data") {
        val fileStream = getClass().getResourceAsStream("/PlayerData.html")
        val season = "1992-1993"
        val result = ImportUtility.parseDataStreamManaged(fileStream)
        testEffect(result, season, 
            (r: List[RowImportResults]) => {
                assert(r.length > 0)
            },
            (t: Throwable) => assert(false)
        )
    }
}