package fmStats.Results

import fmStats.ImportModels.Club
import fmStats.ImportModels.Country
import fmStats.ImportModels.League
import fmStats.ImportModels.Player
import fmStats.ImportModels.PlayerStatistics

object RowImportResults {
    type ImportResult[T <: Product] = Either[Throwable, T]
}

import RowImportResults._
case class RowImportResults(clubResult: ImportResult[Club],
                        countryResult: ImportResult[Country],
                        leagueResult: ImportResult[League],
                        playerResult: ImportResult[Player],
                        statsResult: ImportResult[PlayerStatistics])

