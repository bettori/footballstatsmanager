package fmStats.MessageStrings

object ImportMessages {
    val playerNameNotSupplied = "Cannot create player because name is not supplied"
    val noCountryData = "Could not import Country because country name was not supplied"
}