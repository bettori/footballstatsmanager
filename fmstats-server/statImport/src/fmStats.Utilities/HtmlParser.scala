package fmStats.Utilities

import java.io.InputStream
import org.htmlcleaner.HtmlCleaner
import org.htmlcleaner.TagNode
import fmStats.Utilities.ImportUtility.Row
import fmStats.Utilities.ImportUtility.Column
import zio.ZIO
import zio.UIO
import zio.stream.Stream

object HtmlParser {
    def extractRowsFromHtml(htmlStream: InputStream) : List[UIO[Row]] = {
        val rows = extractDocumentStructureFromHtmlStream(htmlStream)
        val headers = extractHeaderFromStructure(rows)
        val dataRows = extractDataRowsWithIndex(rows)       

        dataRows.map(dataRow => extractImportRowFromNode(dataRow, headers))               
    }
    
    def extractDocumentStructureFromHtmlStream(htmlStream: InputStream) : List[TagNode] = {
        val cleaner = new HtmlCleaner
        val rootNode : TagNode = cleaner.clean(htmlStream)
        rootNode.getElementsByName("tr", true).toList
    }

    def extractDataRowsWithIndex(docStructure: List[TagNode]) : List[(TagNode, Int)] = {
        docStructure.slice(1, docStructure.length).zipWithIndex
    }

    def extractHeadersAndDataRowsWithIndexFromStream(htmlStream: InputStream) : (List[String], List[(TagNode, Int)]) = {
        val documentStructure = HtmlParser.extractDocumentStructureFromHtmlStream(htmlStream)
        val headers = HtmlParser.extractHeaderFromStructure(documentStructure)
        (headers, HtmlParser.extractDataRowsWithIndex(documentStructure))
    }
 
    def extractImportRowFromNode(node: (TagNode, Int), headers: List[String]) : UIO[Row] = {
        node match {
            case (row, index) => {
                val columns = row.getElementsByName("td", true).toList
                val columnObjects = columns.zip(headers).map {
                    case (column, header) => Column(header, column.getText().toString())
                }
                ZIO.succeed(Row(index, columnObjects))
            }
        }
    }

    def extractHeaderFromStructure(docStructure: List[TagNode]) : List[String] = {
        val headerRow = docStructure.head        
        headerRow.getElementsByName("th", true)
                                     .toList.map(header => header.getText()
                                     .toString())
    }

    def streamImportRows(htmlStream: InputStream) : Stream[Throwable, Row] = {
        val documentStructure = extractDocumentStructureFromHtmlStream(htmlStream)
        val headers = extractHeaderFromStructure(documentStructure)
        val documentBody = documentStructure.tail.zipWithIndex
        for {
            docNode <- Stream.fromIterable(documentBody)
            row     <- Stream.fromEffect(extractImportRowFromNode(docNode, headers))
        } yield row
    }
}