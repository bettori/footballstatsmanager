package fmStats.Utilities

import utils.StreamUtils
import utils.ReflectionUtils
import java.io.InputStream
import scala.io.BufferedSource
import fmStats.ImportModels.StatisticalCategory
import fmStats.ImportModels.Player
import scala.util.Try
import zio.Task
import zio.ZIO
import fmStats.ImportModels.Country
import fmStats.ImportModels.Club
import fmStats.ImportModels.PlayerStatistics
import fmStats.ImportModels.League
import fmStats.Results.RowImportResults
import fmStats.ImportModels.CountryOfClub
import zio.stream.Sink
import zio.stream.Stream
import org.htmlcleaner.TagNode

object ImportUtility {

    case class Column(name: String, value: String)
    case class Row(rowNumber: Integer, columns: List[Column])

    // For the time being, assume a list of pre-defined statistical categories is available
    // Domain definitions are collections of statistical categories. 
    // We want to extract all possible statistical category data from the import file
    val allStatisticalCategories : List[StatisticalCategory] = List (
        StatisticalCategory("Non-Pen Goals", "Non penalty goals per game", 0.05, 0.69, "npGoals"),
        StatisticalCategory("Shots", "Shots per game", 1.03, 4.4, "shots")
    )

    def getFileHeaderCategories(headerLine: String) : List[StatisticalCategory] = {
        val headers = headerLine.split(",").toList
        val matchingHeadersOptions : List[(String, Option[StatisticalCategory])] = headers
        .map(header => (header, allStatisticalCategories.find(category => category.name == header)))
        
        //TODO - use logging framework instead
        matchingHeadersOptions.withFilter(_._2.isEmpty)
        .map(headerOption => s"Could not find matching statistical category for ${headerOption._1}")
        .foreach(println _)

        matchingHeadersOptions.withFilter(_._2.isDefined).map(headerOption => headerOption._2.get)
    }

    def parseFileFromStream(statsStream: InputStream) : Option[Boolean] = {
        StreamUtils.withCloseable(io.Source.fromInputStream(statsStream), (statsSource: BufferedSource) => {
            val allLines = statsSource.getLines()
            val header = allLines.take(1).toList.head
            val statCategories = getFileHeaderCategories(header)
            allLines.slice(1, allLines.size).map(row => {
                row.split(",").toList.zip(statCategories)
            })
        })
        Some(true)
    }

    def extractDataFromRow(row: Row) : Task[RowImportResults] = {
        val countryEffect = Country.fromRow(row)

        val countryOfClubEffect = CountryOfClub.fromRow(row)

        val leagueEffect = countryOfClubEffect.foldM(_ =>  League.fromRow(row),
            country => League.fromRow(row).map(_.copy(country = Some(country)))
        )
        
        val clubEffect = leagueEffect.foldM(
            _ => Club.fromRow(row),
            league => Club.fromRow(row).map(_.copy(league = Some(league)))
        )

        val playerEffect = for {
            player <- Player.fromRow(row)
            clubEither <- clubEffect.either
            countryEither <- countryEffect.either
        } yield {
            (clubEither, countryEither) match {
                case (Right(club), Right(country)) => player.copy(currentTeam = Some(club), nationality = Some(country))
                case (Left(_), Right(country)) => player.copy(nationality = Some(country))
                case (Right(club), Left(_)) => player.copy(currentTeam = Some(club))
                case _ => player
            }
        }

        val statisticImportEffect = PlayerStatistics.fromRow(row)
        
        for {
            // without player it's pointless to continue import because the player links everything together
            player <- playerEffect
            countryEither <- countryEffect.either
            leagueEither <- leagueEffect.either
            clubEither <- clubEffect.either
            statImportEither <- statisticImportEffect.either
        } yield {
            RowImportResults(clubEither, countryEither, leagueEither, Right(player), statImportEither)
        }
    }

    def parseDataStream(dataStream: InputStream) : Task[List[RowImportResults]] = {
       val headersAndDataRows = HtmlParser.extractHeadersAndDataRowsWithIndexFromStream(dataStream)
       headersAndDataRows match {
           case (headers, dataRows) => {
                ZIO.foreachParN(dataRows.length)(dataRows)(dataRow => {
                    for {
                        row <- HtmlParser.extractImportRowFromNode(dataRow, headers)
                        importTask <- extractDataFromRow(row)
                    } yield (importTask)
                })
            }
       }     
    }

    def parseDataStreamManaged(dataStream: InputStream) : Task[List[RowImportResults]] = {
        ZIO.effect(dataStream).bracket(
            stream => ZIO.succeed(stream.close()),
            stream => parseDataStream(stream)
        )
    }
}