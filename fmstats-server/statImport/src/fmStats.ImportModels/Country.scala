package fmStats.ImportModels

import fmStats.Utilities.ColumnHeaders
import scala.util.Try
import scala.util.Success
import scala.util.Failure


case class Country(name: String)

object Country extends DataObject[Country] {

  override val instance: Country = Country("")

  override val importColumns: List[ColumnHeaders.ColumnHeader] = List(ColumnHeaders.Nationality)

  override def fromAttributes(attributes: Map[String,Option[String]]): Try[Country] = {
      attributes("name") match {
          case Some(country) => Success(Country(country))
          case None => Failure(new Exception("Could not import Country because country name was not supplied"))
      }
  }  
}