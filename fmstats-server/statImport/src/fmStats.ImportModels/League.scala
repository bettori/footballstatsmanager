package fmStats.ImportModels

import fmStats.Utilities.ColumnHeaders
import scala.util.Try
import scala.util.Success
import scala.util.Failure


case class League(name: String, country: Option[CountryOfClub] = None)

object League extends DataObject[League] {

  override val instance: League = League("")

  override val importColumns: List[ColumnHeaders.ColumnHeader] = List(ColumnHeaders.League)

  override def fromAttributes(attributes: Map[String,Option[String]]): Try[League] = {
    attributes("name") match {
      case Some(league) => Success(League(league))
      case None => Failure(new Exception("Could not import League because league name is not provided"))
    }
  }


}