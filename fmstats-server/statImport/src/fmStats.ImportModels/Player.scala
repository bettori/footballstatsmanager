package fmStats.ImportModels

import fmStats.Utilities.ImportUtility.Row
import fmStats.Utilities.ColumnHeaders
import scala.util.Try
import scala.util.Success
import scala.util.Failure
import fmStats.Utilities.ColumnHeaders.Club
import fmStats.MessageStrings.ImportMessages

final case class Player(name: String, 
                  age: Option[Int] = None, 
                  height: Option[String] = None, 
                  weight: Option[String] = None, 
                  position: Option[String] = None, 
                  nationality: Option[Country] = None,
                  currentTeam: Option[Club] = None)

object Player extends DataObject[Player] {
    val instance = new Player("player")
    val importColumns = List(ColumnHeaders.PlayerName,
                          ColumnHeaders.Age,
                          ColumnHeaders.Height,
                          ColumnHeaders.Weight,
                          ColumnHeaders.Position)
    
    final def fromAttributes(attributes: Map[String, Option[String]]) : Try[Player] = { 
        (attributes("name"), Try(attributes("age").map(_.toInt))) match {
            case (Some(name), Success(age)) => Success(Player(
                                                name, 
                                                age, 
                                                attributes("height"), 
                                                attributes("weight"),
                                                attributes("position")))

            case (None, Success(age)) => Failure(new Exception(ImportMessages.playerNameNotSupplied))
            case (_, Failure(exception)) => Failure(new Exception("Could not convert age value to integer.", exception))
        }
    }
    
    final def tupled = (Player.apply _).tupled
}