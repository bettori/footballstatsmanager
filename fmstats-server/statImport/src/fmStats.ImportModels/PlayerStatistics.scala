package fmStats.ImportModels
import fmStats.Utilities.ImportUtility.Row
import fmStats.Utilities.ColumnHeaders
import fmStats.Utilities.ColumnHeaders.ColumnHeader
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import zio.ZIO
import zio.Task
import zio.Ref

import StatTypes._
import zio.Cause.Fail
import scala.language.experimental

case class PlayerStatistics (player: String, season: String, statistics: List[Statistic], importErrors: List[Failure[Statistic]])

object PlayerStatistics {

  private def extractColumnValueFromRow(row: Row, playerName: String, season: String, columnMetaData: (ColumnHeader, StatType)) : Option[Try[Statistic]] = {
      columnMetaData match {
          case (column, statType) => row.columns.find(_.name == column.label).map(col => {
            val errorParsingMessage = s"Could not parse statistics for column ${column} on row for player ${playerName}"            
            statType match {
                case Distance => {
                    val columnValue = col.value
                    val splitResult = columnValue.split(Statistic.distanceUnits.map(_.label).mkString("|"))
                    (Try(splitResult.head.toDouble), Statistic.distanceUnits.find(u => columnValue.contains(u.label))) match {
                        case (Success(stat), Some(unit)) => Success(Statistic(stat, column.label, Some(unit.asInstanceOf[Units.Unit])))
                        case (Failure(exception), _) => Failure(new Exception(errorParsingMessage, exception))
                        case _ => Failure(new Exception(errorParsingMessage))
                    }
                }
                case Percentage => { 
                    Try(col.value.split("%").head.toDouble) match {
                        case Success(stat) => Success(Statistic(stat, column.label))
                        case Failure(exception) => Failure(new Exception(errorParsingMessage, exception))
                    }
                }
                case Number => Try(col.value.toDouble) match {
                    case Success(stat) => Success(Statistic(stat, column.label))
                    case Failure(exception) => Failure(new Exception(errorParsingMessage, exception))
                }
            }                                   
        })
      }
  }

  private def collectImportFailures(importResults: Iterable[Try[Statistic]]) : List[Failure[Statistic]] = {
      importResults.collect { case Failure(t) => Failure[Statistic](t)}.toList
  }

  private def collectStatistics(importResults: Iterable[Try[Statistic]]) : List[Statistic] = {
      importResults.collect { case Success(value) => value}.toList
  }
    
    def fromRow(row: Row) : Task[PlayerStatistics] = {
        ZIO.accessM[Any](season => {
            ZIO.fromOption(row.columns.find(_.name == ColumnHeaders.PlayerName.label)).foldM(
                _ => ZIO.fail(new Exception(s"Player name not provided in row ${row.rowNumber}.")),
                playerColumn => {
                    val playerName = playerColumn.value
                    val valuesFoundInRow = for(
                        columnMetaData <- Statistic.statisticalColumns;
                        res <- extractColumnValueFromRow(row, playerName, season.asInstanceOf[String], columnMetaData)
                    ) yield {
                        res
                    }
                       
                    val stats = collectStatistics(valuesFoundInRow)
                    val importErrors = collectImportFailures(valuesFoundInRow)
                    ZIO.succeed(PlayerStatistics(playerName, season.asInstanceOf[String], stats, importErrors))
                } 
            )
        })
    }
}