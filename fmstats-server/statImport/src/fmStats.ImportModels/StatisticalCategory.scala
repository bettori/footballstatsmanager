package fmStats.ImportModels
case class StatisticalCategory(name: String, description: String, min: Double, max: Double, categoryCode: String)

object StatisticalCategory {
    val categoryNames : List[String] = List(
        "Gls",
        "Shots",
        "ShT",
        "Pens S",
        "Pens",
        "Last Gl",
        "Asts/90"
    )

}



