package fmStats.ImportModels

import fmStats.Utilities.ColumnHeaders
import scala.util.Try
import scala.util.Success
import scala.util.Failure

case class Club(name: String, league: Option[League] = None)

object Club extends DataObject[Club] {

  override val instance: Club = Club("")

  override val importColumns: List[ColumnHeaders.ColumnHeader] = List(ColumnHeaders.Club)

  override def fromAttributes(attributes: Map[String,Option[String]]): Try[Club] = {
      attributes("name") match {
          case Some(club) => Success(Club(club))
          case None => Failure(new Exception("Could not create club because name is not provided"))
      }
  }


}