package fmStats.ImportModels

import fmStats.Utilities.ImportUtility.Row
import fmStats.Utilities.ColumnHeaders
import fmStats.Utilities.ColumnHeaders.ColumnHeader


object StatTypes {
    sealed abstract class StatType(val label: String)
    case object Percentage extends StatType("percentage")
    case object Number extends StatType("number")
    case object Distance extends StatType("distance")
}

object Units {
    sealed abstract class Unit(val label: String)
    case object Kilometers extends Unit("kms")
    case object Miles extends Unit("miles")
}

case class Statistic(value: Double, categoryCode: String, unit: Option[Units.Unit] = None)

import StatTypes._
object Statistic {
    val statisticalColumns: Map[ColumnHeader, StatType] = Map (
        ColumnHeaders.Appearances -> StatTypes.Number,
        ColumnHeaders.AssistsPer90 -> StatTypes.Number,
        ColumnHeaders.TotalShotsAttempted -> StatTypes.Number,
        ColumnHeaders.PassesCompleted -> StatTypes.Number,
        ColumnHeaders.PassesAttempted -> StatTypes.Number,
        ColumnHeaders.MistakesLeadingToGoals -> StatTypes.Number,
        ColumnHeaders.Mistakes -> StatTypes.Number,
        ColumnHeaders.Headers -> StatTypes.Number,
        ColumnHeaders.HeadersAttempted -> StatTypes.Number,
        ColumnHeaders.CrossesCompleted -> StatTypes.Number,
        ColumnHeaders.CrossesAttempted -> StatTypes.Number,
        ColumnHeaders.Appearances -> StatTypes.Number,
        ColumnHeaders.ShotsOnTarget -> StatTypes.Number,
        ColumnHeaders.ShotsOnTargetPer90 -> StatTypes.Number,
        ColumnHeaders.ShootingPercentage -> StatTypes.Percentage,
        ColumnHeaders.KeyPasses -> StatTypes.Number,
        ColumnHeaders.KeyTackles -> StatTypes.Number,
        ColumnHeaders.Goals -> StatTypes.Number,
        ColumnHeaders.GoalsPer90 -> StatTypes.Number,
        ColumnHeaders.Dribbles -> StatTypes.Number,
        ColumnHeaders.Interceptions -> StatTypes.Number,
        ColumnHeaders.PlayerOfMatch -> StatTypes.Number,
        ColumnHeaders.TacklesAttempted -> StatTypes.Number,
        ColumnHeaders.TacklesReceived -> StatTypes.Percentage,
        ColumnHeaders.TackesWon -> StatTypes.Number,
        ColumnHeaders.TotalTackles -> StatTypes.Number,
        ColumnHeaders.PassesAttemptedPer90 -> StatTypes.Number,
        ColumnHeaders.PassesCompletedPer90 -> StatTypes.Number,
        ColumnHeaders.DistanceCovered -> StatTypes.Distance,
        ColumnHeaders.DisctanceCoveredPer90 -> StatTypes.Distance
    )

    val distanceUnits : List[Units.Unit] = List(Units.Kilometers, Units.Miles)
}