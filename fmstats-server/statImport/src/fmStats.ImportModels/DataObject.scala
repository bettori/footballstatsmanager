package fmStats.ImportModels
import fmStats.Utilities.ImportUtility.Row
import fmStats.Utilities.ColumnHeaders.ColumnHeader
import scala.util.Try
import zio.ZIO
import zio.Task

trait DataObject[T <: Product] {
    val instance: T
    val importColumns: List[ColumnHeader]
    def fromAttributes(attributes: Map[String, Option[String]]) : Try[T]

    final lazy val importColumnMapping: Map[String, ColumnHeader] = instance.productElementNames
                                                             .toList
                                                             .slice(0, importColumns.length)
                                                             .zip(importColumns).toMap

    final def fromRow(row: Row) : Task[T] = {
        ZIO.fromTry({
            val playerArgs : Map[String, Option[String]] = for (columnKeyValue <- importColumnMapping) yield {
                columnKeyValue match {
                    case (property, columnName) => property -> row.columns
                                                                .find(_.name.toLowerCase == columnName.label.toLowerCase)
                                                                .map(_.value)
                }            
            }
            fromAttributes(playerArgs)
        })
    }
}