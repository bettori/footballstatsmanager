package fmStats.ImportModels

import fmStats.Utilities.ColumnHeaders

import scala.util.Try
import scala.util.Success
import scala.util.Failure
import fmStats.Utilities.ImportUtility.Column
import fmStats.MessageStrings.ImportMessages

case class CountryOfClub(name: String)

object CountryOfClub extends DataObject[CountryOfClub] {

  override val instance: CountryOfClub = CountryOfClub("")

  override val importColumns: List[ColumnHeaders.ColumnHeader] = List(ColumnHeaders.ClubBased)

  override def fromAttributes(attributes: Map[String,Option[String]]): Try[CountryOfClub] = {
      attributes("name") match {
          case Some(country) => Success(CountryOfClub(country))
          case None => Failure(new Exception(ImportMessages.noCountryData))
      }
  }


}