import mill._
import scalalib._

object statImport extends ScalaModule {
    def scalaVersion = "2.13.1"
    def ivyDeps = Agg(
        ivy"dev.zio:zio_2.13:1.0.0-RC18-2",
        ivy"dev.zio:zio-streams_2.13:1.0.0-RC18-2"
    )
    def moduleDeps = Seq(GeneralUtils)

    object test extends Tests {
        def ivyDeps = Agg(
            ivy"org.scalatest:scalatest_2.13:3.1.0"
        )

        def testFrameworks = Seq("org.scalatest.tools.Framework")
    }
}

object graphSchema extends ScalaModule {
    def scalaVersion = "2.13.1"
    def ivyDeps = Agg(
        ivy"com.orientechnologies:orientdb-client:3.1.1",
        ivy"com.orientechnologies:orientdb-graphdb:3.1.1",
        ivy"commons-validator:commons-validator:1.4.0",
        ivy"dev.zio:zio_2.13:1.0.0-RC18-2",
        ivy"dev.zio:zio-streams_2.13:1.0.0-RC18-2"
    )

    def moduleDeps = Seq(GeneralUtils)

    object test extends Tests {
        def ivyDeps = Agg(
            ivy"org.scalatest:scalatest_2.13:3.1.0",
            ivy"org.mockito:mockito-scala_2.13:1.14.8"
        )

        def testFrameworks = Seq("org.scalatest.tools.Framework")
    }
}

object GeneralUtils extends ScalaModule {
    def scalaVersion = "2.13.1"
    def ivyDeps = Agg(
        ivy"org.scala-lang:scala-reflect:2.13.1",
        ivy"net.sourceforge.htmlcleaner:htmlcleaner:2.2",
        ivy"dev.zio:zio_2.13:1.0.0-RC18-2",
        ivy"dev.zio:zio-streams_2.13:1.0.0-RC18-2",
        ivy"org.scalatest:scalatest_2.13:3.1.0"
    )
    
    object test extends Tests {
        def ivyDeps = Agg(
            ivy"org.scalatest:scalatest_2.13:3.1.0"
        )
        
        def testFrameworks = Seq("org.scalatest.tools.Framework")
    }
}