package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.common.ConstraintType.NOTNULL

object Country extends VertexObject[Country] {
  def apply() : Country = {
    Country(null, "")
  }

  def apply(name: String) : Country = {
    Country(null, name)
  }
  

  def apply(instance: OVertex) : Country = Country(instance, "")
  
  override val instanceOption = Some(Country(""))
  override val name: String = "Country"
  override val constraints = Map("name" -> List(NOTNULL))
}

case class Country(wrappedVertex: OVertex, name: String) extends OrientFmStatsVertex