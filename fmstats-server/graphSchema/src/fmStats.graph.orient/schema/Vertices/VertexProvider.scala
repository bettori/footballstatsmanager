package fmStats.graph.orient.schema.Vertices

import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex

trait VertexProvider[T] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String, Any]] = None): T
}

object VertexProvider {
  def getVertex[T <: OrientFmStatsVertex : VertexProvider](vInstance: OVertex, propertyValues: Option[Map[String, Any]] = None): T = implicitly[VertexProvider[T]].instance(vInstance, propertyValues)

  implicit object SeasonProvider extends VertexProvider[Season] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String, Any]] = None) = {
      propertyValues match {
        case Some(props) => Season(vInstance, props("key").toString())
        case None => Season(vInstance, "")
      }
    }
  }

  implicit object ClubProvider extends VertexProvider[Club] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String, Any]] = None) = {
      propertyValues match {
        case Some(props) => Club(vInstance, props("name").toString())
        case None => Club(vInstance, "")
      }
    }
  }
  
  implicit object StatisticProvider extends VertexProvider[Statistic] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String, Any]] = None) = {
      propertyValues match {
        case Some(props) => Statistic(vInstance, 
                                      props("value").asInstanceOf[Double], 
                                      props("categoryCode").toString, 
                                      props.get("unit").asInstanceOf[Option[String]],
                                      props.get("category").asInstanceOf[Option[StatisticalCategory]])
        case None => Statistic(vInstance)
      }
    }
  }

  implicit object LeagueProvider extends VertexProvider[League] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String, Any]] = None) = {
      propertyValues match {
        case Some(props) => League(vInstance, props("name").toString, props.get("country").asInstanceOf[Option[Country]])
        case None => League(vInstance)
      }
    }
  }

  implicit object PlayerProvider extends VertexProvider[Player] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String,Any]] = None): Player = {
      propertyValues match {
        case Some(props) => Player(vInstance,
                                   props("name").toString,
                                   props.get("age").asInstanceOf[Option[Int]],
                                   props.get("weight").asInstanceOf[Option[String]],
                                   props.get("height").asInstanceOf[Option[String]],
                                   props.get("nationality").asInstanceOf[Option[String]],
                                   props.get("position").asInstanceOf[Option[String]])
        case None => Player(vInstance)
      }
    }
  }

  implicit object StatisticalCategoryProvider extends VertexProvider[StatisticalCategory] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String,Any]] = None): StatisticalCategory = {
      propertyValues match {
        case Some(props) => StatisticalCategory(vInstance, 
                                                props.get("name").toString,
                                                props.get("description").toString,
                                                props.get("minimum").asInstanceOf[Double],
                                                props.get("maximum").asInstanceOf[Double],
                                                props.get("categoryCode").toString)                                    
        case None => StatisticalCategory(vInstance)
      }
    }
  }

  implicit object CountryProvider extends VertexProvider[Country] {
    def instance(vInstance: OVertex, propertyValues: Option[Map[String,Any]] = None): Country = {
      propertyValues match {
        case Some(props) => Country(vInstance, props.get("name").toString)                                    
        case None => Country(vInstance)
      }
    }
  }

}

// TODO : move all schema objects into fmStats.graph.orient.schema package