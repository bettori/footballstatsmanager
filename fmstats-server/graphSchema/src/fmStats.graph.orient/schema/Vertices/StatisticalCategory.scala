package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.common.ConstraintType.NOTNULL
import fmStats.graph.common.ConstraintType.UNIQUE

object StatisticalCategory extends VertexObject[StatisticalCategory] {

  def apply() : StatisticalCategory = {
    StatisticalCategory(null, "", "", 0, 0, "")
  }

  def apply(instance: OVertex) : StatisticalCategory = {
    StatisticalCategory(instance, "", "", 0, 0, "")
  }

  override val instanceOption = Some(StatisticalCategory())
  override val name: String = "StatisticalCategory"
  override val constraints = Map("name" -> List(NOTNULL, UNIQUE),
                                 "description" -> List(NOTNULL),
                                 "minimum" -> List(NOTNULL),
                                 "maximum" -> List(NOTNULL),
                                 "categoryCode" -> List(NOTNULL, UNIQUE)
  )
}

case class StatisticalCategory(wrappedVertex: OVertex, name: String, description: String, minimum: Double, maximum: Double, categoryCode: String) extends OrientFmStatsVertex