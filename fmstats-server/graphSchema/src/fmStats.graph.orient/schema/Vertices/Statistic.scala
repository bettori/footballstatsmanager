package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.common.ConstraintType.NOTNULL

object Statistic extends VertexObject[Statistic] {

    def apply() : Statistic = {
        Statistic(null, 0.0, "", Some(""), Some(StatisticalCategory()))
    }
    def apply(value: Double, categoryCode: String) : Statistic = {
        Statistic(null, value, categoryCode)
    }

    def apply(vInstance: OVertex) : Statistic = {
        Statistic(vInstance, 0.0, "")
    }

    
    override val instanceOption = Some(Statistic(0, ""))
    override val name: String = "statistic"
    override val constraints = Map("value" -> List(NOTNULL))
}

case class Statistic(wrappedVertex: OVertex, value: Double, categoryCode: String, unit: Option[String] = None, category: Option[StatisticalCategory] = None) extends OrientFmStatsVertex