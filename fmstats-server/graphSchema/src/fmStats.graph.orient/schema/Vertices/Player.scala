package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.Constraints
import fmStats.graph.common.ConstraintType
import fmStats.graph.common.ConstraintType.UNIQUE
import fmStats.graph.common.ConstraintType.NOTNULL
import fmStats.graph.common.ConstraintType.MAX
import fmStats.graph.common.ConstraintType.MIN
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex

object Player extends VertexObject[Player] {
    def apply(name: String) : Player = {
        Player(null, name)
    }

    def apply() : Player = {
        Player(null, "", Some(1), Some(""), Some(""), Some(""), Some(""))
    }

    def apply(instance: OVertex) : Player = {
        Player(instance, "")
    }

    def apply(name: String,
            age: Option[Int],
            weight: Option[String],
            height: Option[String],
            nationality: Option[String],
            position: Option[String]) : Player = {
                Player(null, name, age, weight, height, nationality, position)
            }

    override val name = "Player"
    override val instanceOption = Some(Player())
    override val constraints = Map("name" -> List(UNIQUE, NOTNULL),
                                    "age" -> List(MIN(0), MAX(120)),
                                    "weight" -> List(MIN(0), MAX(450)))
}

case class Player(wrappedVertex: OVertex,
                  name: String, 
                  age: Option[Int] = None, 
                  weight: Option[String] = None, 
                  height: Option[String] = None, 
                  nationality: Option[String] = None, 
                  position: Option[String] = None) extends OrientFmStatsVertex