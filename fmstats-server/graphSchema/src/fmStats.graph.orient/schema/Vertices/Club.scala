package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.schema.Vertices.VertexObject
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.orient.OrientFmStatsVertex
import fmStats.graph.common.ConstraintType.NOTNULL

object Club extends VertexObject[Club] {

  def apply() : Club = {
    Club(null, "")
  }

  def apply(name: String) : Club = {
    Club(null, name)
  }
  override val instanceOption = Some(Club(""))
  override val name: String = "Club"
  override val constraints = Map("name" -> List(NOTNULL))
}

case class Club(wrappedVertex: OVertex, name: String) extends OrientFmStatsVertex
