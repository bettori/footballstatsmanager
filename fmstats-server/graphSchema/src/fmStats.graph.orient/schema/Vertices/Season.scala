package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.common.ConstraintType.NOTNULL
import fmStats.graph.common.ConstraintType.UNIQUE

object Season extends VertexObject[Season] {

    def apply() : Season = {
        Season(null, "")
    }

    def apply(label: String) : Season = {
        Season(null, label)
    }

    def apply(vertex: OVertex) : Season = {
        Season(vertex, "")
    }
    
    override val name: String = "season"
    override val instanceOption = Some(Season())

    override val constraints = Map("label" -> List(NOTNULL, UNIQUE))

}

case class Season(wrappedVertex: OVertex, label: String) extends OrientFmStatsVertex {
    
}