package fmStats.graph.orient.schema.Vertices

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.orient.OrientFmStatsVertex
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.common.ConstraintType.NOTNULL
import fmStats.graph.common.ConstraintType.UNIQUE

// League is defined as a specific grouping of teams in a country
object League extends VertexObject[League] {

  def apply() : League = {
    League(null, "", Some(Country()))
  }

  def apply(name: String) : League = {
    League(null, name)
  }

  def apply(instance: OVertex) : League = League(instance, "")
  
  override val instanceOption = Some(League(""))
  override val name: String = "League"
  override val constraints = Map("name" -> List(NOTNULL, UNIQUE))
}

case class League(wrappedVertex: OVertex, name: String, country: Option[Country] = None) extends OrientFmStatsVertex