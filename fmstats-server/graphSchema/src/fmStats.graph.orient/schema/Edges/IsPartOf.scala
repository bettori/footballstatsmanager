package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.SimpleTypedEdge
import fmStats.graph.orient.schema.Vertices.Club
import fmStats.graph.orient.schema.Vertices.League

object IsPartOf extends SimpleTypedEdge[Club, League] {
    override val name: String = "isPartOf"
}