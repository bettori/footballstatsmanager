package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.SimpleTypedEdge
import fmStats.graph.orient.schema.Vertices.Country
import fmStats.graph.orient.schema.Vertices.Player

object IsCitizenOf extends SimpleTypedEdge[Player, Country] {
  override val name: String = "isCitizenOf"
}