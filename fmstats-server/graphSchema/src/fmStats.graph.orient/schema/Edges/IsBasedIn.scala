package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.TypedEdgeObject
import fmStats.graph.orient.schema.Vertices.Country
import fmStats.graph.orient.schema.Vertices.League

object IsBasedIn extends TypedEdgeObject[League, Country, Product] {
    override val name: String = "isBasedIn"
}