package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.SimpleTypedEdge
import fmStats.graph.orient.schema.Vertices.Statistic
import fmStats.graph.orient.schema.Vertices.Player

object IsAchievedBy extends SimpleTypedEdge[Statistic, Player] {
    override val name: String = "isAchievedBy"
}