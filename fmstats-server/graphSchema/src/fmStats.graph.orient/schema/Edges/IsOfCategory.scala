package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.SimpleTypedEdge
import fmStats.graph.orient.schema.Vertices.Statistic
import fmStats.graph.orient.schema.Vertices.StatisticalCategory


object IsOfCategory extends SimpleTypedEdge[Statistic, StatisticalCategory] {
    override val name: String = "isOfCategory"
}