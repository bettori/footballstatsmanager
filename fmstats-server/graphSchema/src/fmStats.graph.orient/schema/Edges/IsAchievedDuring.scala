package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.SimpleTypedEdge
import fmStats.graph.orient.schema.Vertices.Statistic
import fmStats.graph.orient.schema.Vertices.Season

object IsAchievedDuring extends SimpleTypedEdge[Statistic, Season] {
    override val name = "isAchievedDuring"
}