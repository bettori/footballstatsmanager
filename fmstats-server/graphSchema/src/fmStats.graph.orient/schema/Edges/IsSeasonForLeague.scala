package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.SimpleTypedEdge
import fmStats.graph.orient.schema.Vertices.League
import fmStats.graph.orient.schema.Vertices.Season

object IsSeasonForLeague extends SimpleTypedEdge[Season, League] {
    override val name: String = "isSeasonForLeague"
}