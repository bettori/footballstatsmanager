package fmStats.graph.orient.schema.Edges

import fmStats.graph.schema.Edges.TypedEdgeObject
import fmStats.graph.orient.schema.Vertices.Player
import fmStats.graph.orient.schema.Vertices.Club

object IsContractedTo extends TypedEdgeObject[Player, Club, IsContractedTo] {
    override val instanceOption = Some(IsContractedTo(new java.util.Date(), new java.util.Date()))
    val name: String = "isContractedTo"
}

case class IsContractedTo(contractStartDate: java.util.Date, contractEndDate: java.util.Date)