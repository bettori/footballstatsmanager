package fmStats.graph.orient

import fmStats.graph.common.FmStatsEdge
import com.orientechnologies.orient.core.record.OEdge

case class OrientFmStatsEdge(edgeInstance : OEdge) extends FmStatsEdge {
    def setProperty(label: String, value: Any) : Unit = {
        edgeInstance.setProperty(label, value)
    }
}