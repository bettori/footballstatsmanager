package fmStats.graph.orient

import fmStats.graph.orient.OrientFmStatsEdge
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.common.FmStatsEdge
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import com.orientechnologies.orient.core.record.OVertex
import fmStats.graph.connection.OrientDbGraphConnection
import com.orientechnologies.orient.core.record.OEdge

trait OrientFmStatsVertex extends FmStatsVertex {
    val wrappedVertex: OVertex

    // TODO : add static method in companion object to create vertex
    // Propagate to graph client
    def addEdge[U <: FmStatsVertex](edgeName: String, endVertex: U) : FmStatsEdge = {
        endVertex match {
            case vertex :  OrientFmStatsVertex => {
                val edgeCreator = (edgeName: String, startVertex: OVertex, endVertex: OVertex) => {
                    val createdEdge = startVertex.addEdge(endVertex, edgeName)
                    createdEdge.save()
                    createdEdge
                }
                val wrappedEdge = OrientDbGraphConnection.createEdge(edgeName, wrappedVertex, vertex.wrappedVertex, edgeCreator)
                OrientFmStatsEdge(wrappedEdge)
            }
            case _ => throw new Exception("Unexpected vertex type")
        }
    }
}