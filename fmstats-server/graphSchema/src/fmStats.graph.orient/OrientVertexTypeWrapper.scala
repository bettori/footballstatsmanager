package fmStats.graph.orient

import fmStats.graph.common.VertexTypeWrapper
import com.orientechnologies.orient.core.metadata.schema.OClass

case class OrientVertexTypeWrapper(val vertexTypeInstance: OClass) extends VertexTypeWrapper {

}