package fmStats.graph.orient

import fmStats.graph.common.EdgeTypeWrapper
import com.orientechnologies.orient.core.metadata.schema.OClass

case class OrientEdgeTypeWrapper(edgeTypeInstance: OClass) extends EdgeTypeWrapper