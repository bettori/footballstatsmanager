package fmStats.graph.common

trait Property {
    val label: String
}