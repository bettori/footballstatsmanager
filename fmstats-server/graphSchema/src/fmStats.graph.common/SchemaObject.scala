package fmStats.graph.common

import fmStats.graph.common.PropertyDefinition
import fmStats.graph.common.GraphType
import fmStats.graph.common.GraphType.GraphType
import java.{util => ju}

trait SchemaObject[T <: Product] {
    val instanceOption: Option[T] = None
    val name: String

    lazy val propertyMapping : Map[String, PropertyDefinition] = instanceOption match {
        case Some(instance) =>
            instance.productElementNames.toList
            .zip(instance.productIterator.toList)
            .map({
                case (label, value) => label -> StandardPropertyDefinition(matchPropertyType(value))
            }).toMap
        case None => Map()
    }
    
    protected  def matchPropertyType(value: Any) : GraphType = value match {
        case s : String => GraphType.STRING 
        case Some(s: String) => GraphType.STRING
        case b: Boolean => GraphType.BOOLEAN
        case Some(b: Boolean) => GraphType.BOOLEAN
        case d: Double => GraphType.DOUBLE
        case Some(d: Double) => GraphType.DOUBLE
        case i: Int => GraphType.INTEGER
        case Some(i: Int) => GraphType.INTEGER
        case d: ju.Date => GraphType.DATE
        case Some(d: ju.Date) => GraphType.DATE
        case Some(a: AnyRef) => GraphType.LINK
        case a : AnyRef => GraphType.LINK
    }
}