package fmStats.graph.common

import scala.annotation.StaticAnnotation

final class Constraints(val constraints: List[fmStats.graph.common.ConstraintType.ConstraintType]) extends StaticAnnotation