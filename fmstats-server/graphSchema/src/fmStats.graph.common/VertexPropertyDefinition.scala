package fmStats.graph.common

import com.orientechnologies.orient.core.metadata.schema.OType
import com.tinkerpop.blueprints.impls.orient.OrientVertexType
import com.tinkerpop.blueprints.impls.orient.OrientExtendedGraph
import org.apache.commons.validator.GenericValidator
import java.{util => ju}
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.schema.Vertices.VertexObject
import fmStats.graph.common.GraphType

case class VertexPropertyDefinition (
    val propertyType: GraphType.GraphType,
    val constraints: Option[List[ConstraintType.ConstraintType]] = None,
    val linkRelationship: Option[VertexObject[_]] = None
) extends PropertyDefinition