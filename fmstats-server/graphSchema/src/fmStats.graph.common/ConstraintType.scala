package fmStats.graph.common

object ConstraintType {
    sealed abstract class ConstraintType(label: String, value: Option[Any] = None)
    case object UNIQUE extends ConstraintType("unique")
    case object NOTNULL extends ConstraintType("not null")
    case class MIN(value: Int) extends ConstraintType("min", Some(value))
    case class MAX(value: Int) extends ConstraintType("max", Some(value))
}