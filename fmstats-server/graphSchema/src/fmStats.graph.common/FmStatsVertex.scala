package fmStats.graph.common

trait FmStatsVertex {
    def addEdge[U <: FmStatsVertex](edgeName: String, endVertex: U) : FmStatsEdge
}