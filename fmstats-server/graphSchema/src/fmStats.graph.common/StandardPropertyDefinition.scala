package fmStats.graph.common

import fmStats.graph.common.GraphType

case class StandardPropertyDefinition (
    val propertyType: GraphType.GraphType
) extends PropertyDefinition