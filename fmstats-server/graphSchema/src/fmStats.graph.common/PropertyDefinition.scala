package fmStats.graph.common

import com.orientechnologies.orient.core.metadata.schema.OType
import org.apache.commons.validator.GenericValidator
import java.{util => ju}
import fmStats.graph.common.GraphType

trait PropertyDefinition {
     val propertyType: GraphType.GraphType
     def valueIsOfExpectedType(value: Any) : Boolean = {
        propertyType match {
            case GraphType.STRING => value.isInstanceOf[String]
            case GraphType.BOOLEAN => value.isInstanceOf[Boolean]
            case GraphType.DATE => {
                value.isInstanceOf[ju.Date] || (value.isInstanceOf[String] && GenericValidator.isDate(value.asInstanceOf[String], ju.Locale.US))
            }
            case GraphType.INTEGER => value.isInstanceOf[Int]
            case GraphType.ANY => true
            case _ => false
        }
    }
}