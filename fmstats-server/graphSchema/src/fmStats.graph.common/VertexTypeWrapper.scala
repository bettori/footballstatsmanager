package fmStats.graph.common

import com.tinkerpop.blueprints.impls.orient.OrientVertexType

trait VertexTypeWrapper {
    val vertexTypeInstance: Any
}