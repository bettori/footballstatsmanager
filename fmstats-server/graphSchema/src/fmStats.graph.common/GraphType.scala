package fmStats.graph.common

object GraphType {
    sealed abstract class GraphType(val label: String)
    case object STRING extends GraphType("string")
    case object DOUBLE extends GraphType("double")
    case object DATE extends GraphType("date")
    case object LINK extends GraphType("link")
    case object BOOLEAN extends GraphType("boolean")
    case object ANY extends GraphType("any")
    case object INTEGER extends GraphType("integer")
}