package fmStats.graph.connection

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.common.FmStatsEdge
import fmStats.graph.common.EdgeTypeWrapper
import scala.util.Try
import fmStats.graph.common.StandardPropertyDefinition
import fmStats.graph.common.PropertyDefinition
import zio.RIO
import zio.Task
import fmStats.graph.schema.Vertices.VertexObject

trait GraphClient {
    def createVertexType(vertexTypeName: String, propertyMap: Map[String, VertexPropertyDefinition]): Task[VertexTypeWrapper]
    def addVertex[T <: FmStatsVertex](vertexName: String, propertyTypeMappings: Map[String, VertexPropertyDefinition], propertyValues: Option[Map[String, Any]]) : Task[FmStatsVertex]
    def addVertex[T <: Product](vertexObject: VertexObject[T], propertyValues: Option[Map[String, Any]] = None) : Task[FmStatsVertex]
    def createEdgeType(edgeTypeName: String, propertyTypeMappings: Map[String, PropertyDefinition]): Task[EdgeTypeWrapper]
    def addEdge[T <: FmStatsVertex, U <: FmStatsVertex, V <: Product](edgeName: String, 
                startVertex: T, 
                endVertex: U, 
                propertyTypeMappings: Map[String, PropertyDefinition],
                propertyValues: Option[V]) : Task[FmStatsEdge]
}