package fmStats.graph.connection

import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.common.GraphType
import fmStats.graph.common.PropertyDefinition
import fmStats.graph.common.FmStatsEdge
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.orient.OrientFmStatsEdge
import fmStats.graph.orient.OrientFmStatsVertex
import fmStats.graph.orient.OrientEdgeTypeWrapper
import fmStats.graph.orient.OrientVertexTypeWrapper
import fmStats.graph.orient.OrientFmStatsVertex
import fmStats.graph.schema.Edges.TypedEdgeObject
import fmStats.graph.orient.schema.Vertices.VertexProvider
import fmStats.graph.orient.schema.Vertices.Season
import fmStats.graph.orient.schema.Vertices.Club
import fmStats.graph.orient.schema.Vertices.Statistic
import fmStats.graph.orient.schema.Vertices.League
import fmStats.graph.orient.schema.Vertices.Player
import fmStats.graph.orient.schema.Vertices.Country
import fmStats.graph.orient.schema.Vertices.StatisticalCategory
import fmStats.graph.connection.OrientDbGraphConnection
import scala.util.Try
import com.orientechnologies.orient.core.metadata.schema.OType
import com.orientechnologies.orient.core.db.OrientDB
import com.orientechnologies.orient.core.db.OrientDBConfig
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.db.ODatabaseType
import com.orientechnologies.orient.core.metadata.schema.OClass
import com.orientechnologies.orient.core.record.OVertex
import zio.RIO
import zio.ZIO
import zio.Task
import fmStats.graph.schema.Vertices.VertexObject
import com.orientechnologies.orient.core.record.OElement
import com.orientechnologies.orient.core.record.OEdge
import com.orientechnologies.orient.core.metadata.schema.OProperty
import fmStats.graph.common.ConstraintType
import scala.collection.immutable.LazyList.cons
import fmStats.graph.common.ConstraintType.NOTNULL
import fmStats.graph.common.ConstraintType.MIN
import fmStats.graph.common.ConstraintType.MAX
import fmStats.graph.common.ConstraintType.UNIQUE


class OrientDbGraphClient() extends GraphClient {
  
  override def createVertexType(vertexTypeName: String, propertyMap: Map[String, VertexPropertyDefinition]): Task[VertexTypeWrapper] = {
      createOrientVertexType(vertexTypeName, propertyMap).map(vertexType => OrientVertexTypeWrapper(vertexType))
  }

  def createVertexType[T <: Product](vertexObject: VertexObject[T]) : Task[VertexTypeWrapper] = {
      createVertexType(vertexObject.name, vertexObject.propertyMapping)
  }

  override def addVertex[T <: FmStatsVertex](vertexName: String, propertyTypeMappings: Map[String, VertexPropertyDefinition], propertyValues: Option[Map[String, Any]]) : Task[FmStatsVertex] = {
    ZIO.fromTry(Try {
        OrientDbGraphConnection.createVertex(vertexName, (vertex: OVertex) => {
            propertyValues match {
                case Some(propValues) => {
                    propValues.foreach({
                        case (property, value) if propertyTypeMappings(property).valueIsOfExpectedType(value) => vertex.setProperty(property, value)                                                
                        case (property, value) => throw new Exception(s"Could not assign value $value to property ${property}, expected type ${propertyTypeMappings(property)}, got ${value.getClass}")
                    })
                    vertex.save()
                    createVertex(vertexName, vertex, propertyValues)
                }
                case None => vertex.save(); createVertex(vertexName, vertex)
            }
        })
    })
  }

  def addVertex[T <: Product](vertexObject : VertexObject[T], propertyValues: Option[Map[String, Any]] = None) : Task[FmStatsVertex] = {
      addVertex(vertexObject.name, vertexObject.propertyMapping, propertyValues)
  }

  override def createEdgeType(edgeTypeName: String, propertyTypeMappings : Map[String, PropertyDefinition]): Task[OrientEdgeTypeWrapper] = {     
        createOrientEdgeTypeIfNotExists(edgeTypeName).map(edgeType => {
            propertyTypeMappings.foreach {
                case (property, propertyDefinition) => Option(edgeType.getProperty(property)) match {
                    case Some(definedProperty) => definedProperty
                    case None => edgeType.createProperty(property, getOrientType(propertyDefinition.propertyType))
                }
            }
            OrientEdgeTypeWrapper(edgeType) 
        })       
    }

  override def addEdge[T <: FmStatsVertex, U <: FmStatsVertex, V <: Product] (
                edgeName: String, 
                startVertex: T, 
                endVertex: U, 
                propertyTypeMappings: Map[String, PropertyDefinition],
                propertyValues: Option[V]) : Task[FmStatsEdge] = {
                   ZIO.fromTry(Try {
                        (startVertex, endVertex) match {
                            case (s : OrientFmStatsVertex, e: OrientFmStatsVertex) => {
                                OrientDbGraphConnection.createEdge(edgeName, s.wrappedVertex, e.wrappedVertex, (name, startVertex, endVertex) => {
                                    val edge = startVertex.addEdge(endVertex, name)
                                    val enhancedEdge = propertyValues match {
                                        case Some(properties) => {
                                            properties.productElementNames.toList.zip(properties.productIterator.toList).foreach {
                                                case (property, value) if propertyTypeMappings(property).valueIsOfExpectedType(value) => edge.setProperty(property, value)
                                                case (property, value) => throw new Exception(s"Could not assign value $value to property ${property}, expected type ${propertyTypeMappings(property)}, got ${value.getClass}")
                                            }
                                            edge
                                        }
                                        case None => edge
                                    }
                                    enhancedEdge.save()
                                    OrientFmStatsEdge(enhancedEdge)
                                })
                            }
                            case _ => throw new Exception("Vertex types are not known by Orient Db") 
                        }
                    })
                }
    
    def addEdge[T <: FmStatsVertex, U <: FmStatsVertex, V <: Product] (
        edgeType: TypedEdgeObject[T, U, V],
        startVertex: T, 
        endVertex: U, 
        propertyValues: Option[V] = None) : Task[FmStatsEdge] = {
            addEdge(edgeType.name, startVertex, endVertex, edgeType.propertyMapping, propertyValues)
    }

  private def createOrientVertexType(vertexTypeName: String, propertyMap: Map[String, VertexPropertyDefinition]) : Task[OClass] = { 
        createOrientVertexTypeIfNotExists(vertexTypeName).map(vertexType => {
            propertyMap.foreach {
                case (property, VertexPropertyDefinition(pType, constraintOption, linkOption)) => Option(vertexType.getProperty(property)) match {
                    case Some(definedProperty) => definedProperty
                    case None => (constraintOption, linkOption) match {
                        case (None, None) => vertexType.createProperty(property, getOrientType(pType))
                        case (None, Some(link)) => createOrientVertexType(link.name, link.propertyMapping)
                                                                                .map(vType => {
                                                                                    vertexType.createProperty(property, getOrientType(pType), vType)
                                                                                })
                        case(Some(constraints), None) => {
                            val vProperty = vertexType.createProperty(property, getOrientType(pType))
                            constraints.foldLeft(vProperty)((prop, constraint) => mapConstraint(prop, vertexType, constraint))
                        }
                        case (Some(constraints), Some(link)) => {
                            createOrientVertexType(link.name, link.propertyMapping)
                            .map(vType => {
                                vertexType.createProperty(property, getOrientType(pType), vType)
                            })
                            .map(vProperty => constraints.foldLeft(vProperty)((prop, constraint) => mapConstraint(prop, vertexType, constraint)))
                        }
                    }
                }
            }
        vertexType   
    })
  }

  private def createOrientVertexTypeIfNotExists(vertexTypeName: String) : Task[OClass] = {
      ZIO.effect(OrientDbGraphConnection.dbSession.createClassIfNotExist(vertexTypeName, "V"))
  }

  private def createOrientEdgeTypeIfNotExists(edgeTypeName: String) : Task[OClass] = {
      ZIO.effect(OrientDbGraphConnection.dbSession.createClassIfNotExist(edgeTypeName, "E"))
  }

  private def getOrientType(graphType: GraphType.GraphType) : OType = {
      graphType match {
          case GraphType.DATE => OType.DATE
          case GraphType.DOUBLE => OType.DOUBLE
          case GraphType.BOOLEAN => OType.BOOLEAN
          case GraphType.STRING => OType.STRING
          case GraphType.LINK => OType.LINK
          case GraphType.INTEGER => OType.INTEGER
          case GraphType.ANY => OType.ANY
      }
  }

  private def createVertex(vertexName: String, vertex: OVertex, propertyValues: Option[Map[String, Any]] = None) : OrientFmStatsVertex = {
      vertexName match {
          case Season.name => VertexProvider.getVertex[Season](vertex, propertyValues)
          case Club.name => VertexProvider.getVertex[Club](vertex, propertyValues)
          case Statistic.name => VertexProvider.getVertex[Statistic](vertex, propertyValues)
          case League.name => VertexProvider.getVertex[League](vertex, propertyValues)
          case Player.name => VertexProvider.getVertex[Player](vertex, propertyValues)
          case StatisticalCategory.name => VertexProvider.getVertex[StatisticalCategory](vertex, propertyValues)
          case Country.name => VertexProvider.getVertex[Country](vertex, propertyValues)
      }
  }

  private def mapConstraint(property: OProperty, vertex: OClass, constraint: ConstraintType.ConstraintType) : OProperty = {
      constraint match {
          case NOTNULL => property.setNotNull(true)
          case MIN(value) => property.setMin(value.toString)
          case MAX(value) => property.setMax(value.toString)
          case UNIQUE => {
              vertex.createIndex(s"${property.getName()}Idx", OClass.INDEX_TYPE.UNIQUE, property.getName())
              property
          }
      }
  }
}