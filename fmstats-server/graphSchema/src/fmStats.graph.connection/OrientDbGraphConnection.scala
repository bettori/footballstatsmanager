package fmStats.graph.connection

import com.orientechnologies.orient.core.db.OrientDB
import com.orientechnologies.orient.core.db.OrientDBConfig
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.db.ODatabaseType
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import zio.ZIO
import com.orientechnologies.orient.core.metadata.schema.OClass
import com.orientechnologies.orient.core.record.OElement
import com.orientechnologies.orient.core.record.OVertex
import com.orientechnologies.orient.core.record.OEdge

object OrientDbGraphConnection extends GraphConnection {
    private val dbUrl: String = "remote:localhost" // config value
    private val dbName: String = "fmStats" //config value
    private val dbType: String = "remote" //options are 'remote', 'embedded', from config
    private val rootPassword: String = "admin" //config

    // TODO: define configuration file and pass in config options
    private val orientDb : OrientDB = {
        if (dbType == "remote") {
            new OrientDB(dbUrl, "root", rootPassword, OrientDBConfig.defaultConfig())
        } else {
            new OrientDB(dbUrl, OrientDBConfig.defaultConfig())
        }
    }

    // TODO: define proper user name and password retrieval through config
    lazy val dbSession : ODatabaseSession = {
        orientDb.createIfNotExists(dbName, ODatabaseType.PLOCAL) // TODO: db type should be config value
        orientDb.open(dbName, "admin", "admin") // TODO : need to define users in config
    }

    def createVertex[T](name: String, vertexCreator: (OVertex) => T) : T = {
        Try {
            dbSession.begin()
            val vertexInstance : OVertex = dbSession.newVertex(name)
            vertexCreator(vertexInstance)
        } match {
            case Success(result) => {
                dbSession.commit()
                result
            }
            case Failure(exception) => {
                println(exception.getMessage())
                dbSession.rollback()
                throw exception
            }
        }
    }

    def createEdge[T](edgeName: String, startVertex: OVertex, endVertex: OVertex, edgeCreator: (String, OVertex, OVertex) => T) : T = {
        Try {
            dbSession.begin()
            edgeCreator(edgeName, startVertex, endVertex)
        } match {
            case Success(value) => {
                dbSession.commit()
                value
            }
            case Failure(exception) => {
                println(exception.getMessage())
                dbSession.rollback()
                throw exception
            }
        }
    }

    def shutDown() : Unit = {
        dbSession.close()
        orientDb.close()
    }
}