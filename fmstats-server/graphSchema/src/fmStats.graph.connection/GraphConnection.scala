package fmStats.graph.connection

trait GraphConnection {
    val dbSession: Any
    def shutDown(): Unit
}