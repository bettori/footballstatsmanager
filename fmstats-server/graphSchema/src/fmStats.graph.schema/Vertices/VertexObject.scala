package fmStats.graph.schema.Vertices

import fmStats.graph.connection.GraphClient
import fmStats.graph.common.Property
import fmStats.graph.common.VertexPropertyDefinition
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.common.GraphType
import fmStats.graph.common.SchemaObject
import fmStats.graph.common.GraphType.GraphType 
import scala.util.Try
import java.{util => ju}
import scala.annotation.meta._
import scala.reflect.runtime.universe._
import zio.Task
import zio.RIO
import fmStats.graph.common.Constraints
import fmStats.graph.common.ConstraintType
import fmStats.graph.common.GraphType.LINK

trait VertexProperties {
    private[Vertices] abstract class VertexProperty(val label: String) extends Property
}

trait VertexObject[T <: Product] extends SchemaObject[T] {
    def apply() : T
    val constraints : Map[String, List[fmStats.graph.common.ConstraintType.ConstraintType]]
    override lazy val propertyMapping: Map[String, VertexPropertyDefinition] = instanceOption match {
        case Some(instance) => instance.productElementNames.toList.tail
                                .zip(instance.productIterator.toList.tail)
                                .map({
                                    case (label, value) => label -> {
                                        val propertyType = matchPropertyType(value)
                                        propertyType match {
                                            case LINK => VertexPropertyDefinition(propertyType, Some(constraints(label)), Some(value.asInstanceOf[VertexObject[_ <: Product]]))
                                            case _ => VertexPropertyDefinition(propertyType, Some(constraints(label)))
                                        }                                        
                                    }
                                }).toMap
        case None => Map()
    }

    // If using annotations can retrieve them from this method
    def getConstaints[T <: Product]()(implicit m: Manifest[T]) : List[List[Tree]] = {
        // https://stackoverflow.com/questions/23046958/accessing-an-annotation-value-in-scala
        val constraintTree = typeOf[T]
        .decls
        .find(a => a.isMethod && a.asMethod.isPrimaryConstructor)
        .get
        .asMethod
        .paramss
        .flatten
        .flatMap(_.annotations)
        .filter(_.tree.tpe =:= typeOf[Constraints])
       
        constraintTree.map(x => x.tree.children.tail).flatten
        .map(_.children.tail)
    }

    // Define properties of orient db schema
    def defineVertexType() : RIO[GraphClient, VertexTypeWrapper] = {
        RIO.accessM[GraphClient](graphClient => graphClient.createVertexType(name, propertyMapping))
    }
    
    def createVertex[T <: FmStatsVertex](propertyValues: Option[Map[String, Any]] = None) : RIO[GraphClient, FmStatsVertex] = {
        val formattedPropValues = propertyValues.map {
           _.map({
                case (k, Some(v)) => (k, v)
                case kv => kv
            })
        }
        RIO.accessM[GraphClient](graphClient => graphClient.addVertex[T](name, propertyMapping, formattedPropValues))        
    }
}