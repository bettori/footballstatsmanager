package fmStats.graph.schema.Edges

import scala.util.Try
import fmStats.graph.common.Property
import fmStats.graph.common.EdgeTypeWrapper
import fmStats.graph.common.StandardPropertyDefinition
import fmStats.graph.common.FmStatsEdge
import fmStats.graph.connection.GraphClient
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.common.SchemaObject
import zio.RIO

trait EdgeProperties {
    private[Edges] abstract class EdgeProperty(val label: String) extends Property
}

trait TypedEdgeObject[+T, +U, V <: Product] extends SchemaObject[V] {
    def defineEdgeType() : RIO[GraphClient, EdgeTypeWrapper] = {
        RIO.accessM[GraphClient](graphClient => graphClient.createEdgeType(name, propertyMapping))        
    }

    def createEdge(startVertex: FmStatsVertex, endVertex: FmStatsVertex, edgeProperties: Option[V] = None) : RIO[GraphClient, FmStatsEdge] = {
        RIO.accessM[GraphClient](graphClient => graphClient.addEdge(name, startVertex, endVertex, propertyMapping, edgeProperties))
    }
}

trait SimpleTypedEdge[T, U] extends TypedEdgeObject[T, U, Product] {
    override final val instanceOption: Option[Product] = None
}