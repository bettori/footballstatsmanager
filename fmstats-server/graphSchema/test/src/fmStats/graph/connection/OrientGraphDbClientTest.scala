package fmStats.graph.connection

import org.scalatest.FunSuite
import utils.test.EffectTester
import fmStats.graph.orient.schema.Vertices.Player
import fmStats.graph.orient.schema.Vertices.Club
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.common.FmStatsVertex
import fmStats.graph.orient.OrientFmStatsVertex
import org.scalatest.BeforeAndAfterAll
import fmStats.graph.orient.schema.Edges.IsContractedTo
import fmStats.graph.common.EdgeTypeWrapper
import java.time.chrono.IsoEra
import fmStats.graph.common.FmStatsEdge
import net.bytebuddy.implementation.bytecode.Throw
import org.scalatest.BeforeAndAfterEach
import fmStats.graph.orient.OrientFmStatsEdge
import com.orientechnologies.orient.core.record.ODirection
import scala.annotation.meta._
import scala.reflect.runtime.universe._
import fmStats.graph.common.Constraints
import fmStats.graph.common.ConstraintType.UNIQUE

class OrientGraphDbClientTest extends FunSuite with EffectTester with BeforeAndAfterAll {
    test("Can create new vertex type in orient db") {        
        val sut = new OrientDbGraphClient
        val result = sut.createVertexType(Club.name, Club.propertyMapping)
        testEffect(result, sut, (t: VertexTypeWrapper) => {
            assert(t != null)
            assert(t.vertexTypeInstance != null)
        }, 
        (t: Throwable) => assert(false))
    }

    test("Can add new vertex in orientdb") {
        val sut = new OrientDbGraphClient
        val result = sut.addVertex(Player, Some(Map(
        "name" -> "Zinedine Zidane", 
        "age" -> 35, 
        "nationality" -> "France",
        "position" -> "AM")))
    
        testEffect(result, sut, 
        (v: FmStatsVertex) => {
            try {
                assert(v != null && v.isInstanceOf[OrientFmStatsVertex])
                assert(v.isInstanceOf[Player])
                val p = v.asInstanceOf[Player]
                assert(p.name == "Zinedine Zidane")
                assert(p.age == Some(35))
                assert(p.nationality == Some("France"))
                assert(p.position == Some("AM"))
            } finally {
                val orientVertex = v.asInstanceOf[OrientFmStatsVertex]
                orientVertex.wrappedVertex.delete()
            }
        }, 
        (t: Throwable) => assert(false))
    }

    test("Can create new edge type in orient db") {
        val sut = new OrientDbGraphClient
        val result = sut.createEdgeType(IsContractedTo.name, IsContractedTo.propertyMapping)
        testEffect(result, sut, (r: EdgeTypeWrapper) => assert(r.edgeTypeInstance != null), (t: Throwable) => assert(false))
    }

    test("Can create new edge instance in orient db") {
        val sut = new OrientDbGraphClient
        val result =  for {
            player <- sut.addVertex(Player.name, Player.propertyMapping, Some(Map(
                        "name" -> "Zinedine Zidane", 
                        "age" -> 35, 
                        "nationality" -> "France",
                        "position" -> "AM"
                    )))
            club <- sut.addVertex(Club, Some(Map(
                        "name" -> "Olympique de Marseille"
                    )))
            contract <- sut.addEdge(IsContractedTo, player, club, Some(IsContractedTo(new java.util.Date(), new java.util.Date())))
        } yield {
            (contract, player, club)
        }

        testEffect(result, sut, 
        (r: (FmStatsEdge, FmStatsVertex, FmStatsVertex)) => {
            try {
                assert(r._1 != null && r._1.isInstanceOf[OrientFmStatsEdge])
                val test = r._1.asInstanceOf[OrientFmStatsEdge]
                import scala.collection.JavaConverters._
                val edges = r._2.asInstanceOf[OrientFmStatsVertex].wrappedVertex.getEdges(ODirection.BOTH).asScala
                assert(edges.head.getIdentity() == test.edgeInstance.getIdentity())
            } finally {
                r._1.asInstanceOf[OrientFmStatsEdge].edgeInstance.delete()
                r._2.asInstanceOf[OrientFmStatsVertex].wrappedVertex.delete()
                r._3.asInstanceOf[OrientFmStatsVertex].wrappedVertex.delete()
            }
        }, 
        (t: Throwable) =>{ 
            println(t.getMessage())
            assert(false)
        })
    }
    
    override protected def afterAll(): Unit = {
        OrientDbGraphConnection.shutDown
    }
}