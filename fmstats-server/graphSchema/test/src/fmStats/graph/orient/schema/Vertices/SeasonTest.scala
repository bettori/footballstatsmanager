package fmStats.graph.orient.schema.Vertices

import org.scalatest.FunSuite
import utils.test.EffectTester
import org.mockito.MockitoSugar.when
import org.mockito.MockitoSugar.mock
import org.mockito.ArgumentMatchersSugar.any
import org.mockito.stubbing.ScalaFirstStubbing
import org.mockito.internal.ValueClassExtractor
import fmStats.graph.connection.GraphClient
import fmStats.graph.orient.OrientVertexTypeWrapper
import fmStats.graph.common.VertexTypeWrapper
import fmStats.graph.orient.OrientFmStatsVertex
import fmStats.graph.common.FmStatsVertex
import com.orientechnologies.orient.core.sql.executor.MatchFirstStep
import com.orientechnologies.orient.core.record.OVertex
import zio.ZIO
import zio.Task
import fmStats.graph.common.GraphType

class SeasonTest extends FunSuite with EffectTester {
    test("The vertex object should successfully create the property mapping") {
        val sut = Season.propertyMapping
        println(sut)
        assert(sut.size == 1)
        assert(sut.keys.head == "label" && sut(sut.keys.head).propertyType == GraphType.STRING)
    }

    test("When creating the vertex type, should get a vertex type wrapper back from the graph client") {
        val vertexWrapperMock = mock[VertexTypeWrapper]
        val graphClientMock = mock[GraphClient]
        when(graphClientMock.createVertexType(Season.name, Season.propertyMapping)) thenReturn(ZIO.effect(vertexWrapperMock))

        val sut = Season.defineVertexType()
        testEffect(sut, graphClientMock, (v: VertexTypeWrapper) => assert(v != null), (x: Throwable) => assert(false))
    }

    test("When creating a vertex, get a vertex wrapper back from the graph client") {
        val vertexWrapperMock = mock[FmStatsVertex]
        val graphClientMock = mock[GraphClient]
        when(graphClientMock.addVertex(Season))
        .asInstanceOf[ScalaFirstStubbing[Task[FmStatsVertex]]].thenReturn(ZIO.effect(vertexWrapperMock))

        val sut = Season.createVertex()
        testEffect(sut, graphClientMock, (v: FmStatsVertex) => assert(v != null), (t: Throwable) => assert(false))
    }

    test("When creating a Season vertex with property values, the value is correctly set") {
        val key = "label"
        val properties = Map(key -> "1992-1993")
        val oVertexMock = mock[OVertex]
        when[ValueClassExtractor[String]](oVertexMock.getProperty(key))
        .asInstanceOf[ScalaFirstStubbing[String]] thenReturn(properties(key))
        val graphClientMock = mock[GraphClient]
        when(graphClientMock.addVertex(Season.name, Season.propertyMapping, Some(properties)))
        .asInstanceOf[ScalaFirstStubbing[Task[FmStatsVertex]]] thenReturn(ZIO.effect(Season(oVertexMock, "").asInstanceOf[FmStatsVertex])) 

        val sut = Season.createVertex(Some(properties))
        testEffect(sut, graphClientMock, (v: FmStatsVertex) => assert(v != null && v.isInstanceOf[OrientFmStatsVertex] && 
        v.asInstanceOf[OrientFmStatsVertex].wrappedVertex.getProperty(key).asInstanceOf[String] == properties(key)), 
        (t: Throwable) => assert(false))
    }
}