package fmStats.graph.orient.schema.Edges

import org.scalatest.FunSuite
import utils.test.EffectTester
import fmStats.graph.common.GraphType
import fmStats.graph.common.EdgeTypeWrapper
import fmStats.graph.connection.GraphClient
import org.mockito.MockitoSugar.when
import org.mockito.MockitoSugar.mock
import org.mockito.ArgumentMatchersSugar.any
import org.mockito.stubbing.ScalaFirstStubbing
import org.mockito.internal.ValueClassExtractor
import zio.ZIO
import fmStats.graph.orient.schema.Vertices.Player
import fmStats.graph.orient.schema.Vertices.Club
import fmStats.graph.common.FmStatsEdge


class IsContractedToTest extends FunSuite with EffectTester {

    test("Typed edge object should have name") {
        val name = IsContractedTo.name
        assert(name == "isContractedTo")
    }

    test("Typed edge object should create property mapping") {
        val sut = IsContractedTo.propertyMapping
        assert(sut.size == 2 && sut("contractStartDate").propertyType == GraphType.DATE && sut("contractEndDate").propertyType == GraphType.DATE)
    }

    test("IsContractedTo can create edge type") {
        val wrapperMock = mock[EdgeTypeWrapper]
        val clientMock = mock[GraphClient]

        when(clientMock.createEdgeType(IsContractedTo.name, IsContractedTo.propertyMapping)).thenReturn(ZIO.effect(wrapperMock))
        val result = IsContractedTo.defineEdgeType()
        testEffect(result, clientMock, (e: EdgeTypeWrapper) => assert(e != null), (t: Throwable) => assert(false))
    }

    test("IsContractedTo can create edge") {
        val clientMock = mock[GraphClient]
        val edgeMock = mock[FmStatsEdge]

        val player = Player("Diego Maradona")
        val club = Club("Boca Juniors")
        val contract = IsContractedTo(new java.util.Date, new java.util.Date)

        when(clientMock.addEdge(IsContractedTo.name, player, club, IsContractedTo.propertyMapping, Some(contract))).thenReturn(ZIO.effect(edgeMock))
        
        val sut = IsContractedTo.createEdge(player, club, Some(contract))
        testEffect(sut, clientMock, (e : FmStatsEdge) => assert(e != null), (t: Throwable) => assert(false))
    }
}