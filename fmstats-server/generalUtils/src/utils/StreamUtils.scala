package utils
import java.io.Closeable
object StreamUtils {
    def withCloseable[A <: Closeable, B](resource: A, processMethod: A => B) : B = {
        try {
            processMethod(resource)
        } finally {
            resource.close()
        }
    }
}