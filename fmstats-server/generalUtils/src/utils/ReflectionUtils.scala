package utils
import scala.reflect.ClassTag
import scala.reflect.runtime.universe._
import scala.reflect.runtime.currentMirror

object ReflectionUtils {
    // Use reflection to get all the fields for an object of type T
    def getMethods[T: TypeTag] : List[String] = {
        typeOf[T].members.sorted.collect {
            case m: MethodSymbol if m.isCaseAccessor => m.name.toString()
        }
    }

    // Use reflection to invoke constructor of object of type T
    def invokeConstructor[T: TypeTag](arguments: Any*) : T = {
        val classMirror = currentMirror.reflectClass(typeOf[T].typeSymbol.asClass)
        val ctor = classMirror.symbol.primaryConstructor
        classMirror.reflectConstructor(ctor.asMethod).apply(arguments).asInstanceOf[T]
    }
}