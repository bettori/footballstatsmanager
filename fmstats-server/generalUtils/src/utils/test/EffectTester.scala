package utils.test
import zio.ZIO

trait EffectTester {
    def testEffect[R, E, A](effect: ZIO[R, E, A], 
                                    environment: R, 
                                    successAssert: A => org.scalatest.Assertion,
                                    errorAssert: E => org.scalatest.Assertion): org.scalatest.Assertion = {
        val test = effect.provide(environment).fold(errorAssert, successAssert)
        val runtime = zio.Runtime.default
        runtime.unsafeRun(test)
    }
}